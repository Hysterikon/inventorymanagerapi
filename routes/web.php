<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/register',['uses'=>'UserController@register']);
$router->post('/login',['uses'=>'LoginController@index']); # Testweise Middleware zum Login hinzugefügt ??????? WARUM ? Nächstes mal mit Begründung
$router->get('user/logout',['middleware'=>'can:account-access','uses'=>'LoginController@logout']);

#FILESYTSEM ROUTES
$router->group(['prefix'=>'file','middleware'=>'auth'],function () use ($router){

    $router->get('all',['middleware'=>'can:all-access','uses'=>'FileController@index']);

    $router->get('{guid}/list',['middleware'=>'can:upload-files','uses'=>'FileController@listFiles']);

    $router->post('upload', ['middleware'=>'can:upload-files','uses'=>'FileController@postFile']);

    $router->get('{name}', ['middleware'=>'can:upload-files','uses'=>'FileController@readFile']);

    $router->put('{guid}', ['middleware'=>'can:upload-files','uses'=>'FileController@putFile']);

    $router->get('{name}/download',['middleware'=>'can:upload-files','uses'=>'FileController@download']);

    $router->delete('{name}', ['middleware'=>'can:upload-files','uses'=>'FileController@deleteFile']);
});

#ADMIN REQUEST ROUTES
$router->group(['prefix'=> 'request','middleware'=>'auth'], function () use ($router){

    $router->post('send',['middleware'=>'can:account-access','uses'=>'AdminRequestController@createRequest']);

    $router->get('list',['middleware'=>'can:all-access','uses'=>'AdminRequestController@listRequests']);

    $router->get('read',['middleware'=>'can:all-access','uses'=>'AdminRequestController@getRequest']);

    $router->put('update',['middleware'=>'can:all-access','uses'=>'AdminRequestController@updateRequest']);

    $router->delete('delete',['middleware'=>'can:all-access','uses'=>'AdminRequestController@deleteRequests']);

});

# USER_ROUTES
$router->group(['prefix'=>'user','middleware'=>'auth'],function () use ($router){

    $router->get('isAdmin', ['middleware'=>'can:account-access','uses'=>'UserController@isAdmin']);

    $router->get('all', ['middleware'=>'can:all-access','uses'=>'UserController@showAllUsers']);

    $router->get('count',['middleware'=>'can:all-access','uses'=>'UserController@countAllUsers']);

    $router->get('find/{username}',['middleware'=>'can:all-access','uses'=>'UserController@findUser']);

    $router->get('{id}', ['middleware'=>'can:account-access','uses'=>'UserController@showOneUser']);

    $router->get('', ['middleware'=>'can:account-access','uses'=>'UserController@showOneUserByRequest']);

    $router->delete('storage/delete',['middleware'=>'can:account-access','uses'=>'UserController@deleteStorage']);

    $router->delete('{id}/delete',['middleware'=>'can:account-access','uses'=>'UserController@deleteUser']);

    $router->put('{id}/update',['middleware'=>'can:account-access','uses'=>'UserController@updateUser']);

    //TODO: Remove before release
    $router->get('roles/{id}',['uses'=>'UserController@getRoles']); # DEBUGGING

    $router->post('storage/new',['middleware'=>'can:account-access','uses'=>'UserController@createUserDatabase']);
});

# LICENSE_ROUTES
$router->group(['prefix'=>'license','middleware'=>'auth'],function () use ($router){

    $router->get('all', ['middleware'=>'can:account-access','uses'=>'LicenseController@showAllLicenses']);

    $router->get('all/admin', ['middleware'=>'can:all-access','uses'=>'LicenseController@showAllLicensesAdmin']);

    $router->get('{id}', ['middleware'=>'can:account-access','uses'=>'LicenseController@showOneLicense']);

    $router->post('new',['middleware'=>'can:account-access','uses'=>'LicenseController@createLicense']);

    $router->post('admin/new',['middleware'=>'can:all-access','uses'=>'LicenseController@createLicenseAdmin']);

    $router->delete('{id}/delete',['middleware'=>'can:account-access','uses'=>'LicenseController@deleteLicense']);

    $router->put('{id}/update',['middleware'=>'can:account-access','uses'=>'LicenseController@updateLicense']); # KANN WEG
});

# LICENSE_TYPES_ROUTES
$router->group(['prefix'=>'licenses/types','middleware'=>'auth'],function () use ($router){

    $router->get('all', ['middleware'=>'can:account-access','uses'=>'TypeController@getAllTypes']);

    $router->get('{id}', ['middleware'=>'can:all-access','uses'=>'TypeController@getOneType']);

    $router->post('new',['middleware'=>'can:all-access','uses'=>'TypeController@createType']);

    $router->delete('{id}/delete',['middleware'=>'can:all-access','uses'=>'TypeController@deleteType']);

    $router->put('{id}/update',['middleware'=>'can:all-access','uses'=>'TypeController@updateType']);
});

# TABLES_ROUTES
$router->group(['prefix'=>'tables','middleware'=>'auth'],function () use ($router){

    $router->get('all',['uses'=>'TableController@getAllTables']);

    $router->get('count',['middleware'=>'can:all-access','uses'=>'TableController@countAllTables']);

    $router->get('admin/{searchTerm}',['middleware'=>'can:all-access','uses'=>'TableController@findProjects']);

    $router->get('updated/{count}',['middleware'=>'can:create-table','uses'=>'TableController@getUpdatedTables']);

    $router->get('{guid}',['middleware'=>"role:reader,guid,'table'",'uses'=>'TableController@getOneTable']);

    $router->post('new',['middleware'=>'can:create-table','uses'=>'TableController@createTable']);

    $router->post('tags/new',['middleware'=>'can:create-table','uses'=>'TableController@addTag']);

    $router->post('tags/remove',['middleware'=>'can:create-table','uses'=>'TableController@removeTag']);

    $router->get('{guid}/tags/list',['middleware'=>'can:create-table','uses'=>'TableController@listTags']);

    $router->post('categories/new',['middleware'=>'can:create-table','uses'=>'TableController@addCategory']);

    $router->post('categories/remove',['middleware'=>'can:create-table','uses'=>'TableController@removeCategory']);

    $router->get('{guid}/categories/list',['middleware'=>'can:create-table','uses'=>'TableController@listCategories']);

    $router->post('{guid}/nutzer',['middleware'=>'role:owner,guid','uses'=>'TableController@giveAccess']);

//    $router->put('{guid}/update',['middleware'=>'role:editor,guid','uses'=>'TableController@updateTable']);

    $router->put('update',['middleware'=>'can:create-table','uses'=>'TableController@updateTable']);

    $router->delete('delete',['middleware'=>'can:create-table','uses'=>'TableController@deleteTable']);
});

# ROW ROUTES
$router->group(['prefix'=>'rows','middleware'=>'auth'],function () use ($router){

    $router->get('qr/make/{partOf}',['middleware'=>"role:editor,partOf,'table'",'uses'=>'RowController@addCodes']);

    $router->get('all/{partOf}',['middleware'=>"role:reader,partOf,'table'",'uses'=>'RowController@getAllRows']);

    $router->get('{row_id}',['middleware'=>"role:reader,row_id,'row'",'uses'=>'RowController@getOneRow']);

    $router->post('{row_id}/qr/add',['middleware'=>"role:editor,row_id,'row'",'uses'=>'RowController@addQrCode']);

    $router->get('qr/find/{qr_code}',['middleware'=>'can:create-table','uses'=>'RowController@findQrCode']);

    $router->post('{guid}/new',['middleware'=>"role:editor,guid,'table'",'uses'=>'RowController@createRow']);

    $router->get('{table_guid}/total',['middleware'=>"role:reader,table_guid,'table'",'uses'=>'RowController@getRowNumber']);

    $router->delete('{guid}/delete',['middleware'=>"role:editor,guid,'table'",'uses'=>'RowController@deleteRow']);

    $router->put('{row_id}/update',['middleware'=>"role:editor,row_id,'row'",'uses'=>'RowController@updateRow']);

});

# COLUMNS_ROUTES
$router->group(['prefix'=>'columns','middleware'=>'auth'],function () use ($router){
    $router->get('all/{partOf}',['middleware'=>"role:reader,partOf,'table'",'uses'=>'ColumnController@getAllColumns']);

    $router->get('{guid}',['middleware'=>"role:reader,guid,'column'",'uses'=>'ColumnController@getOneColumn']);

    $router->post('categories/new',['middleware'=>'can:create-table','uses'=>'ColumnController@addCategory']);

    $router->post('tags/new',['middleware'=>'can:create-table','uses'=>'ColumnController@addTag']);

    $router->post('{guid}/new',['middleware'=>"role:editor,guid,'table'",'uses'=>'ColumnController@createColumn']);

    $router->delete('{partOf}/delete',['middleware'=>"role:editor,partOf,'table'",'uses'=>'ColumnController@deleteColumn']);

    $router->put('{partOf}/update',['middleware'=>"role:editor,partOf,'table'",'uses'=>'ColumnController@updateColumn']);

    $router->post('tags/remove',['middleware'=>'can:create-table','uses'=>'ColumnController@removeTag']);

    $router->get('{guid}/tags/list',['middleware'=>'can:create-table','uses'=>'ColumnController@listTags']);

    $router->post('categories/remove',['middleware'=>'can:create-table','uses'=>'ColumnController@removeCategory']);

    $router->get('{guid}/categories/list',['middleware'=>'can:create-table','uses'=>'ColumnController@listCategories']);

});

# VERSION_ROUTES
$router->group(['prefix'=>'versions','middleware'=>'auth'],function () use ($router){
    $router->get('all/{guid}',['as'=> 'getAllVersions','middleware'=>"role:editor,guid,'table'",'uses'=>'CVersionController@getAllVersions']);

    $router->get('{id}',['middleware'=>"role:editor,id,'version'",'uses'=>'CVersionController@getOneVersion']);

    $router->post('{guid}/new',['middleware'=>"role:editor,guid,'table'",'uses'=>'CVersionController@createVersion']);

    $router->delete('delete',['middleware'=>'can:create-table','uses'=>'CVersionController@deleteVersion']); //TODO: Gate in alle Table oder Column bezogenen funktionen einebauen die nur eine "CAN" Middleware in der route haben.

    $router->put('{id}/update',['middleware'=>"role:editor,id,'version'",'uses'=>'CVersionController@updateVersion']);
});

# ENUM_ROUTES
$router->group(['prefix'=>'enum','middleware'=>'auth'],function () use ($router){
    $router->get('all',['middleware'=>'can:create-table','uses'=>'EnumDataController@getAllTypes']);

    $router->get('{data_id}',['middleware'=>'can:create-table,','uses'=>'EnumDataController@getOneType']);

    $router->post('new',['middleware'=>'can:all-access','uses'=>'EnumDataController@createType']);

    $router->delete('{data_id}/delete',['middleware'=>'can:all-access','uses'=>'EnumDataController@deleteType']);

    $router->put('{data_id}/update',['middleware'=>'can:all-access','uses'=>'EnumDataController@updateType']);
});

# CATEGORY_ROUTES
$router->group(['prefix'=>'categories','middleware'=>'auth'],function () use ($router){
    $router->get('all',['middleware'=>'can:create-table','uses'=>'CategoryController@getAllCats']);

    $router->get('list',['middleware'=>'can:create-table','uses'=>'CategoryController@listCats']);

    $router->get('{catID}',['middleware'=>'can:create-table','uses'=>'CategoryController@getOneCat']);

    $router->post('new',['middleware'=>'can:create-table','uses'=>'CategoryController@createCat']);

    $router->delete('delete',['middleware'=>'can:create-table','uses'=>'CategoryController@deleteCat']);

    $router->put('update',['middleware'=>'can:create-table','uses'=>'CategoryController@updateCat']);
});

# TAG_ROUTES
$router->group(['prefix'=>'tags','middleware'=>'auth'],function () use ($router){
    $router->get('all',['middleware'=>'can:create-table','uses'=>'TagController@getAllTags']);

    $router->get('list',['middleware'=>'can:create-table','uses'=>'TagController@listTags']);

    $router->get('{tagID}',['middleware'=>'can:create-table','uses'=>'TagController@getOneTag']);

    $router->post('new',['middleware'=>'can:create-table','uses'=>'TagController@createTag']);

    $router->delete('delete',['middleware'=>'can:create-table','uses'=>'TagController@deleteTag']);

    $router->put('update',['middleware'=>'can:create-table','uses'=>'TagController@updateTag']);
});






