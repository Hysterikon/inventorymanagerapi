<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin=Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => json_encode([
                'all-access' => true,
                'view-account' => true,
                'upload-files' => true,
                'read-files' => true,
            ])
        ]);

        $freeUser=Role::create([
            'name' => 'FreeUser',
            'slug' => 'FreeUser',
            'permissions' =>json_encode([
                'view-account' => true,
            ])
        ]);


        $user=Role::create([
            'name' => 'User',
            'slug' => 'user',
            'permissions' =>json_encode([
                'create-table' => true,
                'view-account' => true,
                'upload-files' => true,
                'read-files' => true,
            ])
        ]);

        $editor=Role::create([
            'name' => 'Editor',
            'slug' => 'editor',
            'permissions' => json_encode([
                'show-table' => true,
                'alter-table' => true,
                'upload-files' => true,
                'read-files' => true,

            ])
        ]);

        $owner=Role::create([
            'name' => 'Owner',
            'slug' => 'owner',
            'permissions' => json_encode([
                'delete-table' => true,
                'alter-table' => true,
                'give-access' => true,
                'show-table'=> true,
                'upload-files' => true,
                'read-files' => true,
            ])
        ]);

        $reader=Role::create([
            'name' => 'Reader',
            'slug' => 'reader',
            'permissions' => json_encode([
                'show-table' => true,
                'read-files' => true,
            ])
        ]);

    }
}
