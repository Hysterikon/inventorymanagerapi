<?php

use App\EnumData;
use Illuminate\Database\Seeder;

class EnumDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title=EnumData::create([
            'data_id'=>'0',
            'type'=> 'Titel'
        ]);

        $string=EnumData::create([
            'data_id'=>'1',
            'type'=> 'Text'
        ]);

        $number=EnumData::create([
            'data_id'=>'2',
            'type'=> 'Zahl'
        ]);

        $image=EnumData::create([
            'data_id'=>'3',
            'type'=> 'Image'
        ]);

        $sub_table=EnumData::create([
            'data_id'=>'4',
            'type'=>'sub_table'
        ]);

        $status=EnumData::create([
            'data_id'=>'5',
            'type'=>'Status'
        ]);

        $date=EnumData::create([
            'data_id'=>'6',
            'type'=>'Datum'
        ]);

        $reminder=EnumData::create([
            'data_id'=>'7',
            'type'=>'Erinnerung'
        ]);
    }
}
