<?php

use App\Type;
use Illuminate\Database\Seeder;

class LicenseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Free=Type::create([
          'license_Name'=>'Kostenlose Lizenz',
          'price'=>'0',
          'max_table_shares' => '0',
          'max_tables' => '10',
          'column_pattern_size' => '10',
          'column_version_control' => false,
          'column_version_lifespan' => '0',
          'max_table_size' => '25',
          'max_table_lifespan' => '0'
        ]);

        $Basic=Type::create([
            'license_Name'=>'Basis Lizenz',
            'price'=>'100',
            'max_table_shares' => '0',
            'max_tables' => '10',
            'column_pattern_size' => '5',
            'column_version_control' => false,
            'column_version_lifespan' => '0',
            'max_table_size' => '25',
            'max_table_lifespan' => '322464'
        ]);

        $Advanced=Type::create([
            'license_Name'=>'25er Lizenz',
            'price'=>'150',
            'max_table_shares' => '5',
            'max_tables' => '25',
            'column_pattern_size' => '15',
            'column_version_control' => true,
            'column_version_lifespan' => '24523',
            'max_table_size' => '50',
            'max_table_lifespan' => '54345245'
        ]);

        $Special=Type::create([
            'license_Name'=>'100er Lizenz',
            'price'=>'200',
            'max_table_shares' => '10',
            'max_tables' => '100',
            'column_pattern_size' => '30',
            'column_version_control' => true,
            'column_version_lifespan' => '99999999',
            'max_table_size' => '999',
            'max_table_lifespan' => '9999999'
        ]);

    }
}
