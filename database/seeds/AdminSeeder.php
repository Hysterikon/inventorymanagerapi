<?php

use App\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminToken= sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
        $adminToken = hash('sha256', $adminToken);

        $admin=User::create([
            'username'=>'root',
            'email'=>'hennig@debakom.de',
            'password'=>'$2y$10$v93AzsurgLZLZY0K7odtXORxwqOnuQtGTdHL8naQ/p083OnCS24/S', #debakom
            'api_token'=>$adminToken,
            'registration_approved'=>1
        ]);
        $admin->roles()->attach([1]);
    }
}
