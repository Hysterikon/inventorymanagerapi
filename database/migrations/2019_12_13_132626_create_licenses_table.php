<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicensesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('userData')->create('licenses', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('user_id')->unsigned(); //->unique()
            $table->integer('type_id')->unsigned();
            $table->string('guid');
            $table->string('license_name')->nullable();
            $table->double('price')->nullable();
            $table->integer('activation_date');
            $table->integer('expiration_date');
            //$table->bool('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('userData')->dropIfExists('licenses');
    }
}
