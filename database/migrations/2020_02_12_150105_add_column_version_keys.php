<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnVersionKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('columnversions', function (Blueprint $table) {

            $table->foreign('column_guid')
                  ->references('guid')
                  ->on('custom_columns')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('columnversions', function (Blueprint $table ){
            $table->dropForeign(['column_guid']);
        });
    }
}
