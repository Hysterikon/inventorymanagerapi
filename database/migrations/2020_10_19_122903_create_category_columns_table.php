<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_columns', function (Blueprint $table) {
            $table->unsignedInteger('category_id');
            $table->string('column_guid');

            $table->unique(['category_id','column_guid']);
            $table->foreign('category_id')
                ->references('id')
                ->on(env('DB_DATABASE').'.categories')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
//            $table->foreign('column_guid')
//                ->references('guid')
//                ->on('custom_tables')
//                ->onDelete('CASCADE')
//                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_columns');
    }
}
