<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenseTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('userData')->create('types', function (Blueprint $table) {
            $table->integer('id',true)->unsigned();
            $table->string('license_name');
            $table->double('price');
            $table->integer('max_table_shares')->nullable();
            $table->integer('max_tables');
            $table->integer('column_pattern_size');
            $table->integer('column_entry_max_amount');
            $table->boolean('column_version_control')->nullable();
            $table->integer('column_version_lifespan')->nullable();
            $table->integer('max_table_size');
            $table->integer('max_table_lifespan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('userData')->dropIfExists('types');
    }
}
