<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $timestamps = false;
    public function up()
    {
        Schema::create('custom_columns', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('guid')->unique();
            $table->string('value');
            $table->integer('data_type_id');
            $table->string('last_user');
            $table->integer('column_order');
            $table->unsignedInteger('row_id');
            $table->string('part_of');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_columns');
    }
}
