<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableColumnKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_columns', function (Blueprint $table) {

            $table->foreign('part_of')
                  ->references('guid')
                  ->on('custom_tables')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');

            $table->foreign('data_type_id')
                  ->references('data_id')
                  ->on('enumData')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_columns', function (Blueprint $table){
//            $table->dropForeign(['partOf']);
            $table->dropForeign(['data_type_id']);
        });
    }
}
