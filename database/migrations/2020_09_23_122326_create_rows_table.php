<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_rows', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            $table->unsignedInteger('last_user');
            $table->unsignedInteger('predecessor')->nullable();
            $table->unsignedInteger('successor')->nullable();
            $table->string('table_guid');
            $table->integer('created_at');
            $table->integer('updated_at');

            $table->foreign('table_guid')
                ->references('guid')
                ->on('custom_tables')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_rows');
    }
}
