<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $timestamps = false;
    public function up()
    {
        Schema::create('custom_tables', function (Blueprint $table) {
            $table->unsignedInteger('id',true);
            $table->string('guid', 36,true)->unique();
            $table->string('name');
            $table->integer('owner')->unsigned();
            $table->boolean('sub_table');
            $table->integer('created_at');
            $table->integer('updated_at');
            $table->integer('deletion_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_tables');
    }
}
