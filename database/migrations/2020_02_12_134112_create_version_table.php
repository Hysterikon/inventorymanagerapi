<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $timestamps = false;
    public function up()
    {
        Schema::create('columnversions', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('column_guid');
            $table->string('value');
//            $table->integer('dataType');
//            $table->integer('row_id');
//            $table->integer('columnOrder');
            $table->integer('date');
            $table->integer('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('columnversions');
    }
}
