<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_tables', function (Blueprint $table) {
            $table->unsignedInteger('tag_id');
            $table->string('table_guid');

            $table->unique(['tag_id','table_guid']);
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
//            $table->foreign('table_guid')
//                ->references('guid')
//                ->on('custom_tables')
//                ->onDelete('CASCADE')
//                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_tables');
    }
}
