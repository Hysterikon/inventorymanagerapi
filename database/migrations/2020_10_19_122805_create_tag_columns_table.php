<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_columns', function (Blueprint $table) {
            $table->unsignedInteger('tag_id');
            $table->string('column_guid');

            $table->unique(['tag_id','column_guid']);
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
//            $table->foreign('table_id')
//                ->references('id')
//                ->on('custom_tables')
//                ->onDelete('CASCADE')
//                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_columns');
    }
}
