<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('custom_table_id');
            $table->text('table_role')->nullable();
            $table->timestamps();

            $table->unique(['user_id','custom_table_id']);
            $table->foreign('user_id')
                  ->references('id')
                  ->on('inventorymanager_userdata.users')
                  ->onDelete('cascade');
            $table->foreign('custom_table_id')
                  ->references('id')
                  ->on('custom_tables')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access');
    }
}
