<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnumDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $timestamps= false;
    public function up()
    {
        Schema::create('enumData', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('data_id')->unique();
            $table->string('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enumData');
    }
}
