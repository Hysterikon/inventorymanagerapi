<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::connection('userData')->create('users', function (Blueprint $table) {
            $table->integer('id', true)->unsigned();
            $table->string('email')->unique();
            $table->string('username');
            $table->string('password',60);
            $table->integer('registration_approved');
            $table->string('api_token')->unique()->nullable();
            $table->remembertoken();
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('userData')->dropIfExists('users');
    }
}
