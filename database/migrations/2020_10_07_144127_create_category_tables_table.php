<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_tables', function (Blueprint $table) {
            $table->unsignedInteger('category_id');
            $table->string('table_guid');

            $table->unique(['category_id','table_guid']);
            $table->foreign('category_id')
                ->references('id')
                ->on(env('DB_DATABASE').'.categories')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
//            $table->foreign('table_guid')
//                ->references('guid')
//                ->on('custom_tables')
//                ->onDelete('CASCADE')
//                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_tables');
    }
}
