<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class CustomColumn extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $connection = 'mainDB';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid', 'value','data_type_id', 'last_user', 'column_order', 'row_id', 'part_of'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public $timestamps =false;

    public function table()
    {
        return $this->belongsTo(CustomTable::class,'part_of','guid');
    }

    public function versions()
    {
        return $this->hasMany(Columnversion::class,'column_guid','guid');
    }

    public function dataType()
    {
        return $this->belongsTo(EnumData::class,'data_type_id','data_id');
    }

    public function row()
    {
        return $this->hasOne(CustomRow::class, 'id', 'row_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, env('DB_DATABASE').'.tag_columns', 'column_guid');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,env('DB_DATABASE').'.category_columns', 'column_guid');
    }
}
