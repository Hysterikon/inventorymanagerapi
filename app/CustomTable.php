<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class CustomTable extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $connection ='mainDB';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'guid','owner', 'created_at','updated_at','deletion_date','sub_table'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function owner(){
        return $this->belongsTo(User::class, 'owner', 'id');
    }

    public function contributors()
    {
        return $this->belongsToMany(User::class,'access');
    }

    public function columns()
    {
        return $this->hasMany(CustomColumn::class, 'part_of','guid');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,env('DB_DATABASE').'.category_tables', 'table_guid', 'category_id','guid');
    }

    public function tags()
    {
         return $this->belongsToMany(Tag::class, env('DB_DATABASE').'.tag_tables','table_guid','tag_id', 'guid');
//        return $this->belongsToMany(Tag::class,'table_guid','guid');
    }

    public function access()
    {
        return $this->belongsToMany(User::class,'access','custom_table_id','user_id','guid');
    }

}
