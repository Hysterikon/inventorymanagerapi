<?php

namespace App\Providers;

use App\Category;
use App\Columnversion;
use App\CustomColumn;
use App\CustomRow;
use App\CustomTable;
use App\Tag;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;



class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerAdminPolicies();
        $this->registerUserPolicies();
        $this->registerTablePolicies();


        $this->app['auth']->viaRequest('api', function ($request) {

            if ($request->input('api_token'))
            {
                $api_token = $request->input('api_token');
                return User::where('api_token',hash('sha256', $api_token))->first();
            }
        });


    }

    public function registerAdminPolicies()
    {
        Gate::define('all-access',function(User $user){
            return $user->hasAccess([json_encode(['all-access'=> true])]);
        });
    }

    public function registerUserPolicies()
    {
        Gate::define('create-table',function(User $user){
            return $user->hasAccess([json_encode(['create-table'=> true,'all-access'=>true])]);
        });


        Gate::define('account-access',function(User $user){
            return $user->hasAccess([json_encode(['view-account' =>true])]);
        });

        Gate::define('upload-files',function(User $user){
            return $user->hasAccess([json_encode(['upload-files' =>true])]);
        });

    }

    public function registerTablePolicies()
    {
        #------------
        Gate::define('owner',function(User $user, $Tid, $dbTable){ //dbTable definiert in welcher Datenbank Tabelle nach der Tid gesucht wird

            if($user->storage != null)
            {
                //set the personalDB connection to the db of the calling user
                Config::set('database.connections.personalDB',[
                    'driver'    => 'mysql',
                    'host'      => env('DB_HOST'),
                    'port'      => env('DB_PORT'),
                    'database'  => $user->storage->database,
                    'username'  => env('DB_USERNAME'),
                    'password'  => env('DB_PASSWORD'),
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                    'strict'    => false,
                ]);
            }

            switch ($dbTable){
                case "table":
                    if (CustomTable::where('guid',$Tid)->exists() || CustomTable::on('personalDB')->where('guid',$Tid)->exists()) {
                        $table = CustomTable::where('guid', $Tid)->value('id');
                        if ($table == null) {
                            $table = CustomTable::on('personalDB')->where('guid', $Tid)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "column":
                    if(CustomColumn::where('guid',$Tid)->exists() || CustomColumn::on('personalDB')->where('guid',$Tid)->exists()) {
                        $table = CustomColumn::where('guid', $Tid)->value('part_of');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = CustomColumn::on('personalDB')->where('guid', $Tid)->value('part_of');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "version":
                    if (Columnversion::where('id', $Tid)->exists() || Columnversion::on('personalDB')->where('id',$Tid)->exists()) {
                        $table = Columnversion::where('id', $Tid)->value('column_guid');
                        $table = CustomColumn::where('guid', $table)->value('part_of');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = Columnversion::on('personalDB')->where('id', $Tid)->value('column_guid');
                            $table = CustomColumn::on('personalDB')->where('guid', $table)->value('part_of');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "row":
                    if (CustomRow::where('id', $Tid)->exists() || CustomRow::on('personalDB')->where('id',$Tid)->exists()) {
                        $table = CustomRow::where('id', $Tid)->value('table_guid');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = CustomRow::on('personalDB')->where('id', $Tid)->value('table_guid');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "category":
                    if (Category::where('id',$Tid)->exists()) {
                        $table = Category::where('id', $Tid)->value('table_guid'); //TODO:  Kann weg ?
                        $table = CustomTable::where('guid', $table)->value('id');
                    } else {
                        $table = null;
                    }
                    break;
                case "tag":
                    if (Tag::where('id',$Tid)->exists()) {
                        $table = Tag::where('id', $Tid)->value('table_guid'); //TODO: Kann weg ?
                        $table = CustomTable::where('guid', $table)->value('id');
                    } else {
                        $table = null;
                    }
                    break;
            }

            # json_encode weil der in_array vergleich sonst nicht funktioniert
            return $user->hasRole([json_encode(['owner'=>true,])], $table);
        });

        Gate::define('reader',function(User $user, $Tid, $dbTable){

            if($user->storage != null)
            {
                //set the personalDB connection to the db of the calling user
                Config::set('database.connections.personalDB',[
                    'driver'    => 'mysql',
                    'host'      => env('DB_HOST'),
                    'port'      => env('DB_PORT'),
                    'database'  => $user->storage->database,
                    'username'  => env('DB_USERNAME'),
                    'password'  => env('DB_PASSWORD'),
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                    'strict'    => false,
                ]);
            }



            switch ($dbTable){
                case "table":
                    if (CustomTable::where('guid',$Tid)->exists() || CustomTable::on('personalDB')->where('guid',$Tid)->exists()) {
                        $table = CustomTable::where('guid', $Tid)->value('id');
                        if ($table == null) {
                            $table = CustomTable::on('personalDB')->where('guid', $Tid)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "column":
                    if(CustomColumn::where('guid',$Tid)->exists() || CustomColumn::on('personalDB')->where('guid',$Tid)->exists()) {
                        $table = CustomColumn::where('guid', $Tid)->value('part_of');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = CustomColumn::on('personalDB')->where('guid', $Tid)->value('part_of');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "version":
                    if (Columnversion::where('id', $Tid)->exists() || Columnversion::on('personalDB')->where('id',$Tid)->exists()) {
                        $table = Columnversion::where('id', $Tid)->value('column_guid');
                        $table = CustomColumn::where('guid', $table)->value('part_of');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = Columnversion::on('personalDB')->where('id', $Tid)->value('column_guid');
                            $table = CustomColumn::on('personalDB')->where('guid', $table)->value('part_of');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "row":
                    if (CustomRow::where('id', $Tid)->exists() || CustomRow::on('personalDB')->where('id',$Tid)->exists()) {
                        $table = CustomRow::where('id', $Tid)->value('table_guid');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = CustomRow::on('personalDB')->where('id', $Tid)->value('table_guid');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "category":
                    if (Category::where('id',$Tid)->exists()) {
                        $table = Category::where('id', $Tid)->value('table_guid'); //TODO:  Kann weg ?
                        $table = CustomTable::where('guid', $table)->value('id');
                    } else {
                        $table = null;
                    }
                    break;
                case "tag":
                    if (Tag::where('id',$Tid)->exists()) {
                        $table = Tag::where('id', $Tid)->value('table_guid'); //TODO: Kann weg ?
                        $table = CustomTable::where('guid', $table)->value('id');
                    } else {
                        $table = null;
                    }
                    break;
            }

//            if (CustomTable::where('guid',$Tid)->exists() || CustomTable::on('personalDB')->where('guid',$Tid)->exists()){
//                $table = CustomTable::where('guid',$Tid)->value('id');
//                if($table == null)
//                {
//                    $table = CustomTable::on('personalDB')->where('guid',$Tid)->value('id');
//                }
//            } elseif(CustomColumn::where('guid',$Tid)->exists() || CustomColumn::on('personalDB')->where('guid',$Tid)->exists()){
//                $table = CustomColumn::where('guid',$Tid)->value('part_of');
//                $table = CustomTable::where('guid',$table)->value('id');
//                if ($table == null)
//                {
//                    $table = CustomColumn::on('personalDB')->where('guid',$Tid)->value('part_of');
//                    $table = CustomTable::on('personalDB')->where('guid',$table)->value('id');
//                }
//            } elseif (Columnversion::where('id', $Tid)->exists() || Columnversion::on('personalDB')->where('id',$Tid)->exists()){
//
//                $table = Columnversion::where('id',$Tid)->value('column_guid');
//                $table = CustomColumn::where('guid',$table)->value('part_of');
//                $table = CustomTable::where('guid',$table)->value('id');
//                if($table == null)
//                {
//                    $table = Columnversion::on('personalDB')->where('id',$Tid)->value('column_guid');
//                    $table = CustomColumn::on('personalDB')->where('guid',$table)->value('part_of');
//                    $table = CustomTable::on('personalDB')->where('guid',$table)->value('id');
//                }
//            } elseif (CustomRow::where('id', $Tid)->exists() || CustomRow::on('personalDB')->where('id',$Tid)->exists()){
//                $table = CustomRow::where('id',$Tid)->value('table_guid');
//                $table = CustomTable::where('guid',$table)->value('id');
//                if($table == null)
//                {
//                    $table = CustomRow::on('personalDB')->where('id',$Tid)->value('table_guid');
//                    $table = CustomTable::on('personalDB')->where('guid',$table)->value('id');
//                }
//            } elseif (Category::where('id',$Tid)->exists()){
//                $table = Category::where('id',$Tid)->value('table_guid');
//                $table = CustomTable::where('guid',$table)->value('id');
//            } elseif (Tag::where('id',$Tid)->exists()){
//                $table = Tag::where('id',$Tid)->value('table_guid');
//                $table = CustomTable::where('guid',$table)->value('id');
//            } else {
//                $table = null;
//            }

            if($user->id == CustomTable::where('id',$table)->value('owner') || $user->id == CustomTable::on('personalDB')->where('id',$table)->value('owner'))
            {
                return true;
            } else if($user->hasRole([json_encode(['editor'=>true,])], $table))
            {
                return true;
            } else if($user->hasRole([json_encode(['reader'=>true,])], $table))
            {
                return true;
            } else {
                return $user->hasRole([json_encode(['owner'=>true,])], $table);
            }



        });

        Gate::define('editor',function(User $user, $Tid, $dbTable){

            if($user->storage != null)
            {
                //set the personalDB connection to the db of the calling user
                Config::set('database.connections.personalDB',[
                    'driver'    => 'mysql',
                    'host'      => env('DB_HOST'),
                    'port'      => env('DB_PORT'),
                    'database'  => $user->storage->database,
                    'username'  => env('DB_USERNAME'),
                    'password'  => env('DB_PASSWORD'),
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                    'strict'    => false,
                ]);
            }

            switch ($dbTable){
                case "table":
                    if (CustomTable::where('guid',$Tid)->exists() || CustomTable::on('personalDB')->where('guid',$Tid)->exists()) {
                        $table = CustomTable::where('guid', $Tid)->value('id');
                        if ($table == null) {
                            $table = CustomTable::on('personalDB')->where('guid', $Tid)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "column":
                    if(CustomColumn::where('guid',$Tid)->exists() || CustomColumn::on('personalDB')->where('guid',$Tid)->exists()) {
                        $table = CustomColumn::where('guid', $Tid)->value('part_of');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = CustomColumn::on('personalDB')->where('guid', $Tid)->value('part_of');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "version":
                    if (Columnversion::where('id', $Tid)->exists() || Columnversion::on('personalDB')->where('id',$Tid)->exists()) {
                        $table = Columnversion::where('id', $Tid)->value('column_guid');
                        $table = CustomColumn::where('guid', $table)->value('part_of');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = Columnversion::on('personalDB')->where('id', $Tid)->value('column_guid');
                            $table = CustomColumn::on('personalDB')->where('guid', $table)->value('part_of');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "row":
                    if (CustomRow::where('id', $Tid)->exists() || CustomRow::on('personalDB')->where('id',$Tid)->exists()) {
                        $table = CustomRow::where('id', $Tid)->value('table_guid');
                        $table = CustomTable::where('guid', $table)->value('id');
                        if ($table == null) {
                            $table = CustomRow::on('personalDB')->where('id', $Tid)->value('table_guid');
                            $table = CustomTable::on('personalDB')->where('guid', $table)->value('id');
                        }
                    } else {
                        $table = null;
                    }
                    break;
                case "category":
                    if (Category::where('id',$Tid)->exists()) {
                        $table = Category::where('id', $Tid)->value('table_guid'); //TODO:  Kann weg ?
                        $table = CustomTable::where('guid', $table)->value('id');
                    } else {
                        $table = null;
                    }
                    break;
                case "tag":
                    if (Tag::where('id',$Tid)->exists()) {
                        $table = Tag::where('id', $Tid)->value('table_guid'); //TODO: Kann weg ?
                        $table = CustomTable::where('guid', $table)->value('id');
                    } else {
                        $table = null;
                    }
                    break;
            }

//            if (CustomTable::where('guid',$Tid)->exists() || CustomTable::on('personalDB')->where('guid',$Tid)->exists()){
//                $table = CustomTable::where('guid',$Tid)->value('id');
//                if($table == null)
//                {
//                    $table = CustomTable::on('personalDB')->where('guid',$Tid)->value('id');
//                }
//            } elseif(CustomColumn::where('guid',$Tid)->exists() || CustomColumn::on('personalDB')->where('guid',$Tid)->exists()){
//                $table = CustomColumn::where('guid',$Tid)->value('part_of');
//                $table = CustomTable::where('guid',$table)->value('id');
//                if ($table == null)
//                {
//                    $table = CustomColumn::on('personalDB')->where('guid',$Tid)->value('part_of');
//                    $table = CustomTable::on('personalDB')->where('guid',$table)->value('id');
//                }
//            } elseif (Columnversion::where('id', $Tid)->exists() || Columnversion::on('personalDB')->where('id',$Tid)->exists()){
//                $table = Columnversion::where('id',$Tid)->value('column_guid');
//                $table = CustomColumn::where('guid',$table)->value('part_of');
//                $table = CustomTable::where('guid',$table)->value('id');
//                if($table == null)
//                {
//                    $table = Columnversion::on('personalDB')->where('id',$Tid)->value('column_guid');
//                    $table = CustomColumn::on('personalDB')->where('guid',$table)->value('part_of');
//                    $table = CustomTable::on('personalDB')->where('guid',$table)->value('id');
//                }
//            } elseif (Category::where('id',$Tid)->exists()){
//                $table = Category::where('id',$Tid)->value('table_guid');
//                $table = CustomTable::where('guid',$table)->value('id');
//            } elseif (Tag::where('id',$Tid)->exists()){
//                $table = Tag::where('id',$Tid)->value('table_guid');
//                $table = CustomTable::where('guid',$table)->value('id');
//            } else {
//                $table = null;
//            }
//            dd($user->hasRole([json_encode(['owner'=>true,])], $table));
            //or CustomColumn::where('guid',$Tid)->exists() or Category::where('id',$Tid)
            if(CustomTable::where('id',$table)->exists() || CustomTable::on('personalDB')->where('id',$table)->exists()) {

                if($user->hasRole([json_encode(['editor'=>true,])], $table))
                {
                    return true;
                }
                return $user->hasRole([json_encode(['owner'=>true,])], $table);
            }
        });
    }
}
