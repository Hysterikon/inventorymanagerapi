<?php

namespace App;

use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $connection ='userData';

    protected $fillable=['name','slug','permissons'];

    public $timestamps =false;

    public function users()
    {
        return $this->belongsToMany(User::class,'role_users')->withPivot('tableRole')->withTimestamps(); //TODO Wofür ist die Column tableRoles gedacht ?
    }

    public function hasPE(array $arr)
    {
        foreach($arr as $permissions)
        {
            $permissions = json_decode($permissions,true);
            if($this->contains($permissions))
            {
                return true;
            }

        }
        return false;
    }

    protected function contains(array $permission)
    {
        $permissions = json_decode($this->permissions,true);
        $array_result = json_encode(array_intersect_key($permissions,$permission));
        $chars = array(":","true","{","}","\"");
        $result_string = str_replace($chars,"",$array_result);
        if(array_key_exists($result_string,$permissions))
        {
            return true;
        }
        return false;

    }
}
