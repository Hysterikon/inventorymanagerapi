<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;


class adminRequest extends Model
{
    protected $connection = 'userData';
    protected $dateFormat = 'U';

    protected $fillable = [
      'user_id','message','status','created_at','updated_at'
    ];

}
