<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStorage extends Model
{
    protected $connection = 'userData';

    protected $fillable = [
      'user_id','database','created_at','updated_at'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
