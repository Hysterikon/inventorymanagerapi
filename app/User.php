<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $connection ='userData';

    protected $fillable = [
        'username', 'email', 'password','api_token', 'created_at', 'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public $timestamps =false;

    //List all tables of the user
    public function tables(){
        return $this->hasMany(CustomTable::class,'owner','id');
    }

    //List all tableRoles
    public function tableRoles($dbname = 'inventorymanager_maindb')
    {
        if($this->storage != null)
        {
            //set the personalDB connection to the db of the calling user
            Config::set('database.connections.mainDB',[
              'driver'    => 'mysql',
              'host'      => env('DB_HOST'),
              'port'      => env('DB_PORT'),
              'database'  => $this->storage->database,
              'username'  => env('DB_USERNAME'),
              'password'  => env('DB_PASSWORD'),
              'charset'   => 'utf8',
              'collation' => 'utf8_unicode_ci',
              'prefix'    => '',
              'strict'    => false,
            ]);
        }
        $dbname = $dbname . '.access';
        return $this->belongsToMany(CustomTable::class,$dbname)->withPivot('table_role')->withTimestamps();
    }

    //List all global roles
    public function roles()
    {
        return $this->belongsToMany(Role::class,'role_users')->withTimestamps();
    }

    //List user licenses
    public function licenses()
    {
        return $this->hasMany(License::class,'user_id','id');
    }

    //List table access
    public function access()
    {
        return $this->belongsToMany(CustomTable::class, env('DB_DATABASE').'.access','user_id','custom_table_id', 'id' );
    }

    //return only active licenses
    public function activeLicense()
    {
        $licenses = $this->licenses;
        foreach ($licenses as $license)
        {
            if($license->expiration_date > time() )
            {
                return $license;
            }
        }
        return false;
    }

    //user storage
    public function storage()
    {
        return $this->hasOne(UserStorage::class,'user_id','id');
    }

    //Check if the user has table access with a specified role
    public function hasRole(array $roles, $table = null)
    {
        $tableAccess = DB::connection('mainDB')->table('access')->where('user_id',$this->id)->get()->toArray();
//        foreach($this->tableRoles as $tableRoles) //TODO Die Rollenprüfung muss komplett neu gemacht werden funktioniert nur mit einer einzigen Tabelle
//        {
//            $tableRole = $tableRoles->pivot->table_role;
//            dd($tableRole);
//            if ($table == null){
//                return in_array($tableRole,$roles,true);
//            } elseif ( in_array($tableRole,$roles,true)){
//                return $tableRoles->pivot->custom_table_id == $table;
//            }
//        }
        foreach ($tableAccess as $access)
        {
            if (in_array($access->table_role,$roles,true) && $access->custom_table_id == $table)
            {
                return true;
            }
        }

        return false;
    }

    //Check if the user has a global role
    public function hasAccess(array $permissions)
    {
        foreach($this->roles as $role)
        {
                if($role->hasPE($permissions))
                {
                    return true;
                }
        }
        return false;
    }
}
