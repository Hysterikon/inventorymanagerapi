<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use const http\Client\Curl\Versions\CURL;

class CustomRow extends Model
{
    protected $connection ='mainDB';
    protected $dateFormat = 'U';


    protected $fillable = [
      'last_user','predecessor','successor','table_guid', 'created_at', 'updated_at','qr_code'
    ];

    public function table()
    {
        return $this->hasOne(CustomTable::class, 'table_guid', 'guid');
    }

    public function columns()
    {
        return $this->belongsToMany( CustomColumn::class, 'custom_columns');
    }

}
