<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Laravel\Lumen\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
#----------
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
#----------
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     */
//    public function render($request, Exception $e)
//    {
//        $rendered = parent::render($request, $e);
//
//        return response()->json([
//            'error' => [
//                'code' => $rendered->getStatusCode(),
//                'message' => $e->getMessage(),
//            ]
//        ], $rendered->getStatusCode());
//    }


      public function render($request, Exception $exception)
       {
           if (config('app.debug')) {
               return parent::render($request, $exception);
           } else {
               $rendered = parent::render($request, $exception);

               return response()->json([
                   'success' => false,
                   'status' => $rendered->getStatusCode(),
                   'message' => $exception->getMessage()
               ], $rendered->getStatusCode());
           }

//           } else if ($exception->request) {
//               return response()->json([
//                   'success' => false,
//                   'status' => $exception->response->getStatusCode(),
//                   'message' => $exception->getMessage()
//               ], $exception->response->getStatusCode());
//           } else {
//               dd($exception);
//               return response()->json([
//                   'success' => false,
//                   'status' => $exception->getCode(),
//                   'message' => $exception->getMessage()
//               ], $exception->getCode());
//           }
       }


     # custom Exception Handler
//    public function render($request, Exception $e)
//    {
//        if (config('app.debug')) {
//          return parent::render($request, $e);
//        }
//        #$status = Response::HTTP_UNPROCESSABLE_ENTITY;
//        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
//        #return response()->json($e);
//        dd($e->getStatusCode());
//        if ($e instanceof HttpResponseException) {
//          $status = Response::HTTP_INTERNAL_SERVER_ERROR;
//        } elseif ($e instanceof MethodNotAllowedHttpException) {
//          $status = Response::HTTP_METHOD_NOT_ALLOWED;
//          $e = new MethodNotAllowedHttpException([], 'HTTP_METHOD_NOT_ALLOWED', $e);
//        } elseif ($e instanceof NotFoundHttpException) {
//          $status = Response::HTTP_NOT_FOUND;
//          $e = new NotFoundHttpException('HTTP_NOT_FOUND', $e);
//        } elseif ($e instanceof AuthorizationException) {
//          $status = Response::HTTP_FORBIDDEN;
//          $e = new AuthorizationException('HTTP_FORBIDDEN', $status);
//        } elseif ($e instanceof \Dotenv\Exception\ValidationException && $e->getResponse()) {
//          $status = Response::HTTP_BAD_REQUEST;
//          $e = new \Dotenv\Exception\ValidationException('HTTP_BAD_REQUEST', $status, $e);
//        } elseif ($e instanceof UnprocessableEntityHttpException or $e->getStatusCode() == 422) {
//          $error = $e->response->original;
//          $status = Response::HTTP_UNPROCESSABLE_ENTITY;
//          $e = new UnprocessableEntityHttpException('HTTP_UNPROCESSABLE_ENTITY', $e);
//          return response()->json([
//            'success' => false,
//            'status' => $status,
//            'error' => $error,
//            'message' => $e->getMessage()
//          ], $status);
//        } elseif ($e) {
//          $e = new HttpException($status, 'HTTP_INTERNAL_SERVER_ERROR');
//        }
//
//        return response()->json([
//          'success' => false,
//          'status' => $status,
//          'message' => $e->getMessage()
//        ], $status);
//    }


}
