<?php

namespace App\Http\Controllers;

use App\CustomColumn;
use App\CustomTable;
use App\License;
use DB;
use App\User;
use App\UserStorage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Exception\PcreException;

/**
 * @group User management
 * @authenticated
 *
 * APIs for managing users
 */
class UserController extends Controller
{
    function checkToken(Request $request)
    {

    }

    ###########################################################
    ##################### ADMIN FUNCTIONS #####################
    ###########################################################

    /**
     * IsAdmin
     *
     * checks if the user is an admin
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     * "success": "true",
     * "admin": "true"
     * }
     *
     * @response {
     * "success": "true",
     * "admin": "false"
     * }
     */
    function isAdmin(Request $request)
    {
        $user = $request->user();
        $roles = $user->roles;
        foreach ($roles as $role) {
            if ($role->slug == 'admin'){
                $res['success'] = true;
                $res['admin'] = true;
                return response()->json($res,200);
            }
        }

        $res['success'] = true;
        $res['admin'] = false;
        return response()->json($res,200);
    }

    /**
     * List all users (Admin)
     * @urlParam partOf required The guid of the associated project
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "users":
     * [
     *      {
     *          "id": 1,
     *          "email": "hennig@debakom.de",
     *          "username": "root",
     *          "registration_approved": 1,
     *          "created_at": 0,
     *          "updated_at": 0
     *      },
     *      {
     *          "id": 2,
     *          "email": "Bob@gmail.com",
     *          "username": "Bob",
     *          "registration_approved": 0,
     *          "created_at": 1603960625,
     *          "updated_at": 0
     *      },
     *      {
     *          "id": 3,
     *          "email": "Max@gmail.com",
     *          "username": "Max",
     *          "registration_approved": 0,
     *          "created_at": 1603960630,
     *          "updated_at": 0
     *      }
     *      ]
     * }
     */
    public function showAllUsers()
    {
        $res['success'] = true;
        $res['users'] = User::all();
        return response()->json($res,200);
    }

    /**
     * Count all users
     *
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     * "success": "true",
     * "user_count": "32"
     * }
     *
     * @response 401{
     * "success": "false",
     * "message": "Unauthorized"
     * }
     */
    public function countAllUsers(Request $request)
    {
        if (Gate::allows('all-access'))
        {
            $res['success'] = true;
            $res['user_count'] = User::all()->count();
            return response()->json($res,200);
        } else {
            $res['success']= false;
            $res['message']= 'Unauthorized';
            return response()->json($res, 401);
        }
    }

    /**
     * Find user
     *
     * @urlParam username required
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     * "success": true,
     * "user": {
     * "id": 1,
     * "email": "hennig@debakom.de",
     * "username": "root",
     * "registration_approved": 1,
     * "created_at": 0,
     * "updated_at": 0,
     * "licenses": []
     * },
     * "license": false
     * }
     *
     * @response {
     * "success": false,
     * "status": 404,
     * "message": "No query results for model [App\\User]."
     * }
     */
    public function findUser($username){
        //TODO: accept all search parameters (email, name, tel)
        if (Gate::allows('all-access')){
            $user = User::where('username', $username)->firstOrFail();
            //load the user tables into the user model
            //$user->tables;

            $res['success'] = true;
            $res['user'] = $user;
            $res['license'] = $user->activeLicense();
            return response()->json($res, 200);
        }
    }

    /**
     * Get all table roles of a user (Testing only)
     *
     * Returns The table and the role connected with it.
     * @authenticated false
     * @urlParam id required The id of the user
     *
     * @response {
     * "0": [
     *   {
     *      "id": 1,
     *      "guid": "747373d6-2abd-4f12-8c3a-733e048913db",
     *      "name": "Table01",
     *      "owner": 2,
     *      "sub_table": 0,
     *      "created_at": 1603960977,
     *      "updated_at": 0,
     *      "deletion_date": 0,
     *      "pivot": {
     *          "user_id": 2,
     *          "custom_table_id": 1,
     *          "table_role": "{\"owner\":true}",
     *          "created_at": "2020-10-29 08:42:57",
     *          "updated_at": "2020-10-29 08:42:57"
     *       }
     *   }
     * ]
     * }
     *
     */
    public function getRoles($id)
    {
        $user = User::findorfail($id);

        return response($user->tableRoles);
    }

    ###############################################################
    ##################### ADMIN FUNCTIONS END #####################
    ###############################################################

    /**
     * Get one user by id
     *
     * A user can only call this on himself, an admin can get other users
     * @urlParam id required The ID of the user
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     * "id": 2,
     * "email": "Bob@gmail.com",
     * "username": "Bob",
     * "registration_approved": 0,
     * "created_at": 1620825003,
     * "updated_at": 0
     * }
     *
     * @response {
     * "success": false,
     * "message": "User does not exist"
     * }
     *
     *
     */
    public function showOneUser($id, Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::where('id',$id)->exists();
        if($user)
        {
           $user = User::find($id);
        } else {
            $res['success'] = false;
            $res['message'] = 'User does not exist';
            return response()->json($res,200);
        }

        #checks if the id and the token matches with the ones of the user
        if($id == $user->id && $token == $user->api_token)
        {
            return response()->json(User::find($id));
        } else { //TODO Unreachable needs refactoring
            $res['success'] = false;
            $res['message'] = 'Invalid permission';
            return response()->json($res,403);
        }

    }

    /**
     * Get one user by token
     *
     * Only returns the user of the token in the request.
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "user":
     * {
     *      "id": 2,
     *      "email": "Bob@gmail.com",
     *      "username": "Bob",
     *      "registration_approved": 0,
     *      "created_at": 1603960625,
     *      "updated_at": 0,
     *      "roles": [
     *      {
     *      "id": 3,
     *      "name": "User",
     *      "slug": "user",
     *      "permissions": "{\"create-table\":true,\"view-account\":true,\"upload-files\":true,\"read-files\":true}",
     *      "pivot": {
     *      "user_id": 2,
     *      "role_id": 3,
     *      "created_at": "2020-10-29 08:37:05",
     *      "updated_at": "2020-10-29 08:37:05"
     *      }
     *      }
     *      ]
     *      }
     * }
     */
    public function showOneUserByRequest(Request $request)
    {
        if ($request->user())
        {
            $user = $request->user();
            $tables =  CustomTable::where('owner',$user->id)->get();
            if($tables){
                $user['tables'] = count($tables);
            } else {
                $user['tables'] = 0;
            }


            $res['success'] = true;
            $res['user'] = $request->user();
        }
        return response($res,200);
    }

    /**
     * Register user (create)
     * @authenticated false
     * @queryParam api_token required Authenticates the user
     * @bodyParam username string required The name of the user
     * @bodyParam email string required The email of the user
     * @bodyParam password string required The SHA512 hashed password of the user
     * @bodyParam password_confirmation string required The SHA512 hashed password confirmation of the user
     *
     * @response 201{
     *  "success": true,
     *   "message": "User successfully registered",
     *   "api_token": "af9b67ed-4e0c-4f5a-866e-77bc33ee58bd",
     *   "data": {
     *   "email": "Bob@gmail.com",
     *   "username": "Bob",
     *   "created_at": 1603961948,
     *   "id": 2
     *   }
     * }
     *
     * @response {
     * "success": false,
     * "message": {
     * "email": [
     * "The email has already been taken."
     * ]
     * }
     * }
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
          'username'=>'required|alpha',
          'email' => 'required|email|unique:userData.users',
          'password' => 'required',
          'password_confirmation' => 'required_with:password|same:password'
        ]);
        if($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }


        $token = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );

        $request['api_token'] = hash('sha256', $token);
        $request['password'] = password_hash($request['password'], PASSWORD_BCRYPT);
        $request['created_at'] = time();

        $user = User::create($request->all());
        $user->roles()->attach([3]);
        $res['success'] = true;
        $res['message'] = 'User successfully registered';
        $res['api_token'] = $token;
        $res['data'] = $user;

        return response()->json($res, 201);
    }

    /**
     * Create a personal Database for the user
     * @queryParam api_token required Authenticates the user
     *
     * @response 201{
     *  "success": true,
     *  "message": "User database was successfully created"
     * }
     * @response 409{
     *  "success":false,
     *  "message":"Database 'ExampleName' already exists"
     * }
     * @response 500{
     *  "success":false,
     *  "message":"Something went wrong, try again!"
     * }
     * @response 401{
     *  "success":false,
     *  "message":"Unauthorized"
     * }
     */
    public function createUserDatabase(Request $request)
    {
        $user = $request->user();
        $user_license = $user->activeLicense();
        $dbName = 'inventorymanager_user_'.$user->id.'';


        if ($user_license != false)
        {
            if($user_license->typeid != '1')
            {
                # create new database
                DB::statement('CREATE DATABASE IF NOT EXISTS '. $dbName);

                # store the new database name in the user_storage table in userDB
                if(!UserStorage::where([['user_id','=', $user->id],['database','=', $dbName]])->exists())
                {
                    $userStorage['user_id'] = $user->id;
                    $userStorage['database'] = $dbName;

                    UserStorage::create($userStorage);
                } else {
                    $res['success']= false;
                    $res['message']= 'Database '.$dbName. ' already exists';

                    return response()->json($res, 409);
                }


                # connect to the new database
                Config::set('database.connections.personalDB',[
                  'driver'    => 'mysql',
                  'host'      => env('DB_HOST'),
                  'port'      => env('DB_PORT'),
                  'database'  => $dbName,
                  'username'  => env('DB_USERNAME'),
                  'password'  => env('DB_PASSWORD'),
                  'charset'   => 'utf8',
                  'collation' => 'utf8_unicode_ci',
                  'prefix'    => '',
                  'strict'    => false,
                ]);

                DB::connection('personalDB')->reconnect();

                # Create all tables in the new database
                try {
                    DB::connection('personalDB')->statement(
                        'CREATE TABLE custom_tables AS
                        SELECT *
                        FROM inventorymanager_maindb.custom_tables');
                    DB::connection('personalDB')->statement('
                        ALTER TABLE custom_tables
                            ADD PRIMARY KEY (id),
                            CHANGE `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                            ADD UNIQUE (guid),
                            ADD FOREIGN KEY (owner) REFERENCES inventorymanager_userData.users(id) ON DELETE CASCADE ON UPDATE CASCADE
                       ');

                    DB::connection('personalDB')->statement('
                        CREATE TABLE access AS
                            SELECT *
                            FROM inventorymanager_maindb.access
                       ');
                    DB::connection('personalDB')->statement('
                        ALTER TABLE access
                            ADD FOREIGN  KEY (user_id) REFERENCES inventorymanager_userData.users(id) ON DELETE CASCADE ON UPDATE CASCADE,
                            ADD FOREIGN KEY (custom_table_id) REFERENCES custom_tables(id) ON DELETE CASCADE ON UPDATE CASCADE,
                            ADD CONSTRAINT unique_index UNIQUE (user_id,custom_table_id)
                       ');
                    DB::connection('personalDB')->statement('
                        CREATE TABLE custom_rows AS
                            SELECT *
                            FROM inventorymanager_maindb.custom_rows
                       ');
                    DB::connection('personalDB')->statement('
                        ALTER TABLE custom_rows
                            ADD PRIMARY KEY (id),
                            CHANGE `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                            ADD FOREIGN KEY (table_guid) REFERENCES custom_tables(guid) ON DELETE CASCADE ON UPDATE CASCADE
                       ');

                    DB::connection('personalDB')->statement('
                        CREATE TABLE custom_columns AS
                            SELECT *
                            FROM inventorymanager_maindb.custom_columns
                       ');
                    DB::connection('personalDB')->statement('
                        ALTER TABLE custom_columns
                            ADD PRIMARY KEY (id),
                            CHANGE `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                            ADD UNIQUE (guid),
                            ADD FOREIGN KEY (data_type_id) REFERENCES inventorymanager_maindb.enumdata(data_id) ON DELETE CASCADE ON UPDATE CASCADE,
                            ADD FOREIGN KEY (row_id) REFERENCES custom_rows(id) ON DELETE CASCADE ON UPDATE CASCADE,
                            ADD FOREIGN KEY (part_of) REFERENCES custom_tables(guid) ON DELETE CASCADE ON UPDATE CASCADE
                       ');
                    DB::connection('personalDB')->statement('
                        CREATE TABLE columnversions AS
                            SELECT *
                            FROM inventorymanager_maindb.columnversions
                       ');
                    DB::connection('personalDB')->statement('
                        ALTER TABLE columnversions
                            ADD PRIMARY KEY (id),
                            CHANGE `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                            ADD FOREIGN KEY (column_guid) REFERENCES custom_columns(guid) ON DELETE CASCADE ON UPDATE CASCADE
                       ');
                } catch (Exception $e){

                }

                # make sure all tables ae empty
                DB::connection('personalDB')->statement("SET foreign_key_checks=0");
                $databaseName = DB::connection('personalDB')->getDatabaseName();
                $tables = DB::connection('personalDB')->select("SELECT * FROM information_schema.tables WHERE table_schema = '$databaseName'");
                foreach ($tables as $table) {
                    $name = $table->TABLE_NAME;
                    DB::connection('personalDB')->table($name)->truncate();
                }
                DB::connection('personalDB')->statement("SET foreign_key_checks=1");

                # check if all tables where created
                if(Schema::connection('personalDB')->hasTable('access', 'column_versions', 'custom_columns','custom_rows','custom_tables'))
                {
                    $res['success']= true;
                    $res['message']= 'User database was successfully created';
                    $res['database'] = $dbName;
                    return response()->json($res, 201);
                } else {
                    DB::statement("DROP DATABASE `{$dbName}`");
                    UserStorage::where([['user_id','=',$user->id],['database','=',$dbName]])->delete();
                    $res['success']= false;
                    $res['message']= 'Something went wrong, try again!';
                    return response()->json($res, 500);
                }

            } else {
                $res['success']= false;
                $res['message']= 'Unauthorized';
            }

            return response()->json($res, 401);
        }

        $res['success']= false;
        $res['message']= 'Unauthorized';

        return response()->json($res, 401);
    }

    /**
     * Delete a personal database
     *
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *   "message": "Database 'ExampleName' deleted"
     * }
     * @response {
     *  "success": true,
     *   "message": "'User' has no database"
     * }
     */
    public function deleteStorage(Request $request)
    {
        $user = $request->user();

        if($user->storage)
        {
            $dbname = $user->storage['database'];
            DB::statement("DROP DATABASE `{$user->storage['database']}`");
            UserStorage::where([['user_id','=',$user->id],['database','=',$dbname]])->delete();

           $res['success'] = true;
           $res['message'] = "Database $dbname deleted";
           return response()->json($res,200);
        } else {
            $res['success'] = false;
            $res['message'] = "$user->username has no database";
            return response()->json($res,200);
        }
    }

    /**
     * Generates a remember token for the caller
     * @queryParam api_token required Authenticates the user
     *
     * @response 201{
     * "success":true,
     * "rememberToken": "274abd30-1f47-4f2b-98ee-9449f306753e"
     * }
     */
    public function generateRememberToken(Request $request){
        $rememberToken = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );

        $user = $request->user();
        $user->update(['remember_token'=> hash('sha256',$rememberToken)]);

        $res['success'] = true;
        $res['rememberToken'] = $rememberToken;
        return response()->json($res,201);
    }

    /**
     * Update a user
     *
     * @urlParam id required The ID of the user
     * @queryParam api_token required Authenticates the user
     * @bodyParam email string optional The email of the user
     * @bodyParam username string optional The name of the user
     * @bodyParam password string The SHA512 hashed password of the user
     * @bodyParam password_confirmation string required_with_password The SHA512 hashed password of the user
     *
     * @response 201{
     * "success":true,
     * "user": {
     *   "id": 2,
     *   "email": "Bob@gmail.com",
     *   "username": "UpdatedName",
     *   "registration_approved": 0,
     *   "created_at": 1603961948,
     *   "updated_at": 1603962686
     *   }
     * }
     */
    public function updateUser($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
          'email'=>'email',
          'username'=> 'alpha',
          'password_confirmation' => 'required_with:password|same:password'
        ]);
        if ($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

//        $this->validate($request,[
//                'email' => 'email',
//                'username' => 'alpha',
//                'password_confirmation' => 'required_with:password|same:password'
//
//        ]);

        $token = hash('sha256',$request->input('api_token'));
        $user = User::where('id',$id)->exists();

        if($user)
        {
            $user = User::find($id);
            if($id == $user->id && $token == $user->api_token || Gate::allows('all-access'))
            {
                if ($request['password'])
                {
                    $request['password'] = password_hash($request['password'], PASSWORD_BCRYPT);
                    $request['updated_at'] = time();
                    $user->update($request->only('email', 'username','updated_at','password'));

                } else {
                    $request['updated_at'] = time();
                    $user->update($request->only('email', 'username','updated_at'));

                }

                $res['success'] = true;
                $res['user'] = $user;
                return response()->json($res, 200);
            } else {
                $res['success'] = false;
                $res['message'] = 'Invalid permission';
                return response()->json($res,403);
            }
        } else {
            $res['success'] = false;
            $res['message'] = 'User does not exist';
            return response()->json($res,200);
        }
    }

    /**
     * Delete a user
     *
     * A user can only call this on himself, an admin can delete others
     *
     * @urlParam id required The ID of the user
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success":true,
     *  "message": "User deleted"
     * }
     * @response 403{
     *  "success": false,
     *  "message": "Invalid permission"
     * }
     *
     */
    public function deleteUser($id, Request $request)
    {

        $token = hash('sha256',$request->input('api_token'));
        # check if the id matches the user who is calling the function
        if(User::where([
            ['api_token','=', $token],
            ['id','=', $id],
        ])->exists() || Gate::allows('all-access')) {
            User::findOrfail($id)->delete();
            $res['success'] = true;
            $res['message'] = 'User deleted';
            return response()->json($res,200);
        } else {
            $res['success'] = false;
            $res['message'] = 'Invalid permission';
            return response()->json($res,403);
        }
    }
}
