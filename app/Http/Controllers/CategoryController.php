<?php

namespace App\Http\Controllers;

use App\CustomTable;
use App\User;
use DB;
use App\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Hashing\MD5Hasher;
use Illuminate\Validation\Rule;


/**
 * @group Category management
 * @authenticated
 *
 * APIs for managing categories
 */
class CategoryController extends Controller
{
    /**
     * List all categories from a user
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "categories": [
     *      {
     *         "id": 1,
     *         "category": "ExampleCategory",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     * ]
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "Keine Kategorien vorhanden"
     * }
     *
     */
    public function getAllCats(Request $request)
    {
        $user_token = hash('sha256',$request->input('api_token'));
        $user = User::where('api_token',$user_token)->first();
        $user_tables = CustomTable::where('owner',$user->id)->get();
        $data = [];
        foreach ($user_tables as $table)
        {
            $arr = $table->categories;
            foreach ($arr as $cat)
            {
                $data[] = $cat['pivot'];
            }
        }
        if (count($data)>0){
            $res['success'] = true;
            $res['categories'] = $data;
        } else {
            $res['success'] = true;
            $res['message'] = 'Keine Kategorien vorhanden';
        }

        return response()->json($res);
    }

    /**
     * List all existing categories
     *
     * The same functionality is implemented under the table management section.
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "categories": [
     *      {
     *         "id": 1,
     *         "category": "ExampleCat",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     * ]
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "Keine Kategorien vorhanden"
     * }
     */
    public function listCats()
    {
        $cats = Category::all();

        if (count($cats)>0){
            $res['success'] = true;
            $res['categories'] = $cats;
        }else{
            $res['success'] = false;
            $res['message'] = 'Keine Kategorien vorhanden';
        }

        return response()->json($res,200);
    }

    /**
     * Get one category
     * @urlParam catID required The ID of the category
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "category": {
     *         "id": 1,
     *         "category": "ExampleCategory",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "Keine Kategorie gefunden"
     * }
     */
    public function getOneCat($catID)
    {
        if (Category::findorfail($catID)){
            $res['success'] = true;
            $res['category'] = Category::find($catID);
        } else {
            $res['success'] = false;
            $res['message'] = 'Keine Kategorie gefunden';
        }
        return response()->json($res);
    }

    /**
     * Create a category
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.category string required The name of the category (alpha)
     * @response 201 {
     *  "success": true,
     *  "categoryItem": {
     *         "id": 1,
     *         "category": "ExampleCategory",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "Kategorien wurden nicht erstellt"
     * }
     */
    public function createCat(Request $request)
    {
        $categories = [];
        $input = ['data'=> $request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
           'data.*.category' => ['required']
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        //loop through all categories in the request and check if it already exists, if not create it
        foreach ($params as $cat)
        {
            if(is_array($cat))
            {
                if(!Category::where('category',$cat['category'])->exists())
                {
                    $categories[] = Category::create($cat);
                }
            }
        }

        if (count($categories)>0){
            $res['success'] = true;
            $res['categories'] = $categories;
        } else {
            $res['success'] = false;
            $res['message'] = 'Kategorien wurden nicht erstellt';
        }
        return response()->json($res, 201);
    }

    /**
     * Update a category
     * @urlParam catID required The ID of the category
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.category string required The name of the category
     * @bodyParam *.cat_id integer required The ID of the category
     *
     * @response {
     *  "success": true,
     *  "category": {
     *         "id": 1,
     *         "category": "ExampleCategory",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "Keine Kategorien aktualisiert"
     * }
     */
    public function updateCat(Request $request) //TODO: Für jeden user eigene Cats und Tags ? User id zu den Einträgen (zurückgestellt)
    {
        $categories = [];
        $input = ['data'=> $request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          '*.*.category' => ['required'],
          '*.*.cat_id' => ['required','numeric',Rule::exists('categories','id')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $cat)
        {
            if(is_array($cat))
            {
                if(Category::where('id',$cat['cat_id'])->exists())
                {
                    $cat = Category::findOrfail($cat['cat_id']);
                    $value = $cat['category'];
                    $cat->update(['category'=> $value, 'updated_at' => time()]);
                    $categories[] = $cat;
                }
            }
        }

        if(count($categories)>0){
            $res['success'] = true;
            $res['categories'] = $categories;
        } else {
            $res['success'] = true;
            $res['message'] = 'Keine Kategorien aktualisiert';
        }

        return response()->json($res, 200);
    }

    /**
     * Delete a category
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.cat_id integer required The ID of the category
     *
     * @response {
     *  "success": true,
     *  "message": "Categories deleted"
     * }
     * @response {
     *  "success": false,
     *  "message": "The provided categories do not exist"
     * }
     */
    public function deleteCat(Request $request)
    {
        $input = ['data'=> $request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.cat_id' => ['required','numeric', Rule::exists('categories','id')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        $counter = 0;

        foreach ($params as $cat)
        {
            if(is_array($cat))
            {
                if(Category::where('id',$cat['cat_id'])->exists())
                {
                    Category::where('id', $cat['cat_id'])->first()->delete();
                    $counter += 1;
                }
            }
        }

        if ($counter > 1) {
            $res['success'] = true;
            $res['message'] = 'Categories deleted';
        } else {
            $res['success'] = false;
            $res['message'] = 'The provided categories do not exist';
        }

        return response($res, 200);
}

}
