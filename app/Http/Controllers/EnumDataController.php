<?php

namespace APP\Http\Controllers;

use App\EnumData;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Hashing\MD5Hasher;

/**
 * @group Enum data management for Admins
 * @authenticated Admin
 *
 * APIs for managing Enum datatypes
 */
class EnumDataController extends Controller
{
    /**
     * List all enum types
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "enums": [
     *      {
     *          "id": 1,
     *          "data_id": 0,
     *          "type": "Titel"
     *      },
     *      {
     *          "id": 2,
     *          "data_id": 1,
     *          "type": "Text"
     *      }
     * ]
     * }
     */
    public function getAllTypes()
    {
        $res['success'] = true;
        $res['enums'] = EnumData::all();
        return response()->json($res,200);
    }

    /**
     * Get one enum types
     * @urlParam data_id required The ID of the enum
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "enum":
     *      {
     *          "id": 1,
     *          "data_id": 0,
     *          "type": "Titel"
     *      }
     * }
     */
    public function getOneType($data_id)
    {
        $res['success'] = true;
        $res['enum'] = EnumData::find($data_id);
        return response()->json($res,200);
    }

    /**
     * Create a new enum
     * @queryParam api_token required Authenticates the user
     * @bodyParam data_id integer required The data_id of the new enum
     * @bodyParam type string required The type of the enum (Text,Zahl, etc.)
     *
     * @response {
     *  "success": true,
     *  "enum":
     *      {
     *          "id": 1,
     *          "data_id": 0,
     *          "type": "Titel"
     *      }
     * }
     * @response {
     *  "success": false,
     *  "message": "This action is unauthorized."
     * }
     */
    public function createType(Request $request)
    {
        $this->validate($request,[
            'data_id' => 'required|integer',
            'type' => 'required|alpha'
        ]);
        if ($request->user()->hasAccess([json_encode(['all-access'=> true])])){

            $type = EnumData::create($request->all());
            $res['success'] = true;
            $res['enum'] = $type;
            return response()->json($res, 201);
        } else {
            $res['success'] = false;
            $res['message'] = 'This action is unauthorized.';
            return response()->json($res, 403);
        }
    }

    /**
     * Update a enum
     * @urlParam data_id required The data_id of the enum
     * @queryParam api_token required Authenticates the user
     * @bodyParam type string required The type of the enum (Text,Zahl, etc.)
     *
     * @response {
     *  "success": true,
     *  "enum":
     *      {
     *          "id": 1,
     *          "data_id": 0,
     *          "type": "Titel"
     *      }
     * }
     * @response {
     *  "success": false,
     *  "message": "This action is unauthorized."
     * }
     */
    public function updateType($data_id, Request $request)
    {
        $this->validate($request,[
            'type' => 'required|alpha'
        ]);
        if ($request->user()->hasAccess([json_encode(['all-access'=> true])])) {

            $type = EnumData::where('data_id', $data_id);

            $type->update($request->all());

            $res['success'] = true;
            $res['enum'] = $type;
            return response()->json($res, 200);
        }  else {
            $res['success'] = false;
            $res['message'] = 'This action is unauthorized.';
            return response()->json($res, 403);
        }
    }

    /**
     * Delete a enum
     * @urlParam data_id required The data_id of the enum
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true
     * }
     * @response {
     *  "success": false,
     *  "message": "This action is unauthorized."
     * }
     * @response 404{
     *  "success": false,
     *  "message": "Enum does not exist."
     * }
     */
    public function deleteType($data_id, Request $request )
    {
        if ($request->user()->hasAccess([json_encode(['all-access'=> true])])) {
            if (EnumData::where('data_id', $data_id)->exists()) {
                EnumData::where('data_id', $data_id)->first()->delete();
                $res['success'] = true;

            }
            abort(404, "Enum does not exist.");
            return response()->json($res, 200);
        } else {
            $res['success'] = false;
            $res['message'] = 'This action is unauthorized.';
            return response()->json($res, 403);
        }
    }
}
