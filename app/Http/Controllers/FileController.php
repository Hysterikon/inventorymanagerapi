<?php

namespace App\Http\Controllers;

use FileStorage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

/**
 * @group File management
 * @authenticated
 *
 * APIs for managing Files
 */
class FileController extends Controller
{
    /**
     * Lists all files for an Admin
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *   "directories": [
     *      "4d5c0c05-8323-4da7-91dc-ba56823c422e",
     *      "e1a3c0ad-f060-4c32-8f73-ebce86ebc711"
     *      ],
     *   "files": [
     *      "4d5c0c05-8323-4da7-91dc-ba56823c422e/1603289690.jpg",
     *      "e1a3c0ad-f060-4c32-8f73-ebce86ebc711/1603289245.jpg"
     *   ]
     * }
     */
    public function index()
    {
        $directories = Storage::disk('local')->allDirectories();
        $files = Storage::disk('local')->allFiles();
        $key = array_search('.gitignore', $files);

        //TODO Errorhandling

        if(isset($key))
        {
            unset($files[$key]);
            foreach ($files as $file)
            {
                $result[] = $file;
            }
            $files = $result;
        }

        $res['success'] = true;
        $res['directories'] = $directories;
        $res['files'] = $files;
        return response()->json($res, 200);
    }

    /**
     * Lists all files from a table
     *
     * The files are returned as a base64 string.
     *
     * @urlParam guid required The guid of the table
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *   "files": [
     *      "base64_string"
     *   ]
     * }
     * @response {
     *  "success":false,
     *  "message": "no files"
     * }
     */
    public function listFiles($guid,Request $request)
    {
        $files = Storage::files($guid);

        if ($files)
        {
            foreach ($files as $file)
            {
                $temp = Storage::disk('local')->get($file);
                $temp = base64_encode($temp);
                $img = 'data:image/' . $request['type'] . ';base64,' . $temp;

                $fileArr[] = $img;
            }
            $res['success'] = true;
            $res['files'] = $fileArr;
            return response()->json($res, 200);
        } else {
            $res['success'] = false;
            $res['message'] = 'no files';
            return response()->json($res, 200);
        }

    }

    /**
     * Upload a file
     *
     * @queryParam api_token required Authenticates the user
     * @bodyParam guid string required The guid of the table
     * @bodyParam content string required The base64 encoded image/or file
     * @bodyParam type string required The image/file type (jpg, png)
     *
     * @response {
     *  "success": true,
     *   "filename": "name"
     * }
     */
    public function postFile(Request $request)
    {
        //TODO Errorhandling

        $this->validate($request,[
          'guid'=>'required|exists:custom_tables',
          'content'=>'required',
          'type' => 'required'
        ]);
        $img = base64_decode($request['content']);
        $name = ''.time().'.'.$request['type'];
        if(Storage::disk('local')->exists($request['guid'])){
            //Storage::disk('local')->put(  $name,$img);
            Storage::disk('local')->put($request['guid'].'/'.$name,$img);
            //Storage::disk('local')->move($name, $request['guid'].'/'.$name);
            $res['success'] = true;
            $res['filename'] = $name;
            return response()->json($res, 201);
        } else {
            Storage::disk('local')->makeDirectory($request['guid']);
            Storage::disk('local')->put($request['guid'].'/'.$name,$img);
//            Storage::disk('local')->put($name,$img);
//            Storage::disk('local')->move($name, $request['guid'].'/'.$name);

            $res['success'] = true;
            $res['filename'] = $name;
            return response()->json($res, 201);
        }

    }

    /**
     * Get one file
     *
     * The file is returned as a base64 string.
     *
     * @urlParam name required The name of the file
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *   "image": [
     *      "base64_string"
     *   ]
     * }
     * @response  403{
     *  "success":false
     * }
     */
    public function readFile($name,Request $request)
    {
        $path = $request->input('guid').'/'.$name.'.'.$request->input('ext');
        if (Storage::disk('local')->exists($path)){
            $temp = Storage::disk('local')->get($path);
            $temp = base64_encode($temp);
            $img = 'data:image/' . $request['type'] . ';base64,' . $temp;
            $res['success'] = true;
            $res['image'] = $img;
            return response()->json($res,200);
        } else {
            $res['success'] = false;
            return response()->json($res,403);
        }
    }

    /**
     * Update a file (NOT IMPLEMENTED)
     *
     * @queryParam api_token required Authenticates the user
     */
    public function putFile(Request $request, $id)
    {

    }

    /**
     * Download a file
     * @urlParam name required The name of the file
     * @queryParam api_token required Authenticates the user
     * @queryParam ext required The file extension
     * @queryParam guid required The guid of the table
     */
    public function download($name,Request $request)
    {
        $filepath = storage_path('app/'.$request->input('guid').'/'.$name.'.'.$request->input('ext'));

        return response()->download($filepath); //TODO check with the mobile app, might only work on browsers
//        return Storage::download($filepath, 'test');
    }

    /**
     * Delete a file
     * @urlParam name required The name of the file
     * @queryParam api_token required Authenticates the user
     * @bodyParam type string required The extension of the file
     * @bodyParam guid string required The guid of the table
     *
     * @response {
     * "success": true,
     * "message": "File deleted"
     * }
     * @response 403 {
     * "success": false,
     * "message": "File does not exist."
     * }
     */
    public function deleteFile($name,Request $request)
    {
        $this->validate($request,[
          'type' => 'required',
          'guid' => 'required|exists:custom_tables'
        ]);
        $path = $request['guid'].'/'.$name.'.'.$request['type'];
        if (Storage::disk('local')->exists($path)){
            Storage::disk('local')->delete($path);
            $res['success'] = true;
            $res['message'] = "File deleted";
            return response()->json($res,200);
        } else {
            $res['success'] = false;
            $res['message'] = "File does not exist.";
            return response()->json($res,404);
        }
    }
}
