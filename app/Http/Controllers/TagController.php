<?php

namespace App\Http\Controllers;


use App\CustomTable;
use DB;
use App\Tag;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Hashing\MD5Hasher;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * @group Tag management
 * @authenticated
 *
 * APIs for managing tags
 */
class TagController extends Controller
{
    /**
     * List all tags from a table
     *
     * The same functionality is implemented under the table management section.
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "tags": [
     *      {
     *         "id": 1,
     *         "tag": "ExampleTag",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     * ]
     * }
     *
     * @response {
     *  "success": true,
     *  "message": "Keine Tags vorhanden"
     * }
     */
    public function getAllTags(Request $request)
    {
        $user_token = hash('sha256',$request->input('api_token'));
        $user = User::where('api_token',$user_token)->first();
        $user_tables = CustomTable::where('owner',$user->id)->get();
//        $arr = array();
        $data = [];
        foreach ($user_tables as $table)
        {
            $arr = $table->tags;
//            dd($arr[0]['pivot']);

            foreach ($arr as $tag)
            {
                $data[] = $tag['pivot'];
            }

        }

        if (count($data) > 0){
            $res['success'] = true;
            $res['tags'] = $data;
        } else {
            $res['success'] = true;
            $res['message'] = 'Keine Tags vorhanden';
        }

        return response()->json($res);
    }

    /**
     * List all existing tags
     *
     * The same functionality is implemented under the table management section.
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "tags": [
     *      {
     *         "id": 1,
     *         "tag": "ExampleTag",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     * ]
     * }
     *
     * @response {
     *  "success": true,
     *  "message": "Keine Tags vorhanden"
     * }
     */
    public function listTags()
    {
        $tags = Tag::all();

        if (count($tags) > 0){
            $res['success'] = true;
            $res['tags'] = $tags;
        } else {
            $res['success'] = false;
            $res['message'] = 'Keine Tags vorhanden';
        }

        return response()->json($res,200);
    }

    /**
     * Get one tag
     * @urlParam tagID required The ID of the tag
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "tag":
     *      {
     *         "id": 1,
     *         "tag": "ExampleTag",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     *
     * }
     *
     * @response {
     *  "success": true,
     *  "message": "Kein Tag gefunden"
     * }
     */
    public function getOneTag($tagID)
    {

        if (Tag::find($tagID)){
            $res['success'] = true;
            $res['tag'] = Tag::find($tagID);
        } else {
            $res['success'] = false;
            $res['message'] = 'Kein Tag gefunden';
        }
        return response()->json($res);
    }

    /**
     * Create a tag
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.*.tag string required The name of the tag (alpha)
     * @response 201 {
     *  "success": true,
     *  "tag":
     *      {
     *         "id": 1,
     *         "tag": "ExampleTag",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     *
     * }
     *
     * @response {
     *  "success": true,
     *  "message": "Tags wurden nicht erstellt"
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "tag": [
     *      "Error message."
     *  ]
     *  }
     * }
     */
    public function createTag(Request $request)
    {
        $tags = [];
        $input = ['data'=> $request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.tag' => ['required']
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $tag)
        {
            if(is_array($tag))
            {
                if(!Tag::where('tag',$tag['tag'])->exists())
                {
                    $tags[] = Tag::create($tag);
                }
            }
        }

        if(count($tags)>0){
            $res['success'] = true;
            $res['categories'] = $tags;
        } else {
            $res['success'] = false;
            $res['message'] = 'Tags wurden nicht erstellt';
        }
        return response()->json($res, 201);
//
//        if(Tag::where('tag',$request['tag'])->exists())
//        {
//            return response('Tag ('. $request['tag'] .') already exists.', 404);
//        } else {
//
//            $tag = Tag::create($request->all());
//            $res['success'] = true;
//            $res['tagItem'] = $tag;
//            return response()->json($res, 201);
//        }
    }

    /**
     * Update a tag
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.*.tag string required The name of the tag
     * @bodyParam *.*.tag_id integer required The ID of the tag
     *
     * @response {
     *  "success": true,
     *  "tag":
     *      {
     *         "id": 1,
     *         "tag": "ExampleTag",
     *         "created_at": "1603898348",
     *         "updated_at": "1603898348"
     *      }
     *
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "tag": [
     *      "Error message."
     *  ]
     *  }
     * }
     *
     * @response {
     *  "success": true,
     *  "message": "Keine Tags aktualisiert"
     * }
     */
    public function updateTag( Request $request)
    {
        $tags = [];
        $input = ['data'=> $request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          '*.*.tag' => ['required'],
          '*.*.tag_id' => ['required','numeric',Rule::exists('tags','id')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $tagItem)
        {
            if(is_array($tagItem))
            {
                if(Tag::where('id',$tagItem['tag_id'])->exists())
                {
                    $tag = Tag::findOrfail($tagItem['tag_id']);
                    $value = $tagItem['tag'];
                    $tag->update(['tag'=> $value, 'updated_at' => time()]);
                    $tags[] = $tag;
                }
            }
        }

        if (count($tags)>0){
            $res['success'] = true;
            $res['tags'] = $tags;
        } else {
            $res['success'] = false;
            $res['message'] = 'Keine Tags aktualisiert';
        }
        return response()->json($res, 200);
    }

    /**
     * Delete a tag
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.*.tag_id integer required The ID of the tag
     *
     * @response {
     *  "success": true,
     *  "message": "Tags deleted"
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "tag_id": [
     *      "Error message."
     *  ]
     *  }
     * }
     */
    public function deleteTag(Request $request)
    {
        $input = ['data'=> $request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.tag_id' => ['required','numeric',Rule::exists('tags','id')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $tag)
        {
            if(is_array($tag))
            {
                if(Tag::where('id',$tag['tag_id'])->exists())
                {
                    Tag::where('id',$tag['tag_id'])->first()->delete();
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Tags deleted';
        return response($res, 200);
    }

}
