<?php

namespace App\Http\Controllers;

use App\Type;
use App\User;
use DB;
use App\License;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Hashing\MD5Hasher;

/**
 * @group License management
 * @authenticated
 *
 * APIs for managing user licenses
 */
class LicenseController extends Controller
{
    ###########################################################
    ##################### ADMIN FUNCTIONS #####################
    ###########################################################

    /**
     * List all licenses for an Admin
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "licenses":[
     *  {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  },
     * {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  }
     * ]
     * }
     */
    public function showAllLicensesAdmin()
    {
        $res['success'] = true;
        $res['licenses'] = License::all();
        return response()->json($res, 200);
    }

    /**
     * Create a new license for a user
     * @queryParam api_token required Authenticates the user
     * @bodyParam type_id integer required The id of the license type
     * @bodyParam user_id integer required The id of the user
     * @bodyParam expiration_date integer required The expiration date of the license in unix time
     *
     * @response {
     *  "success": true,
     *  "license":
     *  {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  }
     *}
     * @response 422{
     *  "success": false,
     *  "status": 422,
     *  "message": "The given data was invalid."
     * }
     */
    public function createLicenseAdmin(Request $request)
    {
        $this->validate($request,[
          'user_id' => 'required|exists:userData.users,id',
          'type_id'=>'required',
          'expiration_date'=>'required'
        ]);

        $request['guid'] = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
          mt_rand( 0, 0xffff ),
          mt_rand( 0, 0x0fff ) | 0x4000,
          mt_rand( 0, 0x3fff ) | 0x8000,
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
        $request['activation_date'] = time();
        $request['license_name'] = Type::where('id',$request['type_id'])->value('license_name');
        $request['price'] = Type::where('id',$request['type_id'])->value('price');
        $license = License::create($request->all());

        $res['success'] = true;
        $res['license'] = $license;
        return response()->json($res, 201);
    }

    ###############################################################
    ##################### ADMIN FUNCTIONS END #####################
    ###############################################################

    /**
     * List all licenses from a user
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "licenses":[
     *  {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  },
     * {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  }
     * ]
     * }
     */
    public function showAllLicenses(Request $request)
    {
        $user_id = User::where('api_token',  hash('sha256',$request->input('api_token')))->value('id');
        $license = License::where('user_id', $user_id)->get();
        $res['success'] = true;
        $res['licenses'] = $license;
        return response()->json($res, 200);
    }

    /**
     * Get one license
     * @urlParam id required The ID of the license
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "license":
     *  {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  }
     *}
     * @response 404{
     *  "success": false,
     *  "status": 404,
     *  "message": "No query results for model [App\\License] 1"
     * }
     */
    public function showOneLicense($id)
    {
        $res['success'] = true;
        $res['license'] = License::findorfail($id);
        return response()->json($res,200);
    }

    /**
     * Create a new license
     * @queryParam api_token required Authenticates the user
     * @bodyParam type_id integer required The id of the license type
     * @bodyParam expiration_date integer required The expiration date of the license in unix time
     *
     * @response {
     *  "success": true,
     *  "license":
     *  {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  }
     *}
     * @response 422{
     *  "success": false,
     *  "status": 422,
     *  "message": "The given data was invalid."
     * }
     */
    public function createLicense(Request $request)
    {
        $this->validate($request,[
                'type_id'=>'required',
                'expiration_date'=>'required'
        ]);
        $token =  hash('sha256',$request->input('api_token'));

        $request['guid'] = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
        $request['activation_date'] = time();
        $request['license_name'] = Type::where('id',$request['type_id'])->value('license_name');
        $request['user_id'] = User::where('api_token',$token)->value('id');
        $request['price'] = Type::where('id',$request['type_id'])->value('price');
        $license = License::create($request->all());

        $res['success'] = true;
        $res['license'] = $license;
        return response()->json($res, 201);
    }

    /**
     * Update a License
     *
     * It is currently possible to update every parameter of the license.
     * @urlParam id required The ID of the license
     * @queryParam api_token required Authenticates the user
     * @bodyParam type_id integer required The id of the license type
     * @bodyParam expiration_date integer required The expiration date of the license in unix time
     *
     *
     * @response {
     *  "success": true,
     *  "license":
     *  {
     *     "id": 1,
     *     "user_id": 2,
     *     "type_id": 2,
     *     "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
     *     "license_name": "Advanced_License",
     *     "price": 150,
     *     "activation_date": 1603891368,
     *     "expiration_date": 1605052800
     *  }
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "type_id": [
     * "The type id must be a number."
     * ]
     * }
     * }
     */
    public function updateLicense($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
          'type_id' => 'numeric',
          'expiration_date' => 'numeric'
        ]);
        if ($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }
        $license = License::findOrfail($id);

        $license->update($request->only('type_id','expiration_date'));

        $res['success'] = true;
        $res['license'] = $license;
        return response()->json($res, 200);
    }

    /**
     * Delete a license
     * @urlParam id required The ID of the license
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true
     * }
     * @response 404{
     *  "success": false,
     *  "status": 404,
     *  "message": "No query results for model [App\\License] 65"
     * }
     */
    public function deleteLicense($id)
    {
        $license = License::findorfail($id);
        $license->delete();
        $res['success'] = true;
        return response($res,200);
    }
}
