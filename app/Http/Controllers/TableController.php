<?php

namespace App\Http\Controllers;

use App\Category;
use App\CustomRow;
use App\Tag;
use DB;
use App\User;
use App\CustomTable;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * @group Table management
 * @authenticated
 *
 * APIs for managing custom tables
 */
class TableController extends Controller
{
    ###########################################################
    ##################### ADMIN FUNCTIONS #####################
    ###########################################################

    /**
     * ADMIN returns the total amount of existing tables
     *
     * @response {
     *  "success": true,
     *  "table_count": 5
     * }
     *
     * @response 401{
     *  "success": false,
     *  "message": "Unauthorized"
     * }
     */
    public function countAllTables()
    {
        if (Gate::allows('all-access'))
        {
            $res['success'] = true;
            $res['table_count'] = CustomTable::all()->count();
            return response()->json($res,200);
        } else {
            $res['success']= false;
            $res['message']= 'Unauthorized';
            return response()->json($res, 401);
        }
    }

    /**
     * ADMIN find specific projects by a searchtearm
     * @urlParam searchTerm required The search term to be searched for in the database
     *
     * @response {
     *  "success": true,
     *  "projects": [
     *  {
     *         "id": 1,
     *          "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *          "name": "Table01",
     *          "owner": 2,
     *          "sub_table": 0,
     *          "created_at": 1603878742,
     *          "updated_at": 1603878796,
     *          "deletion_date": 0
     *  },
     *  {
     *         "id": 1,
     *          "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *          "name": "Table01",
     *          "owner": 2,
     *          "sub_table": 0,
     *          "created_at": 1603878742,
     *          "updated_at": 1603878796,
     *          "deletion_date": 0
     *  }
     * ]
     * }
     * @response 404{
     *  "success": false,
     *  "status": 404,
     *  "message":  "No search results."
     * }
     */
    public function findProjects($searchTerm){

        if(Gate::allows('all-access')){
            $projects = CustomTable::where('name','=', $searchTerm)->orWhere('owner','=', $searchTerm)->get();

            if (count($projects) !== 0){
                $res['succes'] = true;
                $res['projects'] = $projects;
            } else {
                abort(404, "No search results.");
            }

            return response()->json($res, 200);
        }
    }

    ###############################################################
    ##################### ADMIN FUNCTIONS END #####################
    ###############################################################


    /**
     * List all tables of a user
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "tables": [
     *      {
     *         "id": 1,
     *          "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *          "name": "Table01",
     *          "owner": 2,
     *          "sub_table": 0,
     *          "created_at": 1603878742,
     *          "updated_at": 1603878796,
     *          "deletion_date": 0
     *   },
     *   {
     *         "id": 1,
     *          "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *          "name": "Table01",
     *          "owner": 2,
     *          "sub_table": 0,
     *          "created_at": 1603878742,
     *          "updated_at": 1603878796,
     *          "deletion_date": 0
     *   }
     * ]
     * }
     */
    public function getAllTables(Request $request) //TODO Errorhandling
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $user_id = User::where('api_token',$token)->value('id');

        // auch geteilte Projekte anzeigen
        $access = DB::table('access')->where('user_id',$user_id)->pluck('custom_table_id')->toArray();
        $data = CustomTable::find($access);

        // nur die Tables von einem Nutzer ausgeben
//        $arr = CustomTable::where('owner',$user_id)->where('sub_table', '=', 0)->get();

        $res['success'] = true;
        $res['tables'] = $data;
        return response()->json($res,200);
    }

    /**
     * Get one Table
     * @urlParam guid required The guid of the table
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "table": [
     *     {
     *      "id": 1,
     *      "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *      "name": "Table01",
     *      "owner": 2,
     *      "sub_table": 0,
     *      "created_at": 1603878742,
     *      "updated_at": 1603878796,
     *      "deletion_date": 0,
     *      "columns": [
     *          {
     *              "id": 1,
     *              "guid": "03224ca0-243a-4197-a44a-735d7b6a9333",
     *              "value": "TEST6",
     *              "data_type_id": 1,
     *              "last_user": "2",
     *              "column_order": 0,
     *              "row_id": 1,
     *              "part_of": "b90de04e-bf14-4a78-9e72-62d1895de049"
     *          }
     *       ]
     *     }
     *    ]
     * }
     */
    public function getOneTable($guid, Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        DB::connection('mainDB')->reconnect();

        $table = CustomTable::where('guid',$guid)->value('id');
        $table = CustomTable::find($table);
            $array = array();
            $array[] = $table;
            foreach ($table->columns as $data) {
                # $array[] = array($data);
            }

        $res['success'] = true;
        $res['table'] = $array;
        return response()->json($res,200);
    }

    /**
     * List updated tables
     * @urlParam count required The amount of the tables that should be returned
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "tables": [
     *      {
     *         "id": 1,
     *          "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *          "name": "Table01",
     *          "owner": 2,
     *          "sub_table": 0,
     *          "created_at": 1603878742,
     *          "updated_at": 1603878796,
     *          "deletion_date": 0
     *   },
     *   {
     *         "id": 1,
     *          "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *          "name": "Table01",
     *          "owner": 2,
     *          "sub_table": 0,
     *          "created_at": 1603878742,
     *          "updated_at": 1603878796,
     *          "deletion_date": 0
     *   }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message": "Nothing updated yet"
     * }
     * @response {
     *  "success": false,
     *  "message": "This user has no tables"
     * }
     */
    public function getUpdatedTables($count,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $result [] = null;
        $user = $request->user();
        $tableRoles = $user->tableRoles;

        if(count($tableRoles) !== 0)
        {
            foreach ($tableRoles as $role){
                $results [] = $role->pivot;
            }
            foreach($results as $result){
                $tmp = $result;
                $tableIDs [] = $tmp->custom_table_id;
            }
            foreach ($tableIDs as $id){
                if (CustomTable::where('updated_at', '>', 0)->where('id',$id)->exists()){
                    $tables [] = CustomTable::where('updated_at', '>', 0)->where('id',$id)->first();
                }
            }

            if (isset($tables)){
                $sorted = array_column($tables,'updated_at');
                array_multisort($sorted, SORT_DESC,$tables);
                if (count($tables) > $count){
                    $tables = array_slice($tables, 0, $count);
                }
                $res['success'] = true;
                $res['tables'] = $tables;

            } else {
                $res['success'] = false;
                $res['message'] = 'Nothing updated yet';
            }

        } else {

            $res['success'] = false;
            $res['message'] = 'This user has no tables';

        }

        return response()->json($res, 200);
    }

    /**
     * Create a new table
     * @queryParam api_token required Authenticates the user
     * @bodyParam name string required The name of the table
     * @bodyParam sub_table bool required Indicates if the table is a sub table
     *
     * @response 201{
     *  "success": true,
     *  "table": {
     *   "name": "Table01",
     *   "sub_table": "0",
     *   "guid": "b7d7d34b-fe58-4aef-9f5f-7b29080d14d4",
     *   "created_at": 1603897048,
     *   "owner": 2,
     *   "id": 2
     *   }
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "name": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    public function createTable(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
              'driver'    => 'mysql',
              'host'      => env('DB_HOST'),
              'port'      => env('DB_PORT'),
              'database'  => $userdb->database,
              'username'  => env('DB_USERNAME'),
              'password'  => env('DB_PASSWORD'),
              'charset'   => 'utf8',
              'collation' => 'utf8_unicode_ci',
              'prefix'    => '',
              'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()]; //TODO: warum das data array ??
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.name' => ['required'],
            'data.*.sub_table' => ['required','boolean']
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data)
        {
            if (is_array($data))
            {
                $data['guid'] = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                    mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
                    mt_rand( 0, 0xffff ),
                    mt_rand( 0, 0x0fff ) | 0x4000,
                    mt_rand( 0, 0x3fff ) | 0x8000,
                    mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
                );
                $data['created_at'] = time();
                $data['owner'] = $user->id;
                $table = CustomTable::create($data);
                $tables[] = $table;

                if ($userdb != null)
                {
                    $user->tableRoles($userdb->database)->attach($table,['table_role' => json_encode(['owner' => true,])]);
                } else {
                    $user->tableRoles()->attach($table,['table_role' => json_encode(['owner' => true,])]);
                }
            }

        }

//        $table = CustomTable::create($request->all());


        $res['success'] = true;
        $res['tables'] = $tables;
        return response()->json($res,201);
    }

    /**
     * Give access to other users
     * @urlParam guid required The guid of the table
     * @queryParam api_token required Authenticates the user
     * @bodyParam email string required The email of the user that receives access
     * @bodyParam role string required The name of the role the user should get
     *
     * @response {
     *  "success": true,
     *  "message": "Editor role given to Mustermann"
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "email": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    //TODO: Anderen nutzern den zugriff auf tabellen in der perosnalDB ermöglichen
    public function giveAccess($guid, Request $request)
    {
        $data = [$request->all()];
        $data = $data[0];
        unset($data['api_token']);
        $validator = Validator::make($data,[
          '*.email'=> 'required|email',
          '*.role'=> 'required|exists:userData.roles,name'
        ]);
        if ($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

        $msg = [];
        foreach ($data as $share){
            if (is_array($share)) {
                if ( DB::table('custom_tables')->where('guid',$guid)->exists() && User::where('email', $share['email'])->exists()){
                    $Tid = DB::table('custom_tables')->where('guid',$guid)->value('id');
                    $Uid = User::where('email', $share['email'])->value('id');
                    $user = User::find($Uid);
                    //$table = CustomTable::find($Tid);
                    $tableRole = json_encode([$share['role'] => true,]);
                    //$user->tableRoles()->sync($table,['tableRole' => json_encode([$request['role'] => true,])]);
                    $user->tableRoles()->sync([$Tid => ['table_role' => $tableRole]],false);

                    array_push($msg, ''. $share['role']. ' role given to '. $user->username .'');
                }
            }
        }

        $res['success'] = true;
        $res['message'] = $msg;
        return response()->json($res, 201);
    }

    /**
     * Update a table
     * @queryParam api_token required Authenticates the user
     * @bodyParam name string required The new name of the table
     * @bodyParam guid string required The guid of the table
     *
     * @response {
     *  "success": true,
     *  "table": {
     *      "id": 2,
     *      "guid": "b7d7d34b-fe58-4aef-9f5f-7b29080d14d4",
     *      "name": "Updated Name",
     *      "owner": 2,
     *      "sub_table": 0,
     *      "created_at": 1603897048,
     *      "updated_at": 1603897455,
     *      "deletion_date": 0
     *      }
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "guid": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    public function updateTable(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        DB::connection('mainDB')->reconnect();

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.guid' =>['required',Rule::exists('custom_tables','guid')], //TODO ist das data array nötig ?
            'data.*.name' => ['required']
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }
        $params = $request->all();
        foreach ($params as $data)
        {
            if (is_array($data))
            {
                if(CustomTable::where([['guid','=',$data['guid']],['owner','=',$user->id]]))
                {
                    if (Gate::allows('editor',[$data['guid'],'table'])){
                        $data['updated_at'] = time();
                        $table = CustomTable::where('guid',$data['guid'])->first();
                        $table->update($data);
                        $tables[] = $table;
                    }
                }
            }
        }

        $res['success'] = true;
        $res['tables'] = $tables;
        return response()->json($res,200);
    }

    /**
     * Add tag to a table
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.*.tag_id integer required The ID of the tag
     * @bodyParam *.*.table_guid string required The guid of the table
     *
     * @response {
     *  "success": true,
     *  "message": "Tags added"
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "tag": [
     *  "Error message."
     *  ]
     *  }
     * }
     *
     * @response 403{
     *  "success": false,
     *  "message": "Tag not found"
     * }
     */
    public function addTag(Request $request)
    {

        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.tag_id' =>[Rule::requiredIf(array_key_exists('tag_id',$input)),Rule::exists('personalDB.tags','id')],
            'data.*.tag' => [Rule::requiredIf(array_key_exists('tag',$input)),Rule::exists('personalDB.tags', 'tag')],
            'data.*.table_guid' => ['required',Rule::exists('custom_tables','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }
        $params = $request->all();
        foreach ($params as $data)
        {
            if (is_array($data))
            {
                if (Gate::allows('editor',[$data['table_guid'],'table'])){
                    $table = CustomTable::where('guid',$data['table_guid'])->first();

                    if(array_key_exists('tag_id',$data)){
                        if(!DB::connection('personalDB')->table('tag_tables')->where([['tag_id','=',$data['tag_id']],['table_guid','=',$data['table_guid']]])->exists())
                        {
                            $tmp['updated_at'] = time();
                            $table->update($tmp);
                            $table->tags()->sync([$data['tag_id']=>['table_guid'=> $data['table_guid']]],false);
                        }
                    } else {
                        $tmp['updated_at'] = time();
                        $table->update($tmp);
                        $table->tags()->sync([Tag::where('tag',$data['tag'])->value('id') =>['table_guid'=> $data['table_guid']]],false);
                    }
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Tags added';
        return response()->json($res,200);
    }

    /**
     * Remove tag from a table
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.*.tag_id integer required The ID of the tag
     * @bodyParam *.*.table_guid string required The guid of the table
     *
     * @response {
     *  "success": true,
     *  "message": "Tags removed"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "tag": [
     *  "Error message."
     *  ]
     *  }
     * }
     * @response 403{
     *  "success": false,
     *  "message": "Tag not found"
     * }
     */
    public function removeTag(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.tag_id' =>['required',Rule::exists('personalDB.tags','id')],
            'data.*.table_guid' => ['required',Rule::exists('custom_tables','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data)
        {
            if(is_array($data))
            {
                if (Gate::allows('editor',[$data['table_guid'],'table'])){
                    $table = CustomTable::where('guid',$data['table_guid'])->first();
                    if(DB::connection('personalDB')->table('tag_tables')->where([['tag_id','=',$data['tag_id']],['table_guid','=',$data['table_guid']]])->exists())
                    {
                        $tmp['updated_at'] = time();
                        $table->update($tmp);
                        DB::connection('personalDB')->table('tag_tables')->where([['tag_id','=',$data['tag_id']],['table_guid','=',$data['table_guid']]])->delete();
                    } else {
                        $res['success'] = false;
                        $res['message'] = 'Tag not found';
                        return response()->json($res,403);
                    }
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Tags removed';
        return response()->json($res,200);
    }

    /**
     * List tags from a table
     * @urlParam guid required The guid of the associated table
     * @queryParam api_token required Authenticates the user
     *
     * @response    {
     *  "success": true,
     *  "tags": [
     *      {
     *      "tag_id": 1,
     *      "tag_name": "ExampleTag",
     *      "table_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
     *      }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message": "No tags found"
     * }
     */
    public function listTags($guid,Request  $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));

        $tags = DB::table('tag_tables')->where('table_guid',$guid)->get();

        foreach ($tags as $tag){
            $infos = Tag::where('id', $tag->tag_id)->first();
            $tmp['tag_id'] = $tag->tag_id;
            $tmp['tag_name'] = $infos['tag'];
            $tmp['table_guid'] = $tag->table_guid;
            $message[] = $tmp;
        }

        if(isset($message))
        {
            $res['success'] = true;
            $res['tags'] = $message;
            return response()->json($res,200);
        } else {
            $res['success'] = false;
            $res['message'] = 'No tags found';
            return response()->json($res,200);
        }

    }

    /**
     * Add a category to a table
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.*.cat_id integer required The ID of the category
     * @bodyParam *.*.table_guid string required The guid of the table
     *
     * @response {
     *  "success": true,
     *  "message": "Category added"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "cat_id": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    public function addCategory(Request $request)
    {

        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.cat_id' =>[Rule::requiredIf(array_key_exists('cat_id',$input)),Rule::exists('personalDB.categories','id')],
            'data.*.category' => [Rule::requiredIf(array_key_exists('cat',$input)),Rule::exists('personalDB.categories', 'category')],
            'data.*.table_guid' => ['required',Rule::exists('custom_tables','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data)
        {
            if (is_array($data))
            {
                if (Gate::allows('editor',[$data['table_guid'],'table'])){
                    $table = CustomTable::where('guid',$data['table_guid'])->first();

                    if(array_key_exists('cat_id',$data))
                    {
                        if(!DB::connection('personalDB')->table('category_tables')->where([['category_id','=',$data['cat_id']],['table_guid','=',$data['table_guid']]])->exists())
                        {
                            $tmp['updated_at'] = time();
                            $table->update($tmp);
                            $table->categories()->sync([$data['cat_id']=>['table_guid'=> $data['table_guid']]],false);
                        }
                    } else {
                        $tmp['updated_at'] = time();
                        $table->update($tmp);
                        $table->categories()->sync([Category::where('category',$data['category'])->value('id') => ['table_guid'=> $data['table_guid']]],false);
                    }
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Categories added';
        return response()->json($res,200);
    }

    /**
     * Remove a category from a table
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.*.cat_id integer required The ID of the category
     * @bodyParam *.*.table_guid string required The guid of the table
     *
     * @response {
     *  "success": true,
     *  "message": "Category removed"
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "cat_id": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    public function removeCategory(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.cat_id' =>['required',Rule::exists('personalDB.categories','id')],
            'data.*.table_guid' => ['required',Rule::exists('custom_tables','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data)
        {
            if(is_array($data))
            {
                if (Gate::allows('editor',[$data['table_guid'],'table'])){
                    $table = CustomTable::where('guid',$data['table_guid'])->first();
                    if(DB::connection('personalDB')->table('category_tables')->where([['category_id','=',$data['cat_id']],['table_guid','=',$data['table_guid']]])->exists())
                    {
                        //TODO Error response
                        $tmp['updated_at'] = time();
                        $table->update($tmp);
                        DB::connection('personalDB')->table('category_tables')->where([['category_id','=',$data['cat_id']],['table_guid','=',$data['table_guid']]])->delete();
                    }
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Categories removed';
        return response()->json($res,200);
    }

    /**
     * List categories from a table
     * @urlParam guid required The guid of the associated table
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "categories": [
     *          {
     *      "category_id": 1,
     *      "category_name": "ExampleCategory",
     *      "table_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
     * }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message": "No categories found"
     * }
     */
    public function listCategories($guid,Request  $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));

//        $table = CustomTable::where('guid',$request['table_guid'])->first();
        $cats = DB::table('category_tables')->where('table_guid',$guid)->get();

        foreach ($cats as $cat){
            $infos = Category::where('id',$cat->category_id)->first();
            $tmp['category_id'] = $cat->category_id;
            $tmp['category_name'] = $infos['category'];
            $tmp['table_guid'] = $cat->table_guid;
            $message[] = $tmp;
        }


        if(isset($message))
        {
            $res['success'] = true;
            $res['categories'] = $message;
            return response()->json($res,200);
        } else {
            $res['success'] = false;
            $res['message'] = 'No categories found';
            return response()->json($res,200);
        }

    }

    /**
     * Delete a table
     * @queryParam api_token required Authenticates the user
     * @bodyParam array.*.guid string required The guid of the table
     *
     * @response {
     *  "success": true
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "guid": [
     *  "Error message."
     *  ]
     *  }
     * }
     *
     * @response 401{
     *  "success": false,
     *  "status": 401,
     *  "message":  "Unauthorized"
     * }
     *
     * @response 403{
     *  "success": false,
     *  "status": 403,
     *  "message":  "Table does not exist."
     * }
     */
    public function deleteTable(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        # switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $input = [ 'data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.guid' => 'required|exists:custom_tables',
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

        $tables = $request->all();
        foreach ($tables as $table){
            if(is_array($table)){
                    $guid = $table['guid'];
//                    dd(Gate::allows('owner',[$guid,'table']));
                    if(CustomTable::where('guid',$guid)->exists()) {

                        # checks if the user is authorized because no special authorization is set in the route.
                        if(Gate::allows('owner',[$guid,'table'])){
                            CustomTable::where('guid', $guid)->first()->delete();
                            Storage::disk('local')->deleteDirectory($guid);
                            CustomRow::where('table_guid',$guid)->delete();
                        } else {
                            abort(401, 'Unauthorized');
                        }
                    } else {
                        abort(404, 'Table does not exist.');
                    }
            }
        }

        $res['success'] = true;
        return response($res,200);
    }


}
