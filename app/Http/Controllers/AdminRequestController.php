<?php

namespace App\Http\Controllers;

use App\adminRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

/**
 * @group Support/Request management
 * @authenticated
 *
 * APIs for managing support requests
 */
class AdminRequestController extends Controller
{
    public function createRequest(Request $request){
        $validator = Validator::make($request->all(),[
          'message' => 'required|max:255'
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors()->all();
            return response()->json($res, 422);//TODO: 422 for all false responses from validators
        }

        $request['status'] = false;
        $request['user_id'] = $request->user()->id;
        $adminRequest = adminRequest::create($request->all());

        $res['success'] = true;
        return response()->json($res, 201);
    }

    public function listRequests(){
        $adminRequests = adminRequest::all();

        if (count($adminRequests) > 0){
            $res['success'] = true;
            $res['requests'] = $adminRequests;
        } else {
            $res['success'] = false;
            $res['message'] = 'Keine requests vorhanden';
        }

        return response()->json($res,200);
    }

    public function getRequest(Request $request){
        $validator = Validator::make($request->all(),[
          'id' => 'required|exists:usesrData.admin_requests'
        ]);
        if($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors()->all();
            return response()->json($res,422);
        }

        $res['success'] = true;
        $res['request'] = adminRequest::findorFail($request['id']);
        return response()->json($res,200);
    }

    public function updateRequest(Request $request){
        $validator = Validator::make($request->all(),[
          'id' => 'required|exists:userData.admin_requests',
          'status' => 'required|boolean'
        ]);
        if($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors()->all();
            return response()->json($res,422);
        }

        $adminRequest = adminRequest::findorFail($request['id']);
        $adminRequest->update($request->only('status'));

        $res['success'] = true;
        $res['request'] = "Request {$request['id']} status set {$request['status']}";
        return response()->json($res,200);
    }

    public function deleteRequest(Request $request){
        $input = [$request->all()];
        unset($input['api_token']);
        $validator = Validator::make($input,[
          '*.id' => 'required|exists:userData.admin_requests'
        ]);
        if ($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors()->all();
            return response()->json($res,422);
        }

        foreach ($input as $a_request){
            if (is_array($a_request)){
                adminRequest::findorFail($a_request['id'])->delete();
            }
        }

        $res['success'] = true;
        return response()->json($res,204);
    }
}
