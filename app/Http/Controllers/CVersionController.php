<?php

namespace APP\Http\Controllers;

use App\CustomColumn;
use App\User;
use App\Columnversion;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


/**
 * @group Column version management
 * @authenticated
 *
 * APIs for managing column versions
 */
class CVersionController extends Controller
{

    /**
     * List all verisons of a column
     * @urlParam guid required The guid of the associated column
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "versions": [
     *      {
     *          "id": 1,
     *          "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "date": 1603877430,
     *          "created_by": 2
     *   },
     *   {
     *          "id": 2,
     *          "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "date": 1603877430,
     *          "created_by": 2
     *   }
     * ]
     * }
     */
    public function getAllVersions($guid,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $column = CustomColumn::where('guid',$guid)->first(); //TODO Errorhandling
        $versions = $column->versions;
        $res['success'] = true;
        $res['versions'] = $versions;
        return response()->json($res,200);
    }

    /**
     * List one version of a column
     * @urlParam guid required The guid of the column
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "version": [
     *      {
     *          "id": 1,
     *          "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "date": 1603877430,
     *          "created_by": 2
     *   }
     * ]
     * }
     */
    public function getOneVersion($id,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $version = Columnversion::where('guid',$id)->first(); //TODO Errorhandling

        $res['success'] = true;
        $res['version'] = $version;
        return response()->json($res, 200);
    }

    /**
     * Create a new version
     * @urlParam guid required The guid of the associated table
     * @queryParam api_token required Authenticates the user
     * @bodyParam  column_guid required The guid of the column
     * @bodyParam value string required The value of the column
     *
     * @response 201{
     *  "success": true,
     *  "version": [
     *      {
     *       "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *       "value": "ExampleValue",
     *       "date": 1603877430,
     *       "created_by": 2
     *      }
     *      ]
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "value": [
     *  "Error message."
     *  ]
     *  }
     * }
     * @response 403{
     *  "success": false,
     *  "message": "Forbidden"
     * }
     * @response 404{
     *  "success": false,
     *  "message": "Column does not exist"
     * }
     */
    public function createVersion(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.value' => ['required'],
          'data.*.column_guid' => ['required',Rule::exists('custom_columns','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

        $data = $request->all();
        foreach ($data as $version) {
            if (is_array($version)) {
                if(CustomColumn::where('guid',$version['column_guid'])->exists()) {

                    #checks if the user is authorized because no special authorization is set in the route.
                    if(Gate::allows('editor',[$version['column_guid'],'column'])) {
                        $version['date'] = time();
                        $version['created_by'] = $data['api_token'];
                    } else {
                        $res['success'] = false;
                        $res['message'] = 'Forbidden';
                        return response($res, 403);
                    }

                } else {
                    $res['success'] = false;
                    $res['message'] ='Column does not exist.';
                    return response($res, 404);
                }

                $version = Columnversion::create($version);
                $res['success'] = true;
                $res['version'] = $version;
                return response()->json($res, 201);
            }
        }
        $res['success'] = false;
        return response()->json($res, 200);
    }

    /**
     * Update an existing version
     * @urlParam id required The ID of the version
     * @queryParam api_token required Authenticates the user
     * @bodyParam value string required The new value of the version
     *
     * @response {
     *  "success": true,
     *  "version ": [
     *      {
     *          "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "date": 1603877430,
     *          "created_by": 2
     *   }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "value": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    public function updateVersion($id, Request $request) //TODO: Annahme von Arrays ermöglichen (Zurückgestellt)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $validator = Validator::make($request,[
          'value' => 'required'
//            'date' => 'required'
        ]);
        if($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

        $version = Columnversion::where('id',$id)->first();

        $version->update($request->all());
        $res['success'] = true;
        $res['version'] = $version;
        return response()->json($res, 200);
    }

    /**
     * Delete a version
     * @urlParam id required The id of the version
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "message": "Versions deleted"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "id": [
     *  "Error message."
     *  ]
     *  }
     * }
     * @response 404{
     *  "success": false,
     *  "message": "Version does not exist."
     * }
     */
    public function deleteVersion(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $input = [ 'data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.id' => 'required|exists:columnversions',
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

        $versions = $request->all();
        foreach ($versions as $version)
        {
            if(is_array($version))
            {
                if(Columnversion::where('id',$version['id'])->exists()) {

                    # checks if the user is authorized because no special authorization is set in the route.
                    if (Gate::allows('editor',[$version['id'],'version'])) //TODO: GATE für alle funktionen die Arrays annehmen einbauen
                    {
                        Columnversion::where('id',$version['id'])->first()->delete();
                    }

                } else {
                    abort(404, 'Version does not exist.');
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Versions deleted';
        return response($res, 200);
    }
}
