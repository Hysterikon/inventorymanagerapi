<?php

namespace App\Http\Controllers;

use DB;
use App\Type;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Hashing\MD5Hasher;

/**
 * @group License type management
 * @authenticated
 *
 * APIs for managing license types
 */
class TypeController extends Controller
{
    /**
     * List all license types
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "types": [
     *   {
     *      "id": 1,
     *      "license_name": "Basic_License",
     *      "price": 100,
     *      "max_table_shares": 0,
     *      "max_tables": 10,
     *      "column_pattern_size": 5,
     *      "column_entry_max_amount": 10,
     *      "column_version_control": 0,
     *      "column_version_lifespan": 0,
     *      "max_table_size": 25,
     *      "max_table_lifespan": 322464
     *   },
     *   {
     *      "id": 2,
     *      "license_name": "Advanced_License",
     *      "price": 150,
     *      "max_table_shares": 5,
     *      "max_tables": 25,
     *      "column_pattern_size": 15,
     *      "column_entry_max_amount": 25,
     *      "column_version_control": 1,
     *      "column_version_lifespan": 24523,
     *      "max_table_size": 50,
     *      "max_table_lifespan": 54345245
     *   }
     * ]
     * }
     *
     */
    public function getAllTypes()
    {
        $res['success'] = true;
        $res['types'] = Type::all();
        return response()->json($res,200);
    }

    /**
     * Get one license type
     * @urlParam id required The id of the type
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "type":
     *   {
     *      "id": 1,
     *      "license_name": "Basic_License",
     *      "price": 100,
     *      "max_table_shares": 0,
     *      "max_tables": 10,
     *      "column_pattern_size": 5,
     *      "column_entry_max_amount": 10,
     *      "column_version_control": 0,
     *      "column_version_lifespan": 0,
     *      "max_table_size": 25,
     *      "max_table_lifespan": 322464
     *   }
     * }
     *
     * @response 404{
     * "success": false,
     * "status": 404,
     * "message": "No query results for model [App\\Type] 14"
     * }
     */
    public function getOneType($id)
    {
        $res['success'] = true;
        $res['type'] = Type::findorfail($id);
        return response()->json($res,200);
    }

    /**
     * Create a new license type
     * @queryParam api_token required Authenticates the user
     * @bodyParam license_name string required The name of the license
     * @bodyParam price integer required The price of the license
     * @bodyParam max_table_shares integer The max amount of table shares
     * @bodyParam max_tables integer required The max allowed tables
     * @bodyParam column_pattern_size integer required The max amount of allowed columns
     * @bodyParam column_entry_max_amount integer required
     * @bodyParam column_version_control bool required Indicates if the license has version control for columns
     * @bodyParam column_version_lifespan integer required The savetime of column versions
     * @bodyParam max_table_size integer required The allowed table size (rows)
     * @bodyParam max_table_lifespan integer required The allowed table savetime
     *
     * @response {
     *  "success": true,
     *  "type":
     *   {
     *      "id": 1,
     *      "license_name": "Basic_License",
     *      "price": 100,
     *      "max_table_shares": 0,
     *      "max_tables": 10,
     *      "column_pattern_size": 5,
     *      "column_entry_max_amount": 10,
     *      "column_version_control": 0,
     *      "column_version_lifespan": 0,
     *      "max_table_size": 25,
     *      "max_table_lifespan": 322464
     *   }
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "max_tables": [
     *      "Error message."
     *  ]
     *  }
     * }
     */
    public function createType(Request $request)
    {
        $validator = Validator::make($request->all(),[
          'license_name'=>'required|alpha_dash',
          'price'=>'required',
          'max_tables'=>'required|numeric',
          'column_pattern_size'=>'required|numeric',
          'column_entry_max_amount'=>'required|numeric',
          'column_version_control'=>'required|bool',
          'column_version_lifespan'=>'required|numeric',
          'max_table_size'=>'required|numeric',
          'max_table_lifespan'=>'required|numeric'
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }
        $type = Type::create($request->all());

        $res['success'] = true;
        $res['type'] = $type;
        return response()->json($res, 201);
    }

    /**
     * Update a license type
     * @urlParam id required The id of the type
     * @queryParam api_token required Authenticates the user
     * @bodyParam license_name string required The name of the license
     * @bodyParam price integer required The price of the license
     * @bodyParam max_table_shares integer The max amount of table shares
     * @bodyParam max_tables integer required The max allowed tables
     * @bodyParam column_pattern_size integer required The max amount of allowed columns
     * @bodyParam column_entry_max_amount integer required
     * @bodyParam column_version_control bool required Indicates if the license has version control for columns
     * @bodyParam column_version_lifespan integer required The savetime of column versions
     * @bodyParam max_table_size integer required The allowed table size (rows)
     * @bodyParam max_table_lifespan integer required The allowed table savetime
     *
     * @response {
     *  "success": true,
     *  "type":
     *   {
     *      "id": 1,
     *      "license_name": "Basic_License",
     *      "price": 100,
     *      "max_table_shares": 0,
     *      "max_tables": 10,
     *      "column_pattern_size": 5,
     *      "column_entry_max_amount": 10,
     *      "column_version_control": 0,
     *      "column_version_lifespan": 0,
     *      "max_table_size": 25,
     *      "max_table_lifespan": 322464
     *   }
     * }
     *
     * @response {
     *  "success": false,
     *  "message":  {
     *  "max_tables": [
     *      "Error message."
     *  ]
     *  }
     * }
     */
    public function updateType($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
          'license_name'=>'required|alpha_dash',
          'price'=>'required',
          'max_tables'=>'required|numeric',
          'column_pattern_size'=>'required|numeric',
          'column_entry_max_amount'=>'required|numeric',
          'column_version_control'=>'required|bool',
          'column_version_lifespan'=>'required|numeric',
          'max_table_size'=>'required|numeric',
          'max_table_lifespan'=>'required|numeric'
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }
        $type = Type::findOrfail($id);

        $type->update($request->all());
        $res['success'] = true;
        $res['type'] = $type;
        return response()->json($res, 200);
    }

    /**
     * Delete a license type
     * @urlParam id required The id of the type
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true
     * }
     *
     * @response 404{
     * "success": false,
     * "status": 404,
     * "message": "No query results for model [App\\Type] 14"
     * }
     */
    public function deleteType($id)
    {
        $type = Type::findorfail($id);
        $type->delete();

        $res['success'] = true;
        return response($res,200);
    }


}
