<?php

namespace App\Http\Controllers;

use App\Category;
use App\Columnversion;
use App\CustomTable;
use App\Tag;
use App\User;
use DB;
use App\CustomColumn;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * @group Column management
 * @authenticated
 *
 * APIs for managing custom columns
 */
class ColumnController extends Controller
{
    /**
     * List all columns of a table
     * @urlParam partOf required The guid of the associated project
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "columns": [
     *      {
     *          "id": 1,
     *          "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "data_type_id": 1,
     *          "last_user": "1",
     *          "column_order": 1,
     *          "row_id": 1,
     *          "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
     *   },
     *   {
     *          "id": 1,
     *          "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "data_type_id": 1,
     *          "last_user": "1",
     *          "column_order": 1,
     *          "row_id": 1,
     *          "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
     *   }
     * ]
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "No columns found"
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "No table with the guid: eaa8721a-31d0-4027-812f-ec199176960a"
     * }
     */
    public function getAllColumns($partOf,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();


        if (CustomTable::where('guid',$partOf)->first()){
            $col =  CustomColumn::where('part_of',$partOf)->get();
            if($col){
                $res['success'] = true;
                $res['columns'] = CustomColumn::where('part_of',$partOf)->get();
                return response()->json($res,200);
            } else {
                $res['success'] = false;
                $res['message'] = 'No columns found';
                return response()->json($res,200);
            }
        } else {
            $res['success'] = false;
            $res['message'] = 'No table with the guid: '. $partOf;
            return response()->json($res,200);
        }
    }

    /**
     * List one column of a Project
     * @urlParam guid required The guid of the column
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "column": [
     *      {
     *          "id": 1,
     *          "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "data_type_id": 1,
     *          "last_user": "1",
     *          "column_order": 1,
     *          "row_id": 1,
     *          "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
     *   }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message": "No columns found"
     * }
     *
     * @response {
     *  "success": false,
     *  "message": "No table with the guid: eaa8721a-31d0-4027-812f-ec199176960a"
     * }
     */
    public function getOneColumn($guid,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();


        if (CustomColumn::where('guid',$guid)->exists()){
            $col =  CustomColumn::where('guid',$guid)->first();
            if($col){
                $res['success'] = true;
                $res['columns'] = CustomColumn::where('guid',$guid)->first();
                return response()->json($res,200);
            } else {
                $res['success'] = false;
                $res['message'] = 'No columns found';
                return response()->json($res,200);
            }
        } else {
            $res['success'] = false;
            $res['message'] = 'No column with the guid: '. $guid;
            return response()->json($res,200);
        }
    }

    /**
     * Create a new column
     * @urlParam guid required The guid of the associated project
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.value string required The value of the column
     * @bodyParam *.data_type_id integer required The ID of the datatype of the column
     * @bodyParam *.column_order integer required The ID of the column order
     * @bodyParam *.row_id integer required The ID of the row of the column
     *
     * @response {
     *  "success": true,
     *  "columns": [
     *      {
     *          "value": "Test",
     *          "data_type_id": 1,
     *          "column_order": 2,
     *          "row_id": 3,
     *          "guid": "e107d146-b0d9-4786-ac83-4af26dc4d5ac",
     *          "part_of": "eaa8721a-31d0-4027-812f-ec199176960a",
     *          "last_user": 2
     *
     *   }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "row_id": [
     * "Error message."
     * ]
     * }
     * }
     */
    public function  createColumn($guid,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $input = [ 'data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.data_type_id' => ['required'],
            'data.*.column_order' => ['required','numeric'],
            'data.*.row_id' => ['required','numeric',Rule::exists('custom_rows','id')->where('table_guid',$guid)]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }
        $params = $request->all();
        foreach ($params as $column) {
            if(is_array($column)) {

                if(CustomColumn::where([['part_of','=',$guid],['value','=',$column['value']],['row_id','=',1]])->exists()) {
                    abort(422, 'No duplicates in the first row allowed! ("'.$column['value'].'" at row: 1)');
                } elseif(CustomColumn::where([['part_of','=',$guid],['column_order','=',$column['column_order']],['row_id','=',$column['row_id']]])->exists()) {
                    abort(422, 'Duplicate of Column order: '.$column['column_order'].' at Row: '.$column['row_id']);
                }

                if (Gate::allows('editor',[$guid,'table'])){
                    $column['guid'] = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                      mt_rand(0, 0xffff),
                      mt_rand(0, 0x0fff) | 0x4000,
                      mt_rand(0, 0x3fff) | 0x8000,
                      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
                    );
                    $column['part_of'] = $guid;
                    $column['last_user'] = $request->user()->id;

                    $columns[] = $column;
                    $res['success'] = true;
                    $res['column'] = $columns;
                    CustomColumn::create($column);
                }
            }

        }
        $table = CustomTable::where('guid',$guid)->first();
        $table->update(['updated_at'=> time()]);

        return response()->json($res, 201);

    }

    /**
     * Update an existing column
     * @urlParam partOf required The guid of the associated project
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.value string required The value of the column
     * @bodyParam *.data_type_id integer required The ID of the datatype of the column
     * @bodyParam *.column_order integer required The ID of the column order
     * @bodyParam *.row_id integer required The ID of the row of the column
     *
     * @response {
     *  "success": true,
     *  "columns": [
     *      {
     *          "value": "Test",
     *          "data_type_id": 1,
     *          "column_order": 2,
     *          "row_id": 3,
     *          "guid": "e107d146-b0d9-4786-ac83-4af26dc4d5ac",
     *          "part_of": "eaa8721a-31d0-4027-812f-ec199176960a",
     *          "last_user": 2
     *
     *   }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "guid": [
     * "Error message."
     * ]
     * }
     * }
     */
    public function updateColumn($partOf,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $input = [ 'data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.guid' => ['required'],
          'data.*.data_type_id' => ['numeric'],
          'data.*.value' => ['required'],
          'data.*.column_order' => ['required','numeric'],
          'data.*.row_id' => ['required','numeric',Rule::exists('custom_rows','id')->where('table_guid',$partOf)]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }
        $columnsArr = $request->all();

        foreach ($columnsArr as $newColumn){
            if (is_array($newColumn)){
                if (Gate::allows('editor',[$partOf,'table'])){
                    $oldColumn = CustomColumn::where('guid',$newColumn['guid'])->first();
                    #create a version for the changed column
                    $version['date'] = time();
                    $version['created_by'] = $request->user()->id;
                    $version['value'] = $oldColumn['value'];
                    $version['column_guid'] = $oldColumn['guid'];

                    Columnversion::create($version);
                    #version created
                    $oldColumn->update(['value' => $newColumn['value'], 'column_order' => $newColumn['column_order'],'row_id'=> $newColumn['row_id']]);
                }
            }
        }
        $table = CustomTable::where('guid',$partOf)->first();
        $table->update(['updated_at'=> time()]);

        $res['success'] = true;
        return response()->json($res, 200);
    }

    /**
     * Add tag to a column
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.tag_id integer required The ID of the tag
     * @bodyParam *.column_guid string required The guid of the column
     *
     * @response {
     *  "success": true,
     *  "message": "Tag added"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "tag_id": [
     * "Error message."
     * ]
     * }
     * }
     */
    public function addTag(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.tag_id' =>['required',Rule::exists('personalDB.tags','id')],
            'data.*.column_guid' => ['required',Rule::exists('custom_columns','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data) {
            if (is_array($data)) {
                if (Gate::allows('editor',[$data['column_guid'],'column'])){
                    $column = CustomColumn::where('guid',$data['column_guid'])->first();

                    if(!DB::connection('personalDB')->table('tag_columns')->where([['tag_id','=',$data['tag_id']],['column_guid','=',$data['column_guid']]])->exists())
                    {
                        $tmp['updated_at'] = time();
                        $column->update($tmp);
                        $column->tags()->sync([$data['tag_id']=>['column_guid'=> $data['column_guid']]],false);
                    }
                }
            }

        }

        $res['success'] = true;
        $res['message'] = 'Tags added';
        return response()->json($res,200);
    }

    /**
     * Remove tag from a column
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.tag_id integer required The ID of the tag
     * @bodyParam *.column_guid string required The guid of the column
     *
     * @response {
     *  "success": true,
     *  "message": "Tag removed"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "tag_id": [
     * "Error message."
     * ]
     * }
     * }
     */
    public function removeTag(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.tag_id' =>['required',Rule::exists('personalDB.tags','id')],
            'data.*.column_guid' => ['required',Rule::exists('custom_columns','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data) {
            if (is_array($data)) {
                if (Gate::allows('editor',[$data['column_guid'],'column'])) {
                    $column = CustomColumn::where('guid', $data['column_guid'])->first();

                    if (DB::connection('personalDB')->table('tag_columns')->where([
                      ['tag_id', '=', $data['tag_id']], ['column_guid', '=', $data['column_guid']]
                    ])->exists()) {
                        $tmp['updated_at'] = time();
                        $column->update($tmp);
                        DB::connection('personalDB')->table('tag_columns')->where([
                          ['tag_id', '=', $data['tag_id']], ['column_guid', '=', $data['column_guid']]
                        ])->delete();
                    }
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Tags removed';
        return response()->json($res,200);
    }

    /**
     * List tags from a column
     * @urlParam guid required The guid of the associated column
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "tags": [
     *       {
     *      "tag_id": 1,
     *      "tag_name": "ExampleTag",
     *      "column_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
     * }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message": "No tags found"
     * }
     */
    public function listTags($guid,Request  $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));

        $tags = DB::table('tag_columns')->where('column_guid',$guid)->get();

        foreach ($tags as $tag){
            $infos = Tag::where('id', $tag->tag_id)->first();
            $tmp['tag_id'] = $tag->tag_id;
            $tmp['tag_name'] = $infos['tag'];
            $tmp['column_guid'] = $tag->column_guid;
            $message[] = $tmp;
        }

        if(isset($message))
        {
            $res['success'] = true;
            $res['tags'] = $message;
            return response()->json($res,200);
        } else {
            $res['success'] = false;
            $res['message'] = 'No tags found';
            return response()->json($res,200);
        }

    }

    /**
     * Add a category to a column
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.cat_id integer required The ID of the category
     * @bodyParam *.column_guid string required The guid of the column
     *
     * @response {
     *  "success": true,
     *  "message": "Category added"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "cat_id": [
     * "Error message."
     * ]
     * }
     * }
     */
    public function addCategory(Request $request)
    {

        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.cat_id' =>['required',Rule::exists('personalDB.tags','id')],
            'data.*.column_guid' => ['required',Rule::exists('custom_columns','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data) {
            if (is_array($data)) {
                if (Gate::allows('editor',[$data['column_guid'],'column'])){
                    $column = CustomColumn::where('guid',$data['column_guid'])->first();

                    if(!DB::connection('personalDB')->table('category_columns')->where([['category_id','=',$data['cat_id']],['column_guid','=',$data['column_guid']]])->exists()) {
                        $tmp['updated_at'] = time();
                        $column->update($tmp);
                        $column->categories()->sync([$data['cat_id']=>['column_guid'=> $data['column_guid']]],false);
                    }
                }
            }

        }

        $res['success'] = true;
        $res['message'] = 'Categories added';
        return response()->json($res,200);
    }

    /**
     * Remove a category from a column
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.cat_id integer required The ID of the category
     * @bodyParam *.column_guid string required The guid of the column
     *
     * @response {
     *  "success": true,
     *  "message": "Category removed"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "cat_id": [
     * "Error message."
     * ]
     * }
     * }
     */
    public function removeCategory(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }

        $input = ['data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
            'data.*.cat_id' =>['required',Rule::exists('personalDB.tags','id')],
            'data.*.column_guid' => ['required',Rule::exists('custom_columns','guid')]
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }

        $params = $request->all();
        foreach ($params as $data)
        {
            if (is_array($data))
            {
                if (Gate::allows('editor',[$data['column_guid'],'column'])){
                    $column = CustomColumn::where('guid',$data['column_guid'])->first();

                    if(DB::connection('personalDB')->table('category_columns')->where([['category_id','=',$data['cat_id']],['column_guid','=',$data['column_guid']]])->exists())
                    {
                        $tmp['updated_at'] = time();
                        $column->update($tmp);
                        DB::connection('personalDB')->table('category_columns')->where([['category_id','=',$data['cat_id']],['column_guid','=',$data['column_guid']]])->delete();
                    }
                }
            }
        }

        $res['success'] = true;
        $res['message'] = 'Categories removed';
        return response()->json($res,200);
    }

    /**
     * List categories from a column
     * @urlParam guid required The guid of the associated column
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "categories": [
     *      {
     *      "category_id": 1,
     *      "category_name": "ExampleCategory",
     *      "column_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
     *      }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message": "No categories found"
     * }
     * @response {
     *  "success": false,
     *  "message": "No categories found"
     * }
     */
    public function listCategories($guid,Request  $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));

//        $table = CustomTable::where('guid',$request['table_guid'])->first();
        $cats = DB::table('category_columns')->where('column_guid',$guid)->get();

        foreach ($cats as $cat){
            $infos = Category::where('id',$cat->category_id)->first();
            $tmp['category_id'] = $cat->category_id;
            $tmp['category_name'] = $infos['category'];
            $tmp['column_guid'] = $cat->column_guid;
            $message[] = $tmp;
        }


        if(isset($message))
        {
            $res['success'] = true;
            $res['categories'] = $message;
            return response()->json($res,200);
        } else {
            $res['success'] = false;
            $res['message'] = 'No categories found';
            return response()->json($res,200);
        }

    }

    /**
     * Delete a column
     * @urlParam guid required The guid of the associated table
     * @queryParam api_token required Authenticates the user
     * @bodyParam *.guid string required The guid of the column
     *
     * @response {
     *  "success": true
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     * "guid": [
     * "Error message."
     * ]
     * }
     * }
     */
    public function deleteColumn (Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;
        $deletedColumns[] = null;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $input = [ 'data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.guid' => 'required|exists:custom_columns',

        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }
        $columns = $request->all();
        try {
            if (CustomColumn::where('guid',$columns[0]['guid'])->exists()){
                $partOf = CustomColumn::where('guid',$columns[0]['guid'])->first()->value('part_of');
                $table = CustomTable::where('guid',$partOf)->first();
                $table->update(['updated_at'=> time()]);
            }
        } catch (Exception $e) {

        }
        foreach ($columns as $column){
            if(is_array($column)){
                if (Gate::allows('editor',[$column['guid'],'column'])){
                    $guid = $column['guid'];
                    if(CustomColumn::where('guid',$guid)->exists()) {
                        $column = CustomColumn::where('guid',$guid)->first();
                        if ($column->row->predecessor === null){
                            $deletedColumns[] = CustomColumn::where([['part_of','=',$column->part_of],['column_order','=',$column->column_order]])->get();
                            CustomColumn::where([['part_of','=',$column->part_of],['column_order','=',$column->column_order]])->delete();
                        } else {
                           $deletedColumns[] = $column;
                           $column->delete();
                        }
                    } else {
                        abort(404, 'Column does not exist: ' . $guid . '');
                    }
                }
            }
        }

        $res['success'] = true;
        $res['deleted_columns'] = $deletedColumns;
        return response($res,200);
    }
}
