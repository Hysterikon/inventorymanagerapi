<?php

namespace App\Http\Controllers;

use App\CustomColumn;
use App\CustomRow;
use App\CustomTable;
use App\EnumData;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;

/**
 * @group Row management
 * @authenticated
 *
 * API's for managing custom rows
 */
class RowController extends Controller
{
    ###########################################################
    ##################### ADMIN FUNCTIONS #####################
    ###########################################################


    ###############################################################
    ##################### ADMIN FUNCTIONS END #####################
    ###############################################################

    /**
     * List all rows of a table
     * @urlParam partOf required The guid of the associated project
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "rows": [
     *     {
     *      "id": 1,
     *      "last_user": 2,
     *      "predecessor": null,
     *      "successor": 2,
     *      "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *      "created_at": "1603878749",
     *      "updated_at": "1603878749"
     *     },
     *     {
     *      "id": 2,
     *      "last_user": 2,
     *      "predecessor": 1,
     *      "successor": null,
     *      "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *      "created_at": "1603878749",
     *      "updated_at": "1603878749"
     *     }
     * ],
     *  "total": 2
     * }
     * @response 403{
     *  "success": false,
     *  "status": 403,
     *  "message": "This action is unauthorized."
     * }
     */
    public function getAllRows($partOf,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
              'driver'    => 'mysql',
              'host'      => env('DB_HOST'),
              'port'      => env('DB_PORT'),
              'database'  => $userdb->database,
              'username'  => env('DB_USERNAME'),
              'password'  => env('DB_PASSWORD'),
              'charset'   => 'utf8',
              'collation' => 'utf8_unicode_ci',
              'prefix'    => '',
              'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $rows = CustomRow::where('table_guid', $partOf)->get();

        $res['success'] = true;
        $res['rows'] = $rows;
        $res['total'] = count($rows);
        return response()->json($res,200);
    }

    /**
     * Get one row of a table, also returns the total number of rows and the number of the returned row
     * @urlParam row_id required The ID of the row
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "row":
     *     {
     *      "id": 1,
     *      "last_user": 2,
     *      "predecessor": null,
     *      "successor": 2,
     *      "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *      "created_at": "1603878749",
     *      "updated_at": "1603878749"
     *     },
     *  "number": 3,
     *  "total": 10,
     *  "columns": {
     *          "id": 1,
     *          "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *          "value": "ExampleValue",
     *          "data_type_id": 1,
     *          "last_user": "1",
     *          "column_order": 1,
     *          "row_id": 1,
     *          "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
     *      }
     * }
     * @response 403{
     *  "success": false,
     *  "status": 403,
     *  "message": "This action is unauthorized."
     * }
     */
    public function getOneRow($row_id, Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
              'driver'    => 'mysql',
              'host'      => env('DB_HOST'),
              'port'      => env('DB_PORT'),
              'database'  => $userdb->database,
              'username'  => env('DB_USERNAME'),
              'password'  => env('DB_PASSWORD'),
              'charset'   => 'utf8',
              'collation' => 'utf8_unicode_ci',
              'prefix'    => '',
              'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $row = CustomRow::where('id', $row_id)->first();
        $rows = CustomRow::where([['table_guid','=', $row->table_guid],['predecessor','!=',null]])->get();
        $number = 0;
        foreach ($rows as $el){
            $number += 1;
            if ($el->id == $row->id){
                break;
            }
        }

        $firstRow = CustomRow::where('id',CustomRow::where([['table_guid','=', $row->table_guid],['predecessor','=',null]])->first()->successor)->first();
        $lastRow = CustomRow::where([['table_guid','=', $row->table_guid],['successor','=',null]])->first();

        $res['success'] = true;
        $res['row'] = $row;
        $res['first_entry'] = $firstRow->id;
        $res['last_entry'] =  $lastRow->id;
        $res['number'] = $number;
        $res['total'] = count($rows);
        if($request->input('with_columns') == true){
            $res['columns'] = CustomColumn::where([['part_of','=', $row->table_guid],['row_id','=',$row_id]])->get();
        }
        return response()->json($res,200);
    }

    /**
     * get the count of all rows
     * @urlParam tableGuid required The guid of the table
     * @queryParam api_token required Authenticates the user
     *
     * @response {
     *  "success": true,
     *  "total": 2
     * }
     * @response 403{
     *  "success": false,
     *  "status": 403,
     *  "message": "This action is unauthorized."
     * }
     */
    public function getRowNumber(Request $request,$table_guid){
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'),
                'port'      => env('DB_PORT'),
                'database'  => $userdb->database,
                'username'  => env('DB_USERNAME'),
                'password'  => env('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $rows = CustomRow::where('table_guid', $table_guid)->get();

        $res['success'] = true;
        $res['total'] = count($rows);
        return response()->json($res,200);
    }

    /**
     * Create new rows
     * @urlParam partOf required The guid of the associated table
     * @queryParam api_token required Authenticates the user
     *
     * @bodyParam table_guid string required The guid of teh associated table
     * @bodyParam count integer required The amount of new rows that are being created
     *
     * @response 201{
     *  "success": true,
     *  "rows": [
     *  {
     *  "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *  "last_user": 2,
     *  "updated_at": "1603893764",
     *  "created_at": "1603893764",
     *  "id": 11,
     *  "predecessor": 10
     *  },
     *  {
     *  "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *  "last_user": 2,
     *  "updated_at": "1603893764",
     *  "created_at": "1603893764",
     *  "id": 12,
     *  "predecessor": 11
     *  }
     * ]
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "table_guid": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    public function createRow(Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;
        $newRows = [];

        #switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
              'driver'    => 'mysql',
              'host'      => env('DB_HOST'),
              'port'      => env('DB_PORT'),
              'database'  => $userdb->database,
              'username'  => env('DB_USERNAME'),
              'password'  => env('DB_PASSWORD'),
              'charset'   => 'utf8',
              'collation' => 'utf8_unicode_ci',
              'prefix'    => '',
              'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        #Validate input
        $validator = Validator::make($request->all(),[
          'table_guid' => 'required',
          'count' => 'numeric'
        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

        $request['last_user'] = $user->id;

        for ($i = 0; $i < $request['count']; $i++)
        {
            //create the new row
            $newRow = CustomRow::create($request->only('table_guid','last_user'));

            //search for rows with the same table guid and set the successor of the last row
            $allRows = CustomRow::where('table_guid',$request['table_guid'])->orderBy('id', 'DESC')->get();
            if (count($allRows) > 1)
            {
                $predecessor = CustomRow::where([['id','=',$allRows[1]->id],['table_guid','=',$request['table_guid']]])->first();

                if($predecessor)
                {
                    $predecessor->update(['successor' => $newRow->id]);
                    $newRow->update(['predecessor' => $predecessor->id]);
                }
            }
            $newRows[] = $newRow;
        }
        $res['success'] = true;
        $res['rows'] = $newRows;
        return response()->json($res,201);
    }

    /**
     * Update a row (NOT IMPLEMENTED)
     *
     * @queryParam api_token required Authenticates the user
     */
    public function updateRow(Request $request)
    {

    }

    /**
     * Add a QR Code to a row
     *
     * @queryParam api_token required Authenticates the user
     * @urlParam row_id required The id of the row
     * @bodyParam qr_code string required The content of the QR Code
     *
     *
     * @response 201{
     *  "success": true,
     *  "message": "QR Code added"
     *  }
     * @response 200{
     *  "success":  false,
     *  "message": "This row already has a QR Code assigned."
     *  }
     * @response 401{
     *  "success": false,
     *  "status": 401,
     *  "message":  "HTTP_UNAUTHORIZED"
     * }
     */
    public function addQrCode($row_id,Request $request){
        $validator = Validator::make($request->all(),[
          'qr_code' => 'required'
        ]);
        if ($validator->fails()){
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }
//        $qr_code =hash('sha512', $request['qr_code']);
        $row = CustomRow::findOrFail($row_id);

        if ($row->qr_code === null || $row->qr_code === ""){
            $data['qr_code'] = $request['qr_code'];
            $row->update($data);

            $res['success'] = true;
            $res['message'] = 'QR Code added.';
            $res['code'] = $row;
        } else {
            $res['success'] = false;
            $res['message'] = 'This row already has a QR Code assigned.';
        }

        return response()->json($res,200);
    }

    /**
     * find a QR Code
     *
     * @queryParam api_token required Authenticates the user
     * @urlParam qr_code string required The content of the QR Code
     *
     *
     * @response    {
     *  "success": true,
     *  "row": {
     *      "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
     *      "last_user": 2,
     *      "updated_at": "1603893764",
     *      "created_at": "1603893764",
     *      "id": 11,
     *      "predecessor": 10
     *      }
     * }
     * @response 401{
     *  "success": false,
     *  "status": 401,
     *  "message":  "HTTP_UNAUTHORIZED"
     * }
     */
    public function findQrCode($qr_code){
//        $row = CustomRow::where('qr_code', hash('sha512',$qr_code))->first();
        $row = CustomRow::where('qr_code',$qr_code)->first();
        $table = CustomTable::where('guid', $row->table_guid)->first();

        if (Gate::allows('reader',[$row->id,'row'])){
            $res['success'] = true;
            $res['row'] = $row;
            $res['project'] = $table;
        } else {
            abort(401, 'Unauthorized.' );
        }
        return response()->json($res, 200);
    }


    /**
     * Delete rows
     * @urlParam guid required The guid of the associated table
     * @queryParam api_token required Authenticates the user
     *
     * @bodyParam *.id integer required The ID of the row
     *
     * @response {
     *  "success": true,
     *  "message": "Deleted row(s): 1, 2, 3"
     * }
     * @response 403{
     *  "success": false,
     *  "status": 403,
     *  "message":  "This action is unauthorized."
     * }
     * @response 404{
     *  "success": false,
     *  "message":  "Row does not exist."
     * }
     */
    public function deleteRow($guid,Request $request)
    {
        $token = hash('sha256',$request->input('api_token'));
        $user = User::find(User::where('api_token',$token)->value('id'));
        $userdb = $user->storage;

        //switch to personal connection
        if($userdb != null)
        {
            Config::set('database.connections.mainDB',[
              'driver'    => 'mysql',
              'host'      => env('DB_HOST'),
              'port'      => env('DB_PORT'),
              'database'  => $userdb->database,
              'username'  => env('DB_USERNAME'),
              'password'  => env('DB_PASSWORD'),
              'charset'   => 'utf8',
              'collation' => 'utf8_unicode_ci',
              'prefix'    => '',
              'strict'    => false,
            ]);
        }
        DB::connection('mainDB')->reconnect();

        $input = [ 'data'=>$request->all()];
        unset($input['data']['api_token']);
        $validator = Validator::make($input,[
          'data.*.id' => 'required|exists:custom_rows',

        ]);
        if ($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res, 200);
        }

        $rows = $request->all();


        foreach ($rows as $row){
            if (is_array($row)){
//                foreach ($row as $arr){
                    if (CustomRow::where('id',$row['id'])->exists() && CustomRow::where('id',$row['id'])->first()->predecessor != null){
                        $predecessor = CustomRow::where('id', CustomRow::where('id',$row['id'])->first()->predecessor)->first();
                        $successor = CustomRow::where('id', CustomRow::where('id',$row['id'])->first()->successor)->first();

                        if($successor != null)
                        {
                            $predecessor->update(['successor'=> $successor->id]);
                            $successor->update(['predecessor'=> $predecessor->id]);
                        } else {
                            $predecessor->update(['successor'=> null]);
                        }

                        $imageEnum = EnumData::where('type','Image')->first();
                        $imageEnum = $imageEnum->data_id;

                        //delete all columns that were in these rows
                        if(CustomColumn::where([['row_id','=',$row['id']],['data_type_id','=',$imageEnum]])->exists())
                        {
                                $images = CustomColumn::where([['row_id','=',$row['id']],['data_type_id','=',$imageEnum]])->get();
                                foreach ($images as $image)
                                {
                                    $path = $guid.'/'.$image->value;
                                    if (Storage::disk('local')->exists($path)) {
                                        Storage::disk('local')->delete($path);
                                    }
                                }
                        }
                        CustomRow::where('id',$row['id'])->delete();
//                        CustomColumn::where('row_id',$row['id'])->delete();

                    } else if (CustomRow::where('id',$row['id'])->first()->predecessor == null) {
                        continue;
                    } else {
                        $res['success'] = false;
                        $res['message'] = 'Row does not exist.';
                        return response()->json($res,404);
                    }
//                }
            }
        }

        $str = null;
        foreach ($rows[0] as $row)
        {
            $str .= ' '. $row.',';
        }



        $str = rtrim($str, ',');

        $res['success'] = true;
        $res['message'] = 'Deleted row(s):'.$str;
        return response($res,200);
    }
}
