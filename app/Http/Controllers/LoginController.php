<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


/**
 * @group  User Login/Logout
 *
 *
 * APIs for managing login and logout of users
 */
class LoginController extends Controller
{
    /**
     * Login
     *
     * After a successful login a new api token and the user data is returned.
     *
     * @bodyParam email string required The email of the user
     * @bodyParam password string required The SHA512 hashed password of the user
     *
     * @response {
     *  "success": true,
     *  "api_token": "274abd30-1f47-4f2b-98ee-9449f306753e",
     *  "data": {
     *   "id": 2,
     *   "email": "Bob@gmail.com",
     *   "username": "Bob",
     *   "registration_approved": 0,
     *   "created_at": 1603878735,
     *   "updated_at": 0
     *   }
     * }
     * @response {
     *  "success": false,
     *  "message": "Your email or password is incorrect"
     * }
     * @response {
     *  "success": false,
     *  "message":  {
     *  "email": [
     *  "Error message."
     *  ]
     *  }
     * }
     */
    public function index (Request $request)
    {
        $validator = Validator::make($request->all(),[
          'email' => 'required|exists:userData.users|email',
          'password' => 'required'
        ]);
        if($validator->fails())
        {
            $res['success'] = false;
            $res['message'] = $validator->errors();
            return response()->json($res,200);
        }
        $login = User::where('email', $request['email'])->first();
        if( ! $login)
        {
            $res['success'] = false;
            $res['message'] = 'Your email or password is incorrect';
            return response()->json($res);
        } else {

            if(app('hash')->check($request['password'], $login->password)) //app('hash')->check($request['password'], $login->password)
            {
                $api_token = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                mt_rand( 0, 0xffff ),
                mt_rand( 0, 0xffff ),
                mt_rand( 0, 0xffff ),
                mt_rand( 0, 0x0fff ) | 0x4000,
                mt_rand( 0, 0x3fff ) | 0x8000,
                mt_rand( 0, 0xffff ),
                mt_rand( 0, 0xffff ),
                mt_rand( 0, 0xffff )
                );

                $create_token = User::where('id', $login->id)->update(['api_token' => hash('sha256',$api_token)]);

                if($create_token)
                {
                    $res['success'] = true;
                    $res['api_token'] = $api_token;
                    $res['data'] = User::find($login->id);
                    return response()->json($res, 200);
                }
            } else {
                $res['success'] = false;
                $res['message'] = 'Your email or password is incorrect';
                return response()->json($res);
            }
        }
    }

    /**
     * Logout
     *
     * Deletes the api token of the user.
     *
     * @response {
     *  "success": true
     * }
     */
    public function logout(Request $request){
        $user = $request->user();
        $user->update(['api_token' => null]);

        $res['success'] = true;
        return response()->json($res, 200);
    }
}
