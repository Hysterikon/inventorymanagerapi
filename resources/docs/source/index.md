---
title: API Reference

language_tabs:
- javascript
- php

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Category management


<!-- START_b408d3ca66570bdbd5d15b87af12e91b -->
## List all categories from a user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/categories/all"
);

let params = {
    "api_token": "porro",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/categories/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'porro',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "categories": [
        {
            "id": 1,
            "category": "ExampleCategory",
            "created_at": "1603898348",
            "updated_at": "1603898348"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "Keine Kategorien vorhanden"
}
```

### HTTP Request
`GET /categories/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_b408d3ca66570bdbd5d15b87af12e91b -->

<!-- START_e402ae209507d86d7e146d7329658b41 -->
## List all existing categories

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
The same functionality is implemented under the table management section.

> Example request:

```javascript
const url = new URL(
    "http://localhost/categories/list"
);

let params = {
    "api_token": "quia",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/categories/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quia',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "categories": [
        {
            "id": 1,
            "category": "ExampleCat",
            "created_at": "1603898348",
            "updated_at": "1603898348"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "Keine Kategorien vorhanden"
}
```

### HTTP Request
`GET /categories/list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_e402ae209507d86d7e146d7329658b41 -->

<!-- START_14f3ba77b21b0bdc3e00583f76db63b1 -->
## Get one category

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/categories/voluptatem"
);

let params = {
    "api_token": "perspiciatis",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/categories/voluptatem',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'perspiciatis',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "category": {
        "id": 1,
        "category": "ExampleCategory",
        "created_at": "1603898348",
        "updated_at": "1603898348"
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "Keine Kategorie gefunden"
}
```

### HTTP Request
`GET /categories/{catID}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `catID` |  required  | The ID of the category
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_14f3ba77b21b0bdc3e00583f76db63b1 -->

<!-- START_3ed9ffd167c72b1cf0010aabcf551468 -->
## Create a category

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/categories/new"
);

let params = {
    "api_token": "nisi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "category": "commodi"
    }
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/categories/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'nisi',
        ],
        'json' => [
            [
                'category' => 'commodi',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "categoryItem": {
        "id": 1,
        "category": "ExampleCategory",
        "created_at": "1603898348",
        "updated_at": "1603898348"
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "Kategorien wurden nicht erstellt"
}
```

### HTTP Request
`POST /categories/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.category` | string |  required  | The name of the category (alpha)
    
<!-- END_3ed9ffd167c72b1cf0010aabcf551468 -->

<!-- START_f06af3cac038299150e7d2029d86bbcd -->
## Delete a category

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/categories/delete"
);

let params = {
    "api_token": "earum",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "cat_id": 9
    }
]

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/categories/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'earum',
        ],
        'json' => [
            [
                'cat_id' => 9,
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Categories deleted"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "The provided categories do not exist"
}
```

### HTTP Request
`DELETE /categories/delete`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.cat_id` | integer |  required  | The ID of the category
    
<!-- END_f06af3cac038299150e7d2029d86bbcd -->

<!-- START_3f69993005d7546ae775a02f8c8845c6 -->
## Update a category

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/categories/update"
);

let params = {
    "api_token": "non",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "category": "est",
        "cat_id": 19
    }
]

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/categories/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'non',
        ],
        'json' => [
            [
                'category' => 'est',
                'cat_id' => 19,
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "category": {
        "id": 1,
        "category": "ExampleCategory",
        "created_at": "1603898348",
        "updated_at": "1603898348"
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "Keine Kategorien aktualisiert"
}
```

### HTTP Request
`PUT /categories/update`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `catID` |  required  | The ID of the category
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.category` | string |  required  | The name of the category
        `*.cat_id` | integer |  required  | The ID of the category
    
<!-- END_3f69993005d7546ae775a02f8c8845c6 -->

#Column management


<!-- START_83e1ff8bf7b83f01a23c22c9b88ca0bc -->
## List all columns of a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/all/nobis"
);

let params = {
    "api_token": "rerum",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/columns/all/nobis',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'rerum',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "columns": [
        {
            "id": 1,
            "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "data_type_id": 1,
            "last_user": "1",
            "column_order": 1,
            "row_id": 1,
            "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
        },
        {
            "id": 1,
            "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "data_type_id": 1,
            "last_user": "1",
            "column_order": 1,
            "row_id": 1,
            "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No columns found"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No table with the guid: eaa8721a-31d0-4027-812f-ec199176960a"
}
```

### HTTP Request
`GET /columns/all/{partOf}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `partOf` |  required  | The guid of the associated project
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_83e1ff8bf7b83f01a23c22c9b88ca0bc -->

<!-- START_e0c18c65093ff4d64e4e26ebda40e811 -->
## List one column of a Project

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/ea"
);

let params = {
    "api_token": "suscipit",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/columns/ea',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'suscipit',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "column": [
        {
            "id": 1,
            "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "data_type_id": 1,
            "last_user": "1",
            "column_order": 1,
            "row_id": 1,
            "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No columns found"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No table with the guid: eaa8721a-31d0-4027-812f-ec199176960a"
}
```

### HTTP Request
`GET /columns/{guid}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the column
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_e0c18c65093ff4d64e4e26ebda40e811 -->

<!-- START_cb696c7311d2f72f99e1721c52692e69 -->
## Add a category to a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/categories/new"
);

let params = {
    "api_token": "quo",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "cat_id": 18,
        "column_guid": "deleniti"
    }
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/columns/categories/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quo',
        ],
        'json' => [
            [
                'cat_id' => 18,
                'column_guid' => 'deleniti',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Category added"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "cat_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /columns/categories/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.cat_id` | integer |  required  | The ID of the category
        `*.column_guid` | string |  required  | The guid of the column
    
<!-- END_cb696c7311d2f72f99e1721c52692e69 -->

<!-- START_fc986bf39a76f65697213cbdd6449912 -->
## Add tag to a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/tags/new"
);

let params = {
    "api_token": "sed",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "tag_id": 2,
        "column_guid": "eveniet"
    }
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/columns/tags/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'sed',
        ],
        'json' => [
            [
                'tag_id' => 2,
                'column_guid' => 'eveniet',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Tag added"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "tag_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /columns/tags/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.tag_id` | integer |  required  | The ID of the tag
        `*.column_guid` | string |  required  | The guid of the column
    
<!-- END_fc986bf39a76f65697213cbdd6449912 -->

<!-- START_7a9d5d550322f3895d84c4685d6dcaa9 -->
## Create a new column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/dolores/new"
);

let params = {
    "api_token": "non",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "value": "sit",
        "data_type_id": 14,
        "column_order": 2,
        "row_id": 18
    }
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/columns/dolores/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'non',
        ],
        'json' => [
            [
                'value' => 'sit',
                'data_type_id' => 14,
                'column_order' => 2,
                'row_id' => 18,
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "columns": [
        {
            "value": "Test",
            "data_type_id": 1,
            "column_order": 2,
            "row_id": 3,
            "guid": "e107d146-b0d9-4786-ac83-4af26dc4d5ac",
            "part_of": "eaa8721a-31d0-4027-812f-ec199176960a",
            "last_user": 2
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "row_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /columns/{guid}/new`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated project
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.value` | string |  required  | The value of the column
        `*.data_type_id` | integer |  required  | The ID of the datatype of the column
        `*.column_order` | integer |  required  | The ID of the column order
        `*.row_id` | integer |  required  | The ID of the row of the column
    
<!-- END_7a9d5d550322f3895d84c4685d6dcaa9 -->

<!-- START_edd973beef28c59a3ecfe0a2e7ee0e32 -->
## Delete a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/1/delete"
);

let params = {
    "api_token": "maxime",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "guid": "temporibus"
    }
]

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/columns/1/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'maxime',
        ],
        'json' => [
            [
                'guid' => 'temporibus',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "guid": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`DELETE /columns/{partOf}/delete`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.guid` | string |  required  | The guid of the column
    
<!-- END_edd973beef28c59a3ecfe0a2e7ee0e32 -->

<!-- START_5aec137187dd2c691f76f8b61e8c9538 -->
## Update an existing column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/omnis/update"
);

let params = {
    "api_token": "ea",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "value": "est",
        "data_type_id": 13,
        "column_order": 1,
        "row_id": 20
    }
]

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/columns/omnis/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'ea',
        ],
        'json' => [
            [
                'value' => 'est',
                'data_type_id' => 13,
                'column_order' => 1,
                'row_id' => 20,
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "columns": [
        {
            "value": "Test",
            "data_type_id": 1,
            "column_order": 2,
            "row_id": 3,
            "guid": "e107d146-b0d9-4786-ac83-4af26dc4d5ac",
            "part_of": "eaa8721a-31d0-4027-812f-ec199176960a",
            "last_user": 2
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "guid": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`PUT /columns/{partOf}/update`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `partOf` |  required  | The guid of the associated project
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.value` | string |  required  | The value of the column
        `*.data_type_id` | integer |  required  | The ID of the datatype of the column
        `*.column_order` | integer |  required  | The ID of the column order
        `*.row_id` | integer |  required  | The ID of the row of the column
    
<!-- END_5aec137187dd2c691f76f8b61e8c9538 -->

<!-- START_a4f561dda263afe0dfbdb3573d2ea3e4 -->
## Remove tag from a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/tags/remove"
);

let params = {
    "api_token": "incidunt",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "tag_id": 10,
        "column_guid": "corporis"
    }
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/columns/tags/remove',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'incidunt',
        ],
        'json' => [
            [
                'tag_id' => 10,
                'column_guid' => 'corporis',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Tag removed"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "tag_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /columns/tags/remove`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.tag_id` | integer |  required  | The ID of the tag
        `*.column_guid` | string |  required  | The guid of the column
    
<!-- END_a4f561dda263afe0dfbdb3573d2ea3e4 -->

<!-- START_89a39f695b12a4edc3b7081da026d5bc -->
## List tags from a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/ut/tags/list"
);

let params = {
    "api_token": "eum",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/columns/ut/tags/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'eum',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tags": [
        {
            "tag_id": 1,
            "tag_name": "ExampleTag",
            "column_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No tags found"
}
```

### HTTP Request
`GET /columns/{guid}/tags/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated column
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_89a39f695b12a4edc3b7081da026d5bc -->

<!-- START_e1a6779fba8cdfe80add6f272173ab04 -->
## Remove a category from a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/categories/remove"
);

let params = {
    "api_token": "et",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "cat_id": 11,
        "column_guid": "et"
    }
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/columns/categories/remove',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'et',
        ],
        'json' => [
            [
                'cat_id' => 11,
                'column_guid' => 'et',
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Category removed"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "cat_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /columns/categories/remove`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.cat_id` | integer |  required  | The ID of the category
        `*.column_guid` | string |  required  | The guid of the column
    
<!-- END_e1a6779fba8cdfe80add6f272173ab04 -->

<!-- START_c5b3ac4e98ec6b733abf12e5505ff186 -->
## List categories from a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/columns/omnis/categories/list"
);

let params = {
    "api_token": "fuga",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/columns/omnis/categories/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'fuga',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "categories": [
        {
            "category_id": 1,
            "category_name": "ExampleCategory",
            "column_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No categories found"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No categories found"
}
```

### HTTP Request
`GET /columns/{guid}/categories/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated column
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_c5b3ac4e98ec6b733abf12e5505ff186 -->

#Column version management


<!-- START_25f75a492ccc66ea4193289f22a74e7d -->
## List all verisons of a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/versions/all/velit"
);

let params = {
    "api_token": "dignissimos",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/versions/all/velit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'dignissimos',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "versions": [
        {
            "id": 1,
            "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "date": 1603877430,
            "created_by": 2
        },
        {
            "id": 2,
            "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "date": 1603877430,
            "created_by": 2
        }
    ]
}
```

### HTTP Request
`GET /versions/all/{guid}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated column
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_25f75a492ccc66ea4193289f22a74e7d -->

<!-- START_83696debe7959ba186fd5ede2ae0d092 -->
## List one version of a column

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/versions/1"
);

let params = {
    "api_token": "deleniti",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/versions/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'deleniti',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "version": [
        {
            "id": 1,
            "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "date": 1603877430,
            "created_by": 2
        }
    ]
}
```

### HTTP Request
`GET /versions/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the column
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_83696debe7959ba186fd5ede2ae0d092 -->

<!-- START_7a3929aefd540220eda869d036d52653 -->
## Create a new version

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/versions/quaerat/new"
);

let params = {
    "api_token": "temporibus",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "column_guid": "ab",
    "value": "earum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/versions/quaerat/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'temporibus',
        ],
        'json' => [
            'column_guid' => 'ab',
            'value' => 'earum',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "version": [
        {
            "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "date": 1603877430,
            "created_by": 2
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "value": [
            "Error message."
        ]
    }
}
```
> Example response (403):

```json
{
    "success": false,
    "message": "Forbidden"
}
```
> Example response (404):

```json
{
    "success": false,
    "message": "Column does not exist"
}
```

### HTTP Request
`POST /versions/{guid}/new`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `column_guid` | required |  optional  | The guid of the column
        `value` | string |  required  | The value of the column
    
<!-- END_7a3929aefd540220eda869d036d52653 -->

<!-- START_f96c85cdb7166722a17c9166248a3d3c -->
## Delete a version

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/versions/delete"
);

let params = {
    "api_token": "sit",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/versions/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'sit',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Versions deleted"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "id": [
            "Error message."
        ]
    }
}
```
> Example response (404):

```json
{
    "success": false,
    "message": "Version does not exist."
}
```

### HTTP Request
`DELETE /versions/delete`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the version
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_f96c85cdb7166722a17c9166248a3d3c -->

<!-- START_5aeb8533486653404d03bf13a2428eae -->
## Update an existing version

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/versions/harum/update"
);

let params = {
    "api_token": "quidem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "value": "reprehenderit"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/versions/harum/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quidem',
        ],
        'json' => [
            'value' => 'reprehenderit',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "version ": [
        {
            "column_guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
            "value": "ExampleValue",
            "date": 1603877430,
            "created_by": 2
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "value": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`PUT /versions/{id}/update`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the version
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `value` | string |  required  | The new value of the version
    
<!-- END_5aeb8533486653404d03bf13a2428eae -->

#Enum data management for Admins


<!-- START_681545cc8668918e13d43a6161c0114a -->
## List all enum types

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/enum/all"
);

let params = {
    "api_token": "accusamus",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/enum/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'accusamus',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "enums": [
        {
            "id": 1,
            "data_id": 0,
            "type": "Titel"
        },
        {
            "id": 2,
            "data_id": 1,
            "type": "Text"
        }
    ]
}
```

### HTTP Request
`GET /enum/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_681545cc8668918e13d43a6161c0114a -->

<!-- START_a21341e66ba807f51a13a11dee7c12b4 -->
## Get one enum types

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/enum/sint"
);

let params = {
    "api_token": "animi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/enum/sint',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'animi',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "enum": {
        "id": 1,
        "data_id": 0,
        "type": "Titel"
    }
}
```

### HTTP Request
`GET /enum/{data_id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `data_id` |  required  | The ID of the enum
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_a21341e66ba807f51a13a11dee7c12b4 -->

<!-- START_bdb3fff071ae59fa0334b215d5df90b1 -->
## Create a new enum

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/enum/new"
);

let params = {
    "api_token": "est",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "data_id": 16,
    "type": "sapiente"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/enum/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'est',
        ],
        'json' => [
            'data_id' => 16,
            'type' => 'sapiente',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "enum": {
        "id": 1,
        "data_id": 0,
        "type": "Titel"
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "This action is unauthorized."
}
```

### HTTP Request
`POST /enum/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `data_id` | integer |  required  | The data_id of the new enum
        `type` | string |  required  | The type of the enum (Text,Zahl, etc.)
    
<!-- END_bdb3fff071ae59fa0334b215d5df90b1 -->

<!-- START_c688431d7879decf3fef0e424c54fe5c -->
## Delete a enum

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/enum/deserunt/delete"
);

let params = {
    "api_token": "in",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/enum/deserunt/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'in',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "This action is unauthorized."
}
```
> Example response (404):

```json
{
    "success": false,
    "message": "Enum does not exist."
}
```

### HTTP Request
`DELETE /enum/{data_id}/delete`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `data_id` |  required  | The data_id of the enum
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_c688431d7879decf3fef0e424c54fe5c -->

<!-- START_4c68318150f857c3c35c8144639029ce -->
## Update a enum

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/enum/animi/update"
);

let params = {
    "api_token": "sit",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "type": "sit"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/enum/animi/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'sit',
        ],
        'json' => [
            'type' => 'sit',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "enum": {
        "id": 1,
        "data_id": 0,
        "type": "Titel"
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "This action is unauthorized."
}
```

### HTTP Request
`PUT /enum/{data_id}/update`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `data_id` |  required  | The data_id of the enum
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `type` | string |  required  | The type of the enum (Text,Zahl, etc.)
    
<!-- END_4c68318150f857c3c35c8144639029ce -->

#File management


<!-- START_6064d16707d1058b32210cf33f53ddea -->
## Lists all files for an Admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/file/all"
);

let params = {
    "api_token": "a",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/file/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'a',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "directories": [
        "4d5c0c05-8323-4da7-91dc-ba56823c422e",
        "e1a3c0ad-f060-4c32-8f73-ebce86ebc711"
    ],
    "files": [
        "4d5c0c05-8323-4da7-91dc-ba56823c422e\/1603289690.jpg",
        "e1a3c0ad-f060-4c32-8f73-ebce86ebc711\/1603289245.jpg"
    ]
}
```

### HTTP Request
`GET /file/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_6064d16707d1058b32210cf33f53ddea -->

<!-- START_41a0cfc9967768e6b62af3926270cbb7 -->
## Lists all files from a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
The files are returned as a base64 string.

> Example request:

```javascript
const url = new URL(
    "http://localhost/file/sed/list"
);

let params = {
    "api_token": "sit",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/file/sed/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'sit',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "files": [
        "base64_string"
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "no files"
}
```

### HTTP Request
`GET /file/{guid}/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_41a0cfc9967768e6b62af3926270cbb7 -->

<!-- START_6756bc76bacf17564c579124d63485f8 -->
## Upload a file

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/file/upload"
);

let params = {
    "api_token": "eos",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "guid": "eum",
    "content": "dolorem",
    "type": "doloribus"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/file/upload',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'eos',
        ],
        'json' => [
            'guid' => 'eum',
            'content' => 'dolorem',
            'type' => 'doloribus',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "filename": "name"
}
```

### HTTP Request
`POST /file/upload`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `guid` | string |  required  | The guid of the table
        `content` | string |  required  | The base64 encoded image/or file
        `type` | string |  required  | The image/file type (jpg, png)
    
<!-- END_6756bc76bacf17564c579124d63485f8 -->

<!-- START_b2d00e17fa26ea81b67210a13c444a85 -->
## Get one file

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
The file is returned as a base64 string.

> Example request:

```javascript
const url = new URL(
    "http://localhost/file/repellendus"
);

let params = {
    "api_token": "veniam",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/file/repellendus',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'veniam',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "image": [
        "base64_string"
    ]
}
```
> Example response (403):

```json
{
    "success": false
}
```

### HTTP Request
`GET /file/{name}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `name` |  required  | The name of the file
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_b2d00e17fa26ea81b67210a13c444a85 -->

<!-- START_0fef50f122f7688fa9f02a8b7f1984c0 -->
## Update a file (NOT IMPLEMENTED)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/file/1"
);

let params = {
    "api_token": "et",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/file/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'et',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT /file/{guid}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_0fef50f122f7688fa9f02a8b7f1984c0 -->

<!-- START_9269596f60f335238a904a8c256a5cc1 -->
## Download a file

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/file/architecto/download"
);

let params = {
    "api_token": "necessitatibus",
    "ext": "sed",
    "guid": "assumenda",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/file/architecto/download',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'necessitatibus',
            'ext'=> 'sed',
            'guid'=> 'assumenda',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "success": false,
    "status": 401,
    "message": "HTTP_UNAUTHORIZED"
}
```

### HTTP Request
`GET /file/{name}/download`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `name` |  required  | The name of the file
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
    `ext` |  required  | The file extension
    `guid` |  required  | The guid of the table

<!-- END_9269596f60f335238a904a8c256a5cc1 -->

<!-- START_015846d9e345ef6b8c67b39fefc67c7e -->
## Delete a file

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/file/suscipit"
);

let params = {
    "api_token": "quidem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "type": "est",
    "guid": "eius"
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/file/suscipit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quidem',
        ],
        'json' => [
            'type' => 'est',
            'guid' => 'eius',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "File deleted"
}
```
> Example response (403):

```json
{
    "success": false,
    "message": "File does not exist."
}
```

### HTTP Request
`DELETE /file/{name}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `name` |  required  | The name of the file
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `type` | string |  required  | The extension of the file
        `guid` | string |  required  | The guid of the table
    
<!-- END_015846d9e345ef6b8c67b39fefc67c7e -->

#License management


<!-- START_6c732bf51e2d57a75957cc095b23fbda -->
## List all licenses from a user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/license/all"
);

let params = {
    "api_token": "non",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/license/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'non',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "licenses": [
        {
            "id": 1,
            "user_id": 2,
            "type_id": 2,
            "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
            "license_name": "Advanced_License",
            "price": 150,
            "activation_date": 1603891368,
            "expiration_date": 1605052800
        },
        {
            "id": 1,
            "user_id": 2,
            "type_id": 2,
            "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
            "license_name": "Advanced_License",
            "price": 150,
            "activation_date": 1603891368,
            "expiration_date": 1605052800
        }
    ]
}
```

### HTTP Request
`GET /license/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_6c732bf51e2d57a75957cc095b23fbda -->

<!-- START_2e3637d5221ebc32fd0dbff31a369933 -->
## List all licenses for an Admin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/license/all/admin"
);

let params = {
    "api_token": "consequuntur",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/license/all/admin',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'consequuntur',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "licenses": [
        {
            "id": 1,
            "user_id": 2,
            "type_id": 2,
            "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
            "license_name": "Advanced_License",
            "price": 150,
            "activation_date": 1603891368,
            "expiration_date": 1605052800
        },
        {
            "id": 1,
            "user_id": 2,
            "type_id": 2,
            "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
            "license_name": "Advanced_License",
            "price": 150,
            "activation_date": 1603891368,
            "expiration_date": 1605052800
        }
    ]
}
```

### HTTP Request
`GET /license/all/admin`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_2e3637d5221ebc32fd0dbff31a369933 -->

<!-- START_293cb90b59ccd3f01d65392417841f70 -->
## Get one license

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/license/nisi"
);

let params = {
    "api_token": "ut",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/license/nisi',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'ut',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "license": {
        "id": 1,
        "user_id": 2,
        "type_id": 2,
        "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
        "license_name": "Advanced_License",
        "price": 150,
        "activation_date": 1603891368,
        "expiration_date": 1605052800
    }
}
```
> Example response (404):

```json
{
    "success": false,
    "status": 404,
    "message": "No query results for model [App\\License] 1"
}
```

### HTTP Request
`GET /license/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the license
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_293cb90b59ccd3f01d65392417841f70 -->

<!-- START_ae8aae5554fc3ea0787911742672ad3f -->
## Create a new license

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/license/new"
);

let params = {
    "api_token": "est",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "type_id": 5,
    "expiration_date": 15
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/license/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'est',
        ],
        'json' => [
            'type_id' => 5,
            'expiration_date' => 15,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "license": {
        "id": 1,
        "user_id": 2,
        "type_id": 2,
        "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
        "license_name": "Advanced_License",
        "price": 150,
        "activation_date": 1603891368,
        "expiration_date": 1605052800
    }
}
```
> Example response (422):

```json
{
    "success": false,
    "status": 422,
    "message": "The given data was invalid."
}
```

### HTTP Request
`POST /license/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `type_id` | integer |  required  | The id of the license type
        `expiration_date` | integer |  required  | The expiration date of the license in unix time
    
<!-- END_ae8aae5554fc3ea0787911742672ad3f -->

<!-- START_a3e2a7d3458173bfeca32ff3f107a3e0 -->
## Create a new license for a user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/license/admin/new"
);

let params = {
    "api_token": "delectus",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "type_id": 11,
    "user_id": 17,
    "expiration_date": 11
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/license/admin/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'delectus',
        ],
        'json' => [
            'type_id' => 11,
            'user_id' => 17,
            'expiration_date' => 11,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "license": {
        "id": 1,
        "user_id": 2,
        "type_id": 2,
        "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
        "license_name": "Advanced_License",
        "price": 150,
        "activation_date": 1603891368,
        "expiration_date": 1605052800
    }
}
```
> Example response (422):

```json
{
    "success": false,
    "status": 422,
    "message": "The given data was invalid."
}
```

### HTTP Request
`POST /license/admin/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `type_id` | integer |  required  | The id of the license type
        `user_id` | integer |  required  | The id of the user
        `expiration_date` | integer |  required  | The expiration date of the license in unix time
    
<!-- END_a3e2a7d3458173bfeca32ff3f107a3e0 -->

<!-- START_bc3df3079a9cdac39dc826f0170f2872 -->
## Delete a license

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/license/pariatur/delete"
);

let params = {
    "api_token": "in",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/license/pariatur/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'in',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true
}
```
> Example response (404):

```json
{
    "success": false,
    "status": 404,
    "message": "No query results for model [App\\License] 65"
}
```

### HTTP Request
`DELETE /license/{id}/delete`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the license
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_bc3df3079a9cdac39dc826f0170f2872 -->

<!-- START_1006d7e8bf539a714ab644ee0539a717 -->
## Update a License

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
It is currently possible to update every parameter of the license.

> Example request:

```javascript
const url = new URL(
    "http://localhost/license/et/update"
);

let params = {
    "api_token": "voluptates",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "type_id": 15,
    "expiration_date": 2
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/license/et/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'voluptates',
        ],
        'json' => [
            'type_id' => 15,
            'expiration_date' => 2,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "license": {
        "id": 1,
        "user_id": 2,
        "type_id": 2,
        "guid": "ce2b9f5b-6b3c-4cc4-acf0-a90889db0bb7",
        "license_name": "Advanced_License",
        "price": 150,
        "activation_date": 1603891368,
        "expiration_date": 1605052800
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "type_id": [
            "The type id must be a number."
        ]
    }
}
```

### HTTP Request
`PUT /license/{id}/update`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the license
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `type_id` | integer |  required  | The id of the license type
        `expiration_date` | integer |  required  | The expiration date of the license in unix time
    
<!-- END_1006d7e8bf539a714ab644ee0539a717 -->

#License type management


<!-- START_63371e4ef16cd72fe7f5a55a46fe2b11 -->
## List all license types

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/licenses/types/all"
);

let params = {
    "api_token": "eos",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/licenses/types/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'eos',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "types": [
        {
            "id": 1,
            "license_name": "Basic_License",
            "price": 100,
            "max_table_shares": 0,
            "max_tables": 10,
            "column_pattern_size": 5,
            "column_entry_max_amount": 10,
            "column_version_control": 0,
            "column_version_lifespan": 0,
            "max_table_size": 25,
            "max_table_lifespan": 322464
        },
        {
            "id": 2,
            "license_name": "Advanced_License",
            "price": 150,
            "max_table_shares": 5,
            "max_tables": 25,
            "column_pattern_size": 15,
            "column_entry_max_amount": 25,
            "column_version_control": 1,
            "column_version_lifespan": 24523,
            "max_table_size": 50,
            "max_table_lifespan": 54345245
        }
    ]
}
```

### HTTP Request
`GET /licenses/types/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_63371e4ef16cd72fe7f5a55a46fe2b11 -->

<!-- START_df81c53a1952a027f185005d075616d4 -->
## Get one license type

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/licenses/types/illum"
);

let params = {
    "api_token": "sed",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/licenses/types/illum',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'sed',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "type": {
        "id": 1,
        "license_name": "Basic_License",
        "price": 100,
        "max_table_shares": 0,
        "max_tables": 10,
        "column_pattern_size": 5,
        "column_entry_max_amount": 10,
        "column_version_control": 0,
        "column_version_lifespan": 0,
        "max_table_size": 25,
        "max_table_lifespan": 322464
    }
}
```
> Example response (404):

```json
{
    "success": false,
    "status": 404,
    "message": "No query results for model [App\\Type] 14"
}
```

### HTTP Request
`GET /licenses/types/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the type
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_df81c53a1952a027f185005d075616d4 -->

<!-- START_cbac85126b986618f33868482f7b512e -->
## Create a new license type

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/licenses/types/new"
);

let params = {
    "api_token": "est",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "license_name": "voluptatem",
    "price": 4,
    "max_table_shares": 9,
    "max_tables": 16,
    "column_pattern_size": 5,
    "column_entry_max_amount": 8,
    "column_version_control": true,
    "column_version_lifespan": 7,
    "max_table_size": 15,
    "max_table_lifespan": 18
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/licenses/types/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'est',
        ],
        'json' => [
            'license_name' => 'voluptatem',
            'price' => 4,
            'max_table_shares' => 9,
            'max_tables' => 16,
            'column_pattern_size' => 5,
            'column_entry_max_amount' => 8,
            'column_version_control' => true,
            'column_version_lifespan' => 7,
            'max_table_size' => 15,
            'max_table_lifespan' => 18,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "type": {
        "id": 1,
        "license_name": "Basic_License",
        "price": 100,
        "max_table_shares": 0,
        "max_tables": 10,
        "column_pattern_size": 5,
        "column_entry_max_amount": 10,
        "column_version_control": 0,
        "column_version_lifespan": 0,
        "max_table_size": 25,
        "max_table_lifespan": 322464
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "max_tables": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /licenses/types/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `license_name` | string |  required  | The name of the license
        `price` | integer |  required  | The price of the license
        `max_table_shares` | integer |  optional  | The max amount of table shares
        `max_tables` | integer |  required  | The max allowed tables
        `column_pattern_size` | integer |  required  | The max amount of allowed columns
        `column_entry_max_amount` | integer |  required  | 
        `column_version_control` | boolean |  required  | Indicates if the license has version control for columns
        `column_version_lifespan` | integer |  required  | The savetime of column versions
        `max_table_size` | integer |  required  | The allowed table size (rows)
        `max_table_lifespan` | integer |  required  | The allowed table savetime
    
<!-- END_cbac85126b986618f33868482f7b512e -->

<!-- START_84f671a6599bda619cddbca4c723d64d -->
## Delete a license type

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/licenses/types/natus/delete"
);

let params = {
    "api_token": "et",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/licenses/types/natus/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'et',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true
}
```
> Example response (404):

```json
{
    "success": false,
    "status": 404,
    "message": "No query results for model [App\\Type] 14"
}
```

### HTTP Request
`DELETE /licenses/types/{id}/delete`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the type
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_84f671a6599bda619cddbca4c723d64d -->

<!-- START_384ba1f15a03148ca9f1ed30b2a1bd71 -->
## Update a license type

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/licenses/types/et/update"
);

let params = {
    "api_token": "ut",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "license_name": "reprehenderit",
    "price": 5,
    "max_table_shares": 13,
    "max_tables": 7,
    "column_pattern_size": 14,
    "column_entry_max_amount": 15,
    "column_version_control": true,
    "column_version_lifespan": 18,
    "max_table_size": 8,
    "max_table_lifespan": 19
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/licenses/types/et/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'ut',
        ],
        'json' => [
            'license_name' => 'reprehenderit',
            'price' => 5,
            'max_table_shares' => 13,
            'max_tables' => 7,
            'column_pattern_size' => 14,
            'column_entry_max_amount' => 15,
            'column_version_control' => true,
            'column_version_lifespan' => 18,
            'max_table_size' => 8,
            'max_table_lifespan' => 19,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "type": {
        "id": 1,
        "license_name": "Basic_License",
        "price": 100,
        "max_table_shares": 0,
        "max_tables": 10,
        "column_pattern_size": 5,
        "column_entry_max_amount": 10,
        "column_version_control": 0,
        "column_version_lifespan": 0,
        "max_table_size": 25,
        "max_table_lifespan": 322464
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "max_tables": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`PUT /licenses/types/{id}/update`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the type
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `license_name` | string |  required  | The name of the license
        `price` | integer |  required  | The price of the license
        `max_table_shares` | integer |  optional  | The max amount of table shares
        `max_tables` | integer |  required  | The max allowed tables
        `column_pattern_size` | integer |  required  | The max amount of allowed columns
        `column_entry_max_amount` | integer |  required  | 
        `column_version_control` | boolean |  required  | Indicates if the license has version control for columns
        `column_version_lifespan` | integer |  required  | The savetime of column versions
        `max_table_size` | integer |  required  | The allowed table size (rows)
        `max_table_lifespan` | integer |  required  | The allowed table savetime
    
<!-- END_384ba1f15a03148ca9f1ed30b2a1bd71 -->

#Row management


<!-- START_ddf623902a8b80a04d3ffb5b1089bdb8 -->
## List all rows of a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/all/atque"
);

let params = {
    "api_token": "voluptas",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/rows/all/atque',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'voluptas',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "rows": [
        {
            "id": 1,
            "last_user": 2,
            "predecessor": null,
            "successor": 2,
            "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "created_at": "1603878749",
            "updated_at": "1603878749"
        },
        {
            "id": 2,
            "last_user": 2,
            "predecessor": 1,
            "successor": null,
            "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "created_at": "1603878749",
            "updated_at": "1603878749"
        }
    ],
    "total": 2
}
```
> Example response (403):

```json
{
    "success": false,
    "status": 403,
    "message": "This action is unauthorized."
}
```

### HTTP Request
`GET /rows/all/{partOf}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `partOf` |  required  | The guid of the associated project
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_ddf623902a8b80a04d3ffb5b1089bdb8 -->

<!-- START_c4c03653977a50e442b2de9db7f1cc9a -->
## Get one row of a table, also returns the total number of rows and the number of the returned row

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/possimus"
);

let params = {
    "api_token": "cupiditate",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/rows/possimus',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'cupiditate',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "row": {
        "id": 1,
        "last_user": 2,
        "predecessor": null,
        "successor": 2,
        "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
        "created_at": "1603878749",
        "updated_at": "1603878749"
    },
    "number": 3,
    "total": 10,
    "columns": {
        "id": 1,
        "guid": "274abd30-1f47-4f2b-98ee-9449f306753e",
        "value": "ExampleValue",
        "data_type_id": 1,
        "last_user": "1",
        "column_order": 1,
        "row_id": 1,
        "part_of": "eaa8721a-31d0-4027-812f-ec199176960a"
    }
}
```
> Example response (403):

```json
{
    "success": false,
    "status": 403,
    "message": "This action is unauthorized."
}
```

### HTTP Request
`GET /rows/{row_id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `row_id` |  required  | The ID of the row
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_c4c03653977a50e442b2de9db7f1cc9a -->

<!-- START_5769b44ef0b12e1195a6441bd41acfc9 -->
## Add a QR Code to a row

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/aspernatur/qr/add"
);

let params = {
    "api_token": "ut",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "qr_code": "reiciendis"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/rows/aspernatur/qr/add',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'ut',
        ],
        'json' => [
            'qr_code' => 'reiciendis',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "message": "QR Code added"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "This row already has a QR Code assigned."
}
```
> Example response (401):

```json
{
    "success": false,
    "status": 401,
    "message": "HTTP_UNAUTHORIZED"
}
```

### HTTP Request
`POST /rows/{row_id}/qr/add`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `row_id` |  required  | The id of the row
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `qr_code` | string |  required  | The content of the QR Code
    
<!-- END_5769b44ef0b12e1195a6441bd41acfc9 -->

<!-- START_b34bd0deec35f6c8ed73707cba5bd4c5 -->
## find a QR Code

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/qr/find/quaerat"
);

let params = {
    "api_token": "numquam",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/rows/qr/find/quaerat',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'numquam',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "row": {
        "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
        "last_user": 2,
        "updated_at": "1603893764",
        "created_at": "1603893764",
        "id": 11,
        "predecessor": 10
    }
}
```
> Example response (401):

```json
{
    "success": false,
    "status": 401,
    "message": "HTTP_UNAUTHORIZED"
}
```

### HTTP Request
`GET /rows/qr/find/{qr_code}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `qr_code` |  optional  | string required The content of the QR Code
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_b34bd0deec35f6c8ed73707cba5bd4c5 -->

<!-- START_39452379dbd6addb9769f70dcfcf6af9 -->
## Create new rows

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/1/new"
);

let params = {
    "api_token": "libero",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "table_guid": "et",
    "count": 16
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/rows/1/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'libero',
        ],
        'json' => [
            'table_guid' => 'et',
            'count' => 16,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "rows": [
        {
            "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "last_user": 2,
            "updated_at": "1603893764",
            "created_at": "1603893764",
            "id": 11,
            "predecessor": 10
        },
        {
            "table_guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "last_user": 2,
            "updated_at": "1603893764",
            "created_at": "1603893764",
            "id": 12,
            "predecessor": 11
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "table_guid": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /rows/{guid}/new`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `partOf` |  required  | The guid of the associated table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `table_guid` | string |  required  | The guid of teh associated table
        `count` | integer |  required  | The amount of new rows that are being created
    
<!-- END_39452379dbd6addb9769f70dcfcf6af9 -->

<!-- START_4e9eab42e78b5c25c31bf0ea43a9e9a9 -->
## get the count of all rows

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/1/total"
);

let params = {
    "api_token": "minima",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/rows/1/total',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'minima',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "total": 2
}
```
> Example response (403):

```json
{
    "success": false,
    "status": 403,
    "message": "This action is unauthorized."
}
```

### HTTP Request
`GET /rows/{table_guid}/total`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `tableGuid` |  required  | The guid of the table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_4e9eab42e78b5c25c31bf0ea43a9e9a9 -->

<!-- START_34c33996888a777e4c354df8d15fce4a -->
## Delete rows

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/perspiciatis/delete"
);

let params = {
    "api_token": "eligendi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    {
        "id": 7
    }
]

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/rows/perspiciatis/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'eligendi',
        ],
        'json' => [
            [
                'id' => 7,
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Deleted row(s): 1, 2, 3"
}
```
> Example response (403):

```json
{
    "success": false,
    "status": 403,
    "message": "This action is unauthorized."
}
```
> Example response (404):

```json
{
    "success": false,
    "message": "Row does not exist."
}
```

### HTTP Request
`DELETE /rows/{guid}/delete`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.id` | integer |  required  | The ID of the row
    
<!-- END_34c33996888a777e4c354df8d15fce4a -->

<!-- START_b4551a401651c2e8e80a6eb7a438ab7c -->
## Update a row (NOT IMPLEMENTED)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/rows/1/update"
);

let params = {
    "api_token": "veniam",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/rows/1/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'veniam',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT /rows/{row_id}/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_b4551a401651c2e8e80a6eb7a438ab7c -->

#Support/Request management


<!-- START_b779237de9c0937e50feb007b4ac2f86 -->
## /request/send
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/request/send"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/request/send',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST /request/send`


<!-- END_b779237de9c0937e50feb007b4ac2f86 -->

<!-- START_462cb6a1d53e5bc294a883d49ffdbced -->
## /request/list
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/request/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/request/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "success": false,
    "status": 401,
    "message": "HTTP_UNAUTHORIZED"
}
```

### HTTP Request
`GET /request/list`


<!-- END_462cb6a1d53e5bc294a883d49ffdbced -->

<!-- START_11f81626a6ff78a92b678ed3bed29015 -->
## /request/read
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/request/read"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/request/read',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "success": false,
    "status": 401,
    "message": "HTTP_UNAUTHORIZED"
}
```

### HTTP Request
`GET /request/read`


<!-- END_11f81626a6ff78a92b678ed3bed29015 -->

<!-- START_3690562e950c473560182b2cf87eb545 -->
## /request/update
<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/request/update"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/request/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT /request/update`


<!-- END_3690562e950c473560182b2cf87eb545 -->

#Table management


<!-- START_696808253e3afc8dc2cad1dffdd249fc -->
## List all tables of a user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/all"
);

let params = {
    "api_token": "cupiditate",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tables/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'cupiditate',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tables": [
        {
            "id": 1,
            "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603878742,
            "updated_at": 1603878796,
            "deletion_date": 0
        },
        {
            "id": 1,
            "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603878742,
            "updated_at": 1603878796,
            "deletion_date": 0
        }
    ]
}
```

### HTTP Request
`GET /tables/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_696808253e3afc8dc2cad1dffdd249fc -->

<!-- START_551d19eb93a66af119e098cecd82187b -->
## ADMIN returns the total amount of existing tables

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/count"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tables/count',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "table_count": 5
}
```
> Example response (401):

```json
{
    "success": false,
    "message": "Unauthorized"
}
```

### HTTP Request
`GET /tables/count`


<!-- END_551d19eb93a66af119e098cecd82187b -->

<!-- START_57578ca435b9197a505c685bca8ec469 -->
## ADMIN find specific projects by a searchtearm

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/admin/dolores"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tables/admin/dolores',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "projects": [
        {
            "id": 1,
            "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603878742,
            "updated_at": 1603878796,
            "deletion_date": 0
        },
        {
            "id": 1,
            "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603878742,
            "updated_at": 1603878796,
            "deletion_date": 0
        }
    ]
}
```
> Example response (404):

```json
{
    "success": false,
    "status": 404,
    "message": "No search results."
}
```

### HTTP Request
`GET /tables/admin/{searchTerm}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `searchTerm` |  required  | The search term to be searched for in the database

<!-- END_57578ca435b9197a505c685bca8ec469 -->

<!-- START_063193a87300c715f67ba127cfac8248 -->
## List updated tables

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/updated/nesciunt"
);

let params = {
    "api_token": "minus",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tables/updated/nesciunt',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'minus',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tables": [
        {
            "id": 1,
            "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603878742,
            "updated_at": 1603878796,
            "deletion_date": 0
        },
        {
            "id": 1,
            "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603878742,
            "updated_at": 1603878796,
            "deletion_date": 0
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "Nothing updated yet"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "This user has no tables"
}
```

### HTTP Request
`GET /tables/updated/{count}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `count` |  required  | The amount of the tables that should be returned
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_063193a87300c715f67ba127cfac8248 -->

<!-- START_7eb4b8528640deb0329780759aa5108a -->
## Get one Table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/voluptas"
);

let params = {
    "api_token": "officiis",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tables/voluptas',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'officiis',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "table": [
        {
            "id": 1,
            "guid": "b90de04e-bf14-4a78-9e72-62d1895de049",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603878742,
            "updated_at": 1603878796,
            "deletion_date": 0,
            "columns": [
                {
                    "id": 1,
                    "guid": "03224ca0-243a-4197-a44a-735d7b6a9333",
                    "value": "TEST6",
                    "data_type_id": 1,
                    "last_user": "2",
                    "column_order": 0,
                    "row_id": 1,
                    "part_of": "b90de04e-bf14-4a78-9e72-62d1895de049"
                }
            ]
        }
    ]
}
```

### HTTP Request
`GET /tables/{guid}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_7eb4b8528640deb0329780759aa5108a -->

<!-- START_477b3adcbf6d702833a96e47da657633 -->
## Create a new table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/new"
);

let params = {
    "api_token": "nihil",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "excepturi",
    "sub_table": true
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/tables/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'nihil',
        ],
        'json' => [
            'name' => 'excepturi',
            'sub_table' => true,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "table": {
        "name": "Table01",
        "sub_table": "0",
        "guid": "b7d7d34b-fe58-4aef-9f5f-7b29080d14d4",
        "created_at": 1603897048,
        "owner": 2,
        "id": 2
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "name": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /tables/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | The name of the table
        `sub_table` | boolean |  required  | Indicates if the table is a sub table
    
<!-- END_477b3adcbf6d702833a96e47da657633 -->

<!-- START_52b03e4d82b49bbcbf762751be701105 -->
## Add tag to a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/tags/new"
);

let params = {
    "api_token": "molestias",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    [
        {
            "tag_id": 12,
            "table_guid": "non"
        }
    ]
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/tables/tags/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'molestias',
        ],
        'json' => [
            [
                [
                    'tag_id' => 12,
                    'table_guid' => 'non',
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Tags added"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "tag": [
            "Error message."
        ]
    }
}
```
> Example response (403):

```json
{
    "success": false,
    "message": "Tag not found"
}
```

### HTTP Request
`POST /tables/tags/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.*.tag_id` | integer |  required  | The ID of the tag
        `*.*.table_guid` | string |  required  | The guid of the table
    
<!-- END_52b03e4d82b49bbcbf762751be701105 -->

<!-- START_920dd355d88dd039d9a24b53db8e7086 -->
## Remove tag from a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/tags/remove"
);

let params = {
    "api_token": "quasi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    [
        {
            "tag_id": 16,
            "table_guid": "nihil"
        }
    ]
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/tables/tags/remove',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quasi',
        ],
        'json' => [
            [
                [
                    'tag_id' => 16,
                    'table_guid' => 'nihil',
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Tags removed"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "tag": [
            "Error message."
        ]
    }
}
```
> Example response (403):

```json
{
    "success": false,
    "message": "Tag not found"
}
```

### HTTP Request
`POST /tables/tags/remove`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.*.tag_id` | integer |  required  | The ID of the tag
        `*.*.table_guid` | string |  required  | The guid of the table
    
<!-- END_920dd355d88dd039d9a24b53db8e7086 -->

<!-- START_904473042de0e84b8e669cbdc74dea55 -->
## List tags from a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/quae/tags/list"
);

let params = {
    "api_token": "cum",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tables/quae/tags/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'cum',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tags": [
        {
            "tag_id": 1,
            "tag_name": "ExampleTag",
            "table_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No tags found"
}
```

### HTTP Request
`GET /tables/{guid}/tags/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_904473042de0e84b8e669cbdc74dea55 -->

<!-- START_f39f9793f0aa040ed1238069de0f548e -->
## Add a category to a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/categories/new"
);

let params = {
    "api_token": "et",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    [
        {
            "cat_id": 15,
            "table_guid": "autem"
        }
    ]
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/tables/categories/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'et',
        ],
        'json' => [
            [
                [
                    'cat_id' => 15,
                    'table_guid' => 'autem',
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Category added"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "cat_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /tables/categories/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.*.cat_id` | integer |  required  | The ID of the category
        `*.*.table_guid` | string |  required  | The guid of the table
    
<!-- END_f39f9793f0aa040ed1238069de0f548e -->

<!-- START_929a46d19ba41110b6e8172060199722 -->
## Remove a category from a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/categories/remove"
);

let params = {
    "api_token": "fugit",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    [
        {
            "cat_id": 20,
            "table_guid": "ipsa"
        }
    ]
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/tables/categories/remove',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'fugit',
        ],
        'json' => [
            [
                [
                    'cat_id' => 20,
                    'table_guid' => 'ipsa',
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Category removed"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "cat_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /tables/categories/remove`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.*.cat_id` | integer |  required  | The ID of the category
        `*.*.table_guid` | string |  required  | The guid of the table
    
<!-- END_929a46d19ba41110b6e8172060199722 -->

<!-- START_313265aec43594684c4f9448348980e6 -->
## List categories from a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/quas/categories/list"
);

let params = {
    "api_token": "commodi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tables/quas/categories/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'commodi',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "categories": [
        {
            "category_id": 1,
            "category_name": "ExampleCategory",
            "table_guid": "eaa8721a-31d0-4027-812f-ec199176960a"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "No categories found"
}
```

### HTTP Request
`GET /tables/{guid}/categories/list`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the associated table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_313265aec43594684c4f9448348980e6 -->

<!-- START_e1e7c266aca6d29fcc88e2a1eada34dc -->
## Give access to other users

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/qui/nutzer"
);

let params = {
    "api_token": "eaque",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "esse",
    "role": "deleniti"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/tables/qui/nutzer',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'eaque',
        ],
        'json' => [
            'email' => 'esse',
            'role' => 'deleniti',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Editor role given to Mustermann"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "email": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /tables/{guid}/nutzer`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `guid` |  required  | The guid of the table
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | The email of the user that receives access
        `role` | string |  required  | The name of the role the user should get
    
<!-- END_e1e7c266aca6d29fcc88e2a1eada34dc -->

<!-- START_241a784544c4c04491bcad8758c171eb -->
## Update a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/update"
);

let params = {
    "api_token": "repudiandae",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "ducimus",
    "guid": "ut"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/tables/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'repudiandae',
        ],
        'json' => [
            'name' => 'ducimus',
            'guid' => 'ut',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "table": {
        "id": 2,
        "guid": "b7d7d34b-fe58-4aef-9f5f-7b29080d14d4",
        "name": "Updated Name",
        "owner": 2,
        "sub_table": 0,
        "created_at": 1603897048,
        "updated_at": 1603897455,
        "deletion_date": 0
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "guid": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`PUT /tables/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | The new name of the table
        `guid` | string |  required  | The guid of the table
    
<!-- END_241a784544c4c04491bcad8758c171eb -->

<!-- START_902842b4fdbf948bfb45a1ca41bbf0fc -->
## Delete a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tables/delete"
);

let params = {
    "api_token": "quaerat",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "array": [
        {
            "guid": "aliquid"
        }
    ]
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/tables/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quaerat',
        ],
        'json' => [
            'array' => [
                [
                    'guid' => 'aliquid',
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "guid": [
            "Error message."
        ]
    }
}
```
> Example response (401):

```json
{
    "success": false,
    "status": 401,
    "message": "Unauthorized"
}
```
> Example response (403):

```json
{
    "success": false,
    "status": 403,
    "message": "Table does not exist."
}
```

### HTTP Request
`DELETE /tables/delete`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `array.*.guid` | string |  required  | The guid of the table
    
<!-- END_902842b4fdbf948bfb45a1ca41bbf0fc -->

#Tag management


<!-- START_7586267fb9b3512d8a8106f931904402 -->
## List all tags from a table

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
The same functionality is implemented under the table management section.

> Example request:

```javascript
const url = new URL(
    "http://localhost/tags/all"
);

let params = {
    "api_token": "autem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tags/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'autem',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tags": [
        {
            "id": 1,
            "tag": "ExampleTag",
            "created_at": "1603898348",
            "updated_at": "1603898348"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": true,
    "message": "Keine Tags vorhanden"
}
```

### HTTP Request
`GET /tags/all`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_7586267fb9b3512d8a8106f931904402 -->

<!-- START_16ad9457130d4ab4449883051e54c311 -->
## List all existing tags

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
The same functionality is implemented under the table management section.

> Example request:

```javascript
const url = new URL(
    "http://localhost/tags/list"
);

let params = {
    "api_token": "qui",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tags/list',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'qui',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tags": [
        {
            "id": 1,
            "tag": "ExampleTag",
            "created_at": "1603898348",
            "updated_at": "1603898348"
        }
    ]
}
```
> Example response (200):

```json
{
    "success": true,
    "message": "Keine Tags vorhanden"
}
```

### HTTP Request
`GET /tags/list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_16ad9457130d4ab4449883051e54c311 -->

<!-- START_73107310eb8de36d45196b69c246a367 -->
## Get one tag

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tags/omnis"
);

let params = {
    "api_token": "et",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/tags/omnis',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'et',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tag": {
        "id": 1,
        "tag": "ExampleTag",
        "created_at": "1603898348",
        "updated_at": "1603898348"
    }
}
```
> Example response (200):

```json
{
    "success": true,
    "message": "Kein Tag gefunden"
}
```

### HTTP Request
`GET /tags/{tagID}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `tagID` |  required  | The ID of the tag
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_73107310eb8de36d45196b69c246a367 -->

<!-- START_11187d36eedc81c4564549ae7c65ae9a -->
## Create a tag

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tags/new"
);

let params = {
    "api_token": "autem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    [
        {
            "tag": "qui"
        }
    ]
]

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/tags/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'autem',
        ],
        'json' => [
            [
                [
                    'tag' => 'qui',
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "tag": {
        "id": 1,
        "tag": "ExampleTag",
        "created_at": "1603898348",
        "updated_at": "1603898348"
    }
}
```
> Example response (200):

```json
{
    "success": true,
    "message": "Tags wurden nicht erstellt"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "tag": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /tags/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.*.tag` | string |  required  | The name of the tag (alpha)
    
<!-- END_11187d36eedc81c4564549ae7c65ae9a -->

<!-- START_0ca91b255418037f19709de2985723c6 -->
## Delete a tag

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tags/delete"
);

let params = {
    "api_token": "labore",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    [
        {
            "tag_id": 19
        }
    ]
]

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/tags/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'labore',
        ],
        'json' => [
            [
                [
                    'tag_id' => 19,
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Tags deleted"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "tag_id": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`DELETE /tags/delete`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.*.tag_id` | integer |  required  | The ID of the tag
    
<!-- END_0ca91b255418037f19709de2985723c6 -->

<!-- START_4d451d85a6b479ad896aa55b56066281 -->
## Update a tag

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/tags/update"
);

let params = {
    "api_token": "omnis",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = [
    [
        {
            "tag": "incidunt",
            "tag_id": 16
        }
    ]
]

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/tags/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'omnis',
        ],
        'json' => [
            [
                [
                    'tag' => 'incidunt',
                    'tag_id' => 16,
                ],
            ],
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "tag": {
        "id": 1,
        "tag": "ExampleTag",
        "created_at": "1603898348",
        "updated_at": "1603898348"
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "tag": [
            "Error message."
        ]
    }
}
```
> Example response (200):

```json
{
    "success": true,
    "message": "Keine Tags aktualisiert"
}
```

### HTTP Request
`PUT /tags/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `*.*.tag` | string |  required  | The name of the tag
        `*.*.tag_id` | integer |  required  | The ID of the tag
    
<!-- END_4d451d85a6b479ad896aa55b56066281 -->

#User Login/Logout



APIs for managing login and logout of users
<!-- START_dd217657c6d30db33bd0158a8815a014 -->
## Login

After a successful login a new api token and the user data is returned.

> Example request:

```javascript
const url = new URL(
    "http://localhost/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "neque",
    "password": "laboriosam"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/login',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'email' => 'neque',
            'password' => 'laboriosam',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "api_token": "274abd30-1f47-4f2b-98ee-9449f306753e",
    "data": {
        "id": 2,
        "email": "Bob@gmail.com",
        "username": "Bob",
        "registration_approved": 0,
        "created_at": 1603878735,
        "updated_at": 0
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "Your email or password is incorrect"
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "email": [
            "Error message."
        ]
    }
}
```

### HTTP Request
`POST /login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  required  | The email of the user
        `password` | string |  required  | The SHA512 hashed password of the user
    
<!-- END_dd217657c6d30db33bd0158a8815a014 -->

<!-- START_e9abfbadfb1757514ca2aa2de24f948e -->
## Logout

Deletes the api token of the user.

> Example request:

```javascript
const url = new URL(
    "http://localhost/user/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user/logout',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true
}
```

### HTTP Request
`GET /user/logout`


<!-- END_e9abfbadfb1757514ca2aa2de24f948e -->

#User management


<!-- START_669c21a0ec50102c5d7a38fdaec7d34e -->
## Register user (create)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/register"
);

let params = {
    "api_token": "aut",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "username": "placeat",
    "email": "architecto",
    "password": "dicta",
    "password_confirmation": "magni"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/register',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'aut',
        ],
        'json' => [
            'username' => 'placeat',
            'email' => 'architecto',
            'password' => 'dicta',
            'password_confirmation' => 'magni',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "message": "User successfully registered",
    "api_token": "af9b67ed-4e0c-4f5a-866e-77bc33ee58bd",
    "data": {
        "email": "Bob@gmail.com",
        "username": "Bob",
        "created_at": 1603961948,
        "id": 2
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": {
        "email": [
            "The email has already been taken."
        ]
    }
}
```

### HTTP Request
`POST /register`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  required  | The name of the user
        `email` | string |  required  | The email of the user
        `password` | string |  required  | The SHA512 hashed password of the user
        `password_confirmation` | string |  required  | The SHA512 hashed password confirmation of the user
    
<!-- END_669c21a0ec50102c5d7a38fdaec7d34e -->

<!-- START_863d7985d31cc55fc65edcd3b7915a90 -->
## IsAdmin

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
checks if the user is an admin

> Example request:

```javascript
const url = new URL(
    "http://localhost/user/isAdmin"
);

let params = {
    "api_token": "repellendus",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user/isAdmin',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'repellendus',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": "true",
    "admin": "true"
}
```
> Example response (200):

```json
{
    "success": "true",
    "admin": "false"
}
```

### HTTP Request
`GET /user/isAdmin`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_863d7985d31cc55fc65edcd3b7915a90 -->

<!-- START_5de59db5fe1cb2fee38034164f28968c -->
## List all users (Admin)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/user/all"
);

let params = {
    "api_token": "unde",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user/all',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'unde',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "users": [
        {
            "id": 1,
            "email": "hennig@debakom.de",
            "username": "root",
            "registration_approved": 1,
            "created_at": 0,
            "updated_at": 0
        },
        {
            "id": 2,
            "email": "Bob@gmail.com",
            "username": "Bob",
            "registration_approved": 0,
            "created_at": 1603960625,
            "updated_at": 0
        },
        {
            "id": 3,
            "email": "Max@gmail.com",
            "username": "Max",
            "registration_approved": 0,
            "created_at": 1603960630,
            "updated_at": 0
        }
    ]
}
```

### HTTP Request
`GET /user/all`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `partOf` |  required  | The guid of the associated project
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_5de59db5fe1cb2fee38034164f28968c -->

<!-- START_64401c1467bc7ec896075b6fa5ae964a -->
## Count all users

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/user/count"
);

let params = {
    "api_token": "quis",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user/count',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quis',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": "true",
    "user_count": "32"
}
```
> Example response (401):

```json
{
    "success": "false",
    "message": "Unauthorized"
}
```

### HTTP Request
`GET /user/count`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_64401c1467bc7ec896075b6fa5ae964a -->

<!-- START_27f0243b2a2c2b6486bd92cc72ff5659 -->
## Find user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/user/find/sit"
);

let params = {
    "api_token": "modi",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user/find/sit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'modi',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "user": {
        "id": 1,
        "email": "hennig@debakom.de",
        "username": "root",
        "registration_approved": 1,
        "created_at": 0,
        "updated_at": 0,
        "licenses": []
    },
    "license": false
}
```
> Example response (200):

```json
{
    "success": false,
    "status": 404,
    "message": "No query results for model [App\\User]."
}
```

### HTTP Request
`GET /user/find/{username}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `username` |  required  | 
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_27f0243b2a2c2b6486bd92cc72ff5659 -->

<!-- START_26fe881a2459eabd7b6715bf3f32c99c -->
## Get one user by id

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
A user can only call this on himself, an admin can get other users

> Example request:

```javascript
const url = new URL(
    "http://localhost/user/maiores"
);

let params = {
    "api_token": "hic",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user/maiores',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'hic',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "id": 2,
    "email": "Bob@gmail.com",
    "username": "Bob",
    "registration_approved": 0,
    "created_at": 1620825003,
    "updated_at": 0
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "User does not exist"
}
```

### HTTP Request
`GET /user/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the user
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_26fe881a2459eabd7b6715bf3f32c99c -->

<!-- START_cfdea6bd00af26d2464208564f2d921d -->
## Get one user by token

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Only returns the user of the token in the request.

> Example request:

```javascript
const url = new URL(
    "http://localhost/user"
);

let params = {
    "api_token": "voluptatem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'voluptatem',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "user": {
        "id": 2,
        "email": "Bob@gmail.com",
        "username": "Bob",
        "registration_approved": 0,
        "created_at": 1603960625,
        "updated_at": 0,
        "roles": [
            {
                "id": 3,
                "name": "User",
                "slug": "user",
                "permissions": "{\"create-table\":true,\"view-account\":true,\"upload-files\":true,\"read-files\":true}",
                "pivot": {
                    "user_id": 2,
                    "role_id": 3,
                    "created_at": "2020-10-29 08:37:05",
                    "updated_at": "2020-10-29 08:37:05"
                }
            }
        ]
    }
}
```

### HTTP Request
`GET /user`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_cfdea6bd00af26d2464208564f2d921d -->

<!-- START_e44c57d8cd7334aa3eafbbfffea5293e -->
## Delete a personal database

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/user/storage/delete"
);

let params = {
    "api_token": "dolorum",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/user/storage/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'dolorum',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Database 'ExampleName' deleted"
}
```
> Example response (200):

```json
{
    "success": true,
    "message": "'User' has no database"
}
```

### HTTP Request
`DELETE /user/storage/delete`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_e44c57d8cd7334aa3eafbbfffea5293e -->

<!-- START_b8ad8874e674832a08626ce3cc8f3a2c -->
## Delete a user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
A user can only call this on himself, an admin can delete others

> Example request:

```javascript
const url = new URL(
    "http://localhost/user/debitis/delete"
);

let params = {
    "api_token": "voluptatem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://localhost/user/debitis/delete',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'voluptatem',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "success": true,
    "message": "User deleted"
}
```
> Example response (403):

```json
{
    "success": false,
    "message": "Invalid permission"
}
```

### HTTP Request
`DELETE /user/{id}/delete`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the user
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_b8ad8874e674832a08626ce3cc8f3a2c -->

<!-- START_42f8360a8340eff40b057fd527c2555a -->
## Update a user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/user/quos/update"
);

let params = {
    "api_token": "quisquam",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "id",
    "username": "voluptas",
    "password": "adipisci",
    "password_confirmation": "velit"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://localhost/user/quos/update',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'quisquam',
        ],
        'json' => [
            'email' => 'id',
            'username' => 'voluptas',
            'password' => 'adipisci',
            'password_confirmation' => 'velit',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "user": {
        "id": 2,
        "email": "Bob@gmail.com",
        "username": "UpdatedName",
        "registration_approved": 0,
        "created_at": 1603961948,
        "updated_at": 1603962686
    }
}
```

### HTTP Request
`PUT /user/{id}/update`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the user
#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `email` | string |  optional  | optional The email of the user
        `username` | string |  optional  | optional The name of the user
        `password` | string |  optional  | The SHA512 hashed password of the user
        `password_confirmation` | string |  optional  | required_with_password The SHA512 hashed password of the user
    
<!-- END_42f8360a8340eff40b057fd527c2555a -->

<!-- START_838b8692ea56b2669ab8fad8c904cffb -->
## Get all table roles of a user (Testing only)

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
Returns The table and the role connected with it.

> Example request:

```javascript
const url = new URL(
    "http://localhost/user/roles/perferendis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://localhost/user/roles/perferendis',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "0": [
        {
            "id": 1,
            "guid": "747373d6-2abd-4f12-8c3a-733e048913db",
            "name": "Table01",
            "owner": 2,
            "sub_table": 0,
            "created_at": 1603960977,
            "updated_at": 0,
            "deletion_date": 0,
            "pivot": {
                "user_id": 2,
                "custom_table_id": 1,
                "table_role": "{\"owner\":true}",
                "created_at": "2020-10-29 08:42:57",
                "updated_at": "2020-10-29 08:42:57"
            }
        }
    ]
}
```

### HTTP Request
`GET /user/roles/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The id of the user

<!-- END_838b8692ea56b2669ab8fad8c904cffb -->

<!-- START_27770bf75919a2bb91e7d89bf9e8c9a0 -->
## Create a personal Database for the user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```javascript
const url = new URL(
    "http://localhost/user/storage/new"
);

let params = {
    "api_token": "dolor",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://localhost/user/storage/new',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'api_token'=> 'dolor',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (201):

```json
{
    "success": true,
    "message": "User database was successfully created"
}
```
> Example response (409):

```json
{
    "success": false,
    "message": "Database 'ExampleName' already exists"
}
```
> Example response (500):

```json
{
    "success": false,
    "message": "Something went wrong, try again!"
}
```
> Example response (401):

```json
{
    "success": false,
    "message": "Unauthorized"
}
```

### HTTP Request
`POST /user/storage/new`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `api_token` |  required  | Authenticates the user

<!-- END_27770bf75919a2bb91e7d89bf9e8c9a0 -->


